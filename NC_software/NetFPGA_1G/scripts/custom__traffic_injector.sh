#!/bin/bash

## --- Configuration ---
# Program utilities
memfill_bin="/usr/local/bin/memfill"
regwrite_bin="/usr/local/bin/regwrite"

# Interface of the NetFPGA
iface=""
# NCM program to execute in the NetFPGA
mem_file=""

## Command line configuration
# Interface
if [[ $1 ]]; then
    iface=$1
else
    echo "ERROR: Interface not specified for traffic injector"
    exit 1
fi
# Memory file
if [[ $2 ]]; then
    mem_file=$2
else
    echo "ERROR: Memory file not specified for traffic injector"
    exit 1
fi


## --- Execution ---

# Check for binary existence
if ! test -f "$memfill_bin" ; then
    echo "Binary not found, please make sure this is the path: $memfill_bin"
    exit 1
fi
#
if ! test -f "$regwrite_bin" ; then
    echo "Binary not found, please make sure this is the path: $regwrite_bin"
    exit 1
fi

# Get the location of this script (file)
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $BASE_PATH

# Write control register: Stop all NCMs
$regwrite_bin -i "$iface" 0x400000 0x00000000

## --- NCM 0 ---
# PROG_ROM
$memfill_bin  -i "$iface" 0x600000 "$mem_file"
# CFG_ROM
$regwrite_bin -i "$iface" 0x640008 0x114

## --- NCM 1 ---
$memfill_bin  -i "$iface" 0x680000 "$mem_file"
$regwrite_bin -i "$iface" 0x6C0008 0x114

## --- NCM 2 ---
$memfill_bin  -i "$iface" 0x700000 "$mem_file"
$regwrite_bin -i "$iface" 0x740008 0x114

## --- NCM 3 ---
$memfill_bin  -i "$iface" 0x780000 "$mem_file"
$regwrite_bin -i "$iface" 0x7C0008 0x114

# Write control register: Start NCMs
$regwrite_bin -i "$iface" 0x400000 0x00640111

