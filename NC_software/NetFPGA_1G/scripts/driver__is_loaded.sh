#!/bin/bash

## --- Configuration ---
# Driver name
driver_name="nf2"


## --- Execution ---

if lsmod | grep "$driver_name" 1> /dev/null ; then
  exit 1
else
  exit 0
fi

