#!/bin/bash


## --- Configuration ---
# Get the location of this script (file)
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Log file
LOG_FILE="$BASE_PATH/NetFPGA_init.log"
#LOG_FILE=/dev/null


## --- Execution ---

# Print date to the log file
echo "----- Log started at `date` -----" > "$LOG_FILE"

## Load driver if needed (not use a pipe to correctly eval the script's return value)
"$BASE_PATH"/driver__load.sh >> "$LOG_FILE" 2>&1
if [ $? -ne 0 ]; then
    echo "Error loading the driver, please check the logfile."
    exit 1
fi

## Program the NetFPGAs boards
## HINT: Add your own initialization scripts here (Use "tee" if you want to log)
#
# Examples (Board 1 ID is "nf2c0", Board 2 ID is "nf2c4", Board 3 ID is "nf2c8", Board 4 ID is "nf2c12":
# Board 1: Real-time switch
#"$BASE_PATH"/program__NetFPGA.sh "nf2c0" "$BASE_PATH/../FPGA_firmware/switch__RT_no_ncp.bit" 2>&1 | tee -a "$LOG_FILE"
# or
# Board 1: Reference NetFPGA switch
#"$BASE_PATH"/program__NetFPGA.sh "nf2c0" "$BASE_PATH/../FPGA_firmware/switch__reference_NetFPGA.bit" 2>&1 | tee -a "$LOG_FILE"
#
# Board 2: Traffic injector
#"$BASE_PATH"/program__NetFPGA.sh "nf2c4" "$BASE_PATH/../FPGA_firmware/dongle_emulator.bit" 2>&1 | tee -a "$LOG_FILE"
#"$BASE_PATH"/custom__traffic_injector.sh "nf2c4" "$BASE_PATH/../FPGA_firmware/dongle_emulator/NC_tx.txt" 2>&1 | tee -a "$LOG_FILE"
#
# This lines initialize two boards with Real-time capable switches
# If the host machine has more boards, add corresponding lines with IDs nf2c8 and nf2c12

"$BASE_PATH"/program__NetFPGA.sh "nf2c0" "$BASE_PATH/../FPGA_firmware/switch__RT_no_ncp.bit" 2>&1 | tee -a "$LOG_FILE"
"$BASE_PATH"/program__NetFPGA.sh "nf2c4" "$BASE_PATH/../FPGA_firmware/switch__RT_no_ncp.bit" 2>&1 | tee -a "$LOG_FILE"

echo "--------------- End of execution ---------------" >> "$LOG_FILE"

