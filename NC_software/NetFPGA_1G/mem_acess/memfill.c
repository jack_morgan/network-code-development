/* ****************************************************************************
 * $Id: memfill.c $
 *
 * Module: memfill.c
 * Project: NCM PC interface
 * Description: Can fill the NCM memory
 *
 * Change history:
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <net/if.h>

#include "regdefs.h"
#include "../common/nf2.h"
#include "../common/nf2util.h"

#define PATHLEN		80

#define DEFAULT_IFACE	"nf2c0"

/* Global vars */
static struct nf2device nf2;
static int verbose = 0;
static int force_cnet = 0;
static int alias = 0;
static int base_offset = 0;
unsigned int * data2store;
int N;
/* Function declarations */
void writeRegisters (int , char **);
void processArgs (int , char **);
void usage (void);

int fetch_file(char *filename,unsigned int **data,int *N){
	int nlines=0;
	int i;
	char a[512];
	FILE * datafile;
	datafile = fopen(filename,"r");
	if(!datafile){
		perror("Error abriendo archivo");
		exit(-1);
	}
	while(fgets(a,512,datafile))
		nlines++;
	fseek(datafile,0,SEEK_SET);
	*data = (unsigned int *) malloc(nlines*sizeof(unsigned int));
	for(i=0;i<nlines;i++){
		fscanf(datafile,"%08x",&data[0][i]);
		while(fgetc(datafile) != '\n');
	}
	*N = nlines;
	return 0;
}

int main(int argc,char ** argv){
	unsigned val;
	int i;
	nf2.device_name = DEFAULT_IFACE;

	processArgs(argc, argv);

	// Open the interface if possible
	if (check_iface(&nf2))
	{
		exit(1);
	}
	if (openDescriptor(&nf2))
	{
		exit(1);
	}

	// Increment the argument pointer
	argc -= optind;
	argv += optind;
	

	// Read the registers
	writeRegisters(argc, argv);

	closeDescriptor(&nf2);

	return 0;
}
/*
 * Write the register(s)
 */
void writeRegisters(int argc, char** argv)
{
	int i;
	int ok;
	unsigned addr;
	unsigned value;
	int limit_address = 512;
	// Verify that we actually have some registers to display
	if (argc == 0)
	{
		usage();
		exit(1);
	}
	else if (argc !=  2 )
	{
		fprintf(stderr, "Error: you must supply address/file pair\n");
		usage();
		exit(1);
	}

  // Read the file and get size
	fetch_file(argv[1],&data2store,&N);
	// Process the registers one by one
	for (i = 0; i < argc; i += 2)
	{
		// Work out if we're dealing with decimal or hexadecimal
		if(!alias)
    if (strncmp(argv[i], "0x", 2) == 0 || strncmp(argv[i], "0X", 2) == 0)
		{
			sscanf(argv[i] + 2, "%x", &addr);
		}
		else
		{
			sscanf(argv[i], "%u", &addr);
		}
    else{
      if(strcmp(argv[i],"DATA_MEMORY")==0){
        addr = DATA_MEMORY_BASE;
	// So if i check this here probably the condition to check the boundary in the loop ahead would never trigger
        if(N-base_offset>DATA_MEMORY_SIZE){ // Limit for data_memory
          printf("Error: data_memory would be out of bounds\n");
          exit(-1);
        }
      }else
      { // For any other case limit is 512 or PROG_ROM_SIZE (not generic enough?)
        if(N-base_offset>PROG_ROM_SIZE){
          printf("Error: data_memory would be out of bounds\n");
          exit(-1);
        }
      }
      if(strcmp(argv[i],"PROG_ROM_0")==0)
        addr = PROG_ROM_0_BASE;
      else if(strcmp(argv[i],"CFG_ROM_0")==0)
        addr = CFG_ROM_0_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_0")==0)
        addr = CFG_ROM_QUEUE_0_BASE;
      else if(strcmp(argv[i],"PROG_ROM_1")==0)
        addr = PROG_ROM_1_BASE;
      else if(strcmp(argv[i],"CFG_ROM_1")==0)
        addr = CFG_ROM_1_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_1")==0)
        addr = CFG_ROM_QUEUE_1_BASE;
      else if(strcmp(argv[i],"PROG_ROM_2")==0)
        addr = PROG_ROM_2_BASE;
      else if(strcmp(argv[i],"CFG_ROM_2")==0)
        addr = CFG_ROM_2_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_2")==0)
        addr = CFG_ROM_QUEUE_2_BASE;
      else if(strcmp(argv[i],"PROG_ROM_3")==0)
        addr = PROG_ROM_3_BASE;
      else if(strcmp(argv[i],"CFG_ROM_3")==0)
        addr = CFG_ROM_3_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_3")==0)
        addr = CFG_ROM_QUEUE_3_BASE;
      else {
        printf("Wrong alias, valid options include:\n");
        printf("\t DATA_MEMORY\n");
        printf("\t PROG_ROM_N\n");
        printf("\t CFG_ROM_N\n");
        printf("\t CFG_ROM_QUEUE_N\n");
        return ;
      }
    }
		// Work out if we're dealing with decimal or hexadecimal
		// Perform the actual register write
		ok = 0;
		// README: Its different than the readall case 'cos i is data2store index now.
		int real_limit = addr + limit_address*4;
		addr += base_offset*4; // Am i chasing my own tail here? :S... getting tired...should work
		for(i=0;i < N; i++){ 
			//printf("Storing: %08x\n",data2store[i]);
			if(real_limit == addr+4*i){
				printf("Limit address reached\n\n");
				break;
			}
			writeReg(&nf2, addr+4*i, data2store[i]);
			readReg(&nf2, addr+4*i, &value); // work around the missing cpcirequest and check
			printf("Write: Reg 0x%08x (%u):   0x%08x (%u)\n", addr+4*i, addr, value, value);
			if(data2store[i] != value)
				ok++;
		}
		if(!ok){
			printf("Verified. Memory was Successfully filled!\n");
			printf("%u words written!\n",N);
		}
		else 
			printf("Differences Exists!...Please repeat\n");
	}
}

/* 
 *  Process the arguments.
 */
void processArgs (int argc, char **argv )
{
	char c;

	/* don't want getopt to moan - I can do that just fine thanks! */
	opterr = 0;
	  
	while ((c = getopt (argc, argv, "i:aho:")) != -1)
	{
		switch (c)
	 	{
	 		case 'i':	/* interface name */
		 		nf2.device_name = optarg;
		 		break;
			case 'o':
				base_offset= atoi(optarg);
				break;
			case 'a':
        alias = 1;
        break;
	 		case '?':
		 		if (isprint (optopt))
		         		fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		 		else
		         		fprintf (stderr,
		                  		"Unknown option character `\\x%x'.\n",
		                  		optopt);
			case 'h':
	 		default:
		 		usage();
		 		exit(1);
	 	}
	}
}


/*
 *  Describe usage of this program.
 */
void usage (void)
{
	printf("Usage: ./memfill <options> [addr...] file.txt\n\n");
	printf("Options  -h : Print this message and exit.\n");
	printf("         -o : Defines an offset in 32 bits words over the base address to start reading (default 0).\n");
  	printf("         -a : Allows an alias instead an address. Eg:\n");
	printf("\t\t DATA_MEMORY\n");
	printf("\t\t PROG_ROM_N\n");
	printf("\t\t CFG_ROM_N\n");
	printf("\t\t CFG_ROM_QUEUE_N\n");
	printf("         [addr] Is the base address to start copying\n");
}
