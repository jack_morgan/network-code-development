# README #

This folder contains an utility, NC_transcoder, that converts to machine-code a text file containing assembly instructions.
The available instructions are described in the "Byte Code Specification" section of the "Network Code Language Specification" document.

As an example, consider the NC program used in demo_0 as he master node:

	L0: sync(master,7);
	    future(552,L1);
	    halt();
	L1: future(872,L2)
	    create(1);
	    send(0,RT,7);
	    halt();
	L2: future(2506,L0);
	    create(1);
	    send(0,BE,7);
	    halt();

The program sends a master sync frame, and then transmits sequentially two copies of variable 1, first in a real-time frame and then in a best-effort frame. This process repeat over and over.
 
Important: The current version of the utility doesn't play well with empty lines, miscalculating the labels addresses and producing errors when executed in the real processor, so please be sure to delete all empty lines before yo run this utility.
The only recommended empty line should be at the end of the file because it helps the utility to properly identify it.


The command to use the utility is straightforward because only requires the path to an assembly file. For example:

	NC_transcoder ../example_folder/example_prog.txt


The output is a file containing the machine-code formatted as hexadecimal 32 bits words per line and ended with a semicolon.
For the example program, the output file is:

	F8000007;
	50300228;
	40000000;
	50700368;
	10000100;
	20000007;
	40000000;
	500009CA;
	10000100;
	21000007;
	40000000;
