#!/bin/bash

# --- Get the location of this script (file) ---
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


# --- Init and program NetFPGA-1G boards ---
$BASE_PATH/NetFPGA_1G/scripts/init_NetFPGA_1G.sh

# TODO: Put here some different boards required

