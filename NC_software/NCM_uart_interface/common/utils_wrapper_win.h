#ifndef UTILS_WRAPPER_WIN_H_INCLUDED
#define UTILS_WRAPPER_WIN_H_INCLUDED

#include <windows.h>

void Utils_sleep_ms( int time_ms ) {
    Sleep( time_ms );
}

void Utils_clear_console( void ) {
    system( "cls" );
}


static LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
static LARGE_INTEGER Frequency;

void Utils_timer_us_init( void ) {
    QueryPerformanceFrequency(&Frequency);
}

void Utils_timer_us_start( void ) {
    QueryPerformanceCounter(&StartingTime);
}

unsigned int Utils_timer_us_elapsed( void ) {
    QueryPerformanceCounter(&EndingTime);
    ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
    // We now have the elapsed number of ticks, along with the
    // number of ticks-per-second. We use these values
    // to convert to the number of elapsed microseconds.
    // To guard against loss-of-precision, we convert
    // to microseconds *before* dividing by ticks-per-second.
    //
    ElapsedMicroseconds.QuadPart *= 1000000;
    ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;
    return (unsigned int) ElapsedMicroseconds.QuadPart;
}

#endif // UTILS_WRAPPER_WIN_H_INCLUDED
