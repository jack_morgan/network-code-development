#ifndef SERIAL_WRAPPER_WIN_H_INCLUDED
#define SERIAL_WRAPPER_WIN_H_INCLUDED

#include <windows.h>


static HANDLE port_handle = NULL;

int SerialWrapper_OpenPort( char *port_name, int baud_rate, int read_timeout_ms, char *error_str_ptr ) {
    // Open virtual file
    port_handle = CreateFile( port_name, GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH, 0 );
    if( port_handle == INVALID_HANDLE_VALUE ) {
	    error_str_ptr[0] = 'E';
	    error_str_ptr[1] = '1';
	    error_str_ptr[2] = 0;
	    CloseHandle( port_handle );
	    port_handle = NULL;
		return -1;
	}

    // Configure connection
    DCB dcb;
    FillMemory( &dcb, sizeof(dcb), 0 );
    dcb.DCBlength = sizeof(dcb);
    char conn_str[50];
    sprintf( conn_str, "baud=%i parity=N data=8 stop=1", baud_rate );

    if( !BuildCommDCB(conn_str,&dcb) || !SetCommState(port_handle, &dcb) ) {
        error_str_ptr[0] = 'E';
        error_str_ptr[1] = '2';
	    error_str_ptr[2] = 0;
        CloseHandle( port_handle );
        port_handle = NULL;
        return -1;
    }

    // Set timeout
    COMMTIMEOUTS time_out_cfg;
    time_out_cfg.ReadIntervalTimeout = 0;
    time_out_cfg.ReadTotalTimeoutMultiplier = 0;
    time_out_cfg.ReadTotalTimeoutConstant = read_timeout_ms;
    time_out_cfg.WriteTotalTimeoutMultiplier = 0;
    time_out_cfg.WriteTotalTimeoutConstant = 0;
    if( !SetCommTimeouts(port_handle, &time_out_cfg ) ) {
        error_str_ptr[0] = 'E';
        error_str_ptr[1] = '3';
	    error_str_ptr[2] = 0;
        CloseHandle( port_handle );
        port_handle = NULL;
        return -1;
    }

    return 0;
}

void SerialWrapper_ClosePort( void ) {

    if( port_handle != NULL )
        CloseHandle( port_handle );

    port_handle = NULL;

    return;
}


int SerialWrapper_SendData( uint8_t *data_buffer, uint16_t data_len ) {

    if( port_handle == NULL )
        return -1;

    int bytes_Tx;
    WriteFile( port_handle, data_buffer, data_len, (PDWORD)&bytes_Tx, NULL );
    if( bytes_Tx != data_len ) {
        CloseHandle( port_handle );
        port_handle = NULL;
        return -1;
    }

    return 0;
}


int SerialWrapper_RecvData( uint8_t *data_buffer, uint16_t max_data_len ) {

    if( port_handle == NULL )
        return -1;

    int bytes_Rx;
    ReadFile( port_handle, data_buffer, max_data_len, (PDWORD)&bytes_Rx, NULL );
    if( bytes_Rx != max_data_len ) {
        CloseHandle( port_handle );
        port_handle = NULL;
        return -1;
    }

    return 0;
}

void SerialWrapper_FlushRxBuff( void ) {

    if( port_handle == NULL )
        return;

    PurgeComm( port_handle, PURGE_RXABORT | PURGE_RXCLEAR );

    return;
}


#endif // SERIAL_WRAPPER_WIN_H_INCLUDED
