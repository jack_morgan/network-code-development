#ifndef UTILS_WRAPPER_LINUX_H_INCLUDED
#define UTILS_WRAPPER_LINUX_H_INCLUDED

#include <sys/time.h>


void Utils_sleep_ms( int time_ms ) {
    usleep( 1000*time_ms );
}

void Utils_clear_console( void ) {
    // "tput reset" is better but not always present.
    system( "clear" );
}

struct timeval StartingTime, EndingTime;

void Utils_timer_us_init( void ) {
    return;
}

void Utils_timer_us_start( void ) {
     gettimeofday( &StartingTime, NULL );
    return;
}

unsigned int Utils_timer_us_elapsed( void ) {

    long int elapsedTime;
    gettimeofday( &EndingTime, NULL );

    // Compute the elapsed time
    elapsedTime = ((EndingTime.tv_sec - StartingTime.tv_sec)*1000000L + EndingTime.tv_usec) - StartingTime.tv_usec;
    return elapsedTime;
}

#endif // UTILS_WRAPPER_LINUX_H_INCLUDED
