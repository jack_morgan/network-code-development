#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

void print_array( uint8_t *array_ptr, uint32_t array_size, uint32_t N_columns );

void print_uint32_array( uint32_t *array_ptr, uint32_t array_size, uint32_t N_columns );

// Manual platform selection. For use without build targets on Code::Blocks
#ifndef auto_sel_platform
 //#define platform_windows
 //#define platform_linux
#endif


#include <serial_wrapper.h>
#include <utils_wrapper.h>

// -- Misc --
#define MAX_ERROR_STR_LEN 2048
#define MAX_WRITE_TRIES   4
#define PROG_VERSION      "0.1"
// -- File parsing --
// In words (uint32_t)
#define MAX_MEM_SIZE 256
// In chars
#define LINE_BUFF_SIZE     100
#define MIN_VALID_DATA_LEN 8

// -- OP codes: 1 byte values --
#define OP_WRITE_MEM  0
#define OP_READ_MEM   1
#define OP_RESP_WRITE 2 // TODO
#define OP_RESP_READ  3 // TODO

// -- Serial port --
#define SERIAL_START_CHAR 129
// In bytes
#define SERIAL_MAX_PAYLOAD_LEN 1400


void print_array( uint8_t *array_ptr, uint32_t array_size, uint32_t N_columns ) {

    uint32_t columns_count, items_count;

    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "%02i  ", columns_count );
    printf( "\n" );
    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "----" );
    printf( "\n" );

    for( items_count = 0, columns_count = 0; items_count < array_size; ++items_count ) {
        printf( "%02X  ", array_ptr[items_count] );

        ++columns_count;
        if( columns_count == N_columns ) {
            printf( "\n" );
            columns_count = 0;
        }
    }
    printf( "\n" );

    return;
}

void print_uint32_array( uint32_t *array_ptr, uint32_t array_size, uint32_t N_columns ) {

    uint32_t columns_count, items_count = 0;

    printf( "      " );
    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "   %02i      ", columns_count );
    printf( "\n     " );
    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "-----------" );
    printf( "\n%3i | ", items_count );
    for( items_count = 0, columns_count = 0; items_count < array_size; ++items_count ) {
        printf( "%08X | ", array_ptr[items_count] );

        ++columns_count;
        if( columns_count == N_columns ) {
            if( items_count+1 != array_size )
                printf( "\n%3i | ", items_count+1 );
            columns_count = 0;
        }
    }
    printf( "\n" );

    return;
}


void print_commands( void ) {
    printf( "Available commands:\n" );
    printf( " -ser: Connect using serial port\n" );
    printf( " -mem: Memory ID\n" );
    printf( " -r: Read data from memory\n" );
    printf( " -w: Write data to memory\n" );
    printf( " -f: Input or output file (optional when reading from memory)\n" );
    return;
}


int main( int argc, char* argv[] ) {
    char error_str[MAX_ERROR_STR_LEN];
    char *in_out_filename = NULL;
    // Mode selection
    char read_or_write_mem = 0; // 0=not set; 1=read; 2=write
    //
    //const uint8_t station_ID = 1;
    uint8_t memory_ID  = 10;
    const uint8_t memory_section = 0;
    // Serial interface
    char    *serial_port_name = "COM4";
    uint32_t serial_baud_rate = 0;
    // Interface selection
    char serial_selected = 0; // 0=not selected 1=serial

    printf( "NCM programmer %s\n", PROG_VERSION );

    // Validate and parse commands
    int cmd_idx;
    if( argc > 1 ) {
        for( cmd_idx = 1; cmd_idx < argc; ++cmd_idx ) {
            if( strcmp(argv[cmd_idx],"-ser") == 0 && argc > cmd_idx+2 ){
                serial_port_name = argv[++cmd_idx];
                if( serial_selected != 0 ) {
                    printf( "Only one communication interface permitted\n" );
                    return 0;
                }
                serial_baud_rate = atoi( argv[++cmd_idx] );
                serial_selected = 1;
            }
            else if( strcmp(argv[cmd_idx],"-f") == 0 && argc > cmd_idx+1 )
                in_out_filename = argv[++cmd_idx];
            else if( strcmp(argv[cmd_idx],"-mem") == 0 && argc > cmd_idx+1 )
                memory_ID = atoi(argv[++cmd_idx]);
            else if( strcmp(argv[cmd_idx],"-r") == 0 ) {
                if( read_or_write_mem != 0 ) {
                    printf( "Only one operation permitted\n" );
                    return 0;
                }
                read_or_write_mem = 1;
            }
            else if( strcmp(argv[cmd_idx],"-w") == 0 ) {
                if( read_or_write_mem != 0 ) {
                    printf( "Only one operation permitted\n" );
                    return 0;
                }
                read_or_write_mem = 2;
            }
            else
                printf( "cmd %i: %s\n", cmd_idx, argv[cmd_idx] );
        }

    }
    else if( argc == 1 ) {
        print_commands();
        return 0;
    }

    // -- Check conditions --
    if( serial_selected == 0 ) {
        printf( "No serial port selected\n" );
        return 0;
    }
    if( serial_baud_rate == 0 ) {
        printf( "Invalid baud rate\n" );
        return 0;
    }

    if( read_or_write_mem == 0 ) {
        printf( "No operation selected\n" );
        return 0;
    }
    if( (read_or_write_mem == 2) && (in_out_filename == NULL) ) {
        printf( "Write operation selected without an input file\n" );
        return 0;
    }
    if( memory_ID == 10 ) {
        printf( "No memory selected\n" );
        return 0;
    }

    // ---- Open the input/output file ----
    FILE * in_out_file = NULL;
    if( in_out_filename != NULL ) {
        char *open_mode = (read_or_write_mem == 1)? ("wb") : ("rb");
        in_out_file = fopen( in_out_filename, open_mode );
        if( in_out_file == NULL ) {
            printf( "Error: Can't open file for %s (%s)\n", ((read_or_write_mem == 1)? ("write") : ("read")), in_out_filename );
            return -1;
        }
    }


    // ---- Read file content to memory buffer ----
    uint32_t mem_array[MAX_MEM_SIZE] = {0};
    uint32_t mem_size = 0;

    if( (read_or_write_mem == 2) && (in_out_filename != NULL) ) {
        char line_buffer[LINE_BUFF_SIZE];
        char    *num_as_str = NULL;
        uint32_t num_as_int = 0;

        while( 1 ) {
            // Read one line from file
            if( fgets(line_buffer,LINE_BUFF_SIZE,in_out_file) == NULL ) {
                if( feof(in_out_file) == 0 ) { // Error
                    fclose( in_out_file );
                    printf( "Error: On reading input file\n" );
                    return -1;
                }
                else { // EOF
                    fclose( in_out_file );
                    break;
                }
            }

            // Test line length
            if( strlen(line_buffer) >= (LINE_BUFF_SIZE-1) ) {
                fclose( in_out_file );
                printf( "Error: Maximum line length (%i) reached\n", LINE_BUFF_SIZE-1 );
                return -1;
            }

            // Split data from delimiters
            num_as_str = strtok( line_buffer, ";" );

            // Skip small and empty lines
            if( strlen(num_as_str) < MIN_VALID_DATA_LEN ) {
                printf( "Warning: Skipping small/empty data (length is %i)\n", strlen(num_as_str) );
                continue;
            }

            // Convert from ascii to integer (little-endian)
            num_as_int = strtoul( num_as_str, NULL, 16 );

            // Check buffMAX_MEM_SIZEer size
            if( mem_size >= MAX_MEM_SIZE ) {
                fclose( in_out_file );
                printf( "Error: File exceeds maximum length (%i words)\n", MAX_MEM_SIZE );
                return -1;
            }

            // Add element to the array
            mem_array[mem_size++] = num_as_int;
        }

        fclose( in_out_file );
    }

    // -- Serial interface --
    uint8_t  payload_buffer[SERIAL_MAX_PAYLOAD_LEN] = { 0 };
    uint8_t  recv_buffer[SERIAL_MAX_PAYLOAD_LEN] = { 0 };
    uint32_t payload_len = 0;
    // Header
    payload_buffer[0] = SERIAL_START_CHAR ;
    payload_buffer[1] = memory_ID;
    payload_buffer[2] = memory_section;
    payload_buffer[3] = (read_or_write_mem == 1)? (OP_READ_MEM) : (OP_WRITE_MEM);
    // Data
    memcpy( &payload_buffer[4], mem_array, mem_size*sizeof(uint32_t) );
    // Final length
    //payload_len = 4 + mem_size*sizeof(uint32_t);
    payload_len = 4 + 256*sizeof(uint32_t);

    // Open port
    if( SerialWrapper_OpenPort(serial_port_name,serial_baud_rate,3000,error_str) != 0 ) {
        printf( "Error: Unable to open serial port \"%s\"\n Info: \"%s\"", serial_port_name, error_str );
        return -1;
    }

    int write_tries = 0;
    while( 1 ) {
        //SerialWrapper_FlushRxBuff();
        // Send data (request)
        if( SerialWrapper_SendData(payload_buffer,payload_len) != 0 ) {
            printf( "Error: On sending data\n" );
            return -1;
        }
        Utils_sleep_ms( 60 );
        // Receive data (response)
        if( SerialWrapper_RecvData(recv_buffer,payload_len-1) != 0 ) {
            printf( "Error: On receiving data\n" );
            return -1;
        }

        // Read operation
        if( read_or_write_mem == 1 ) {
            if( in_out_file == NULL ) { // Monitor mode
                Utils_clear_console();
                printf( "NCM programmer %s\n", PROG_VERSION );
                printf( "Monitor mode: Mem ID = %i\n", memory_ID );
                print_uint32_array( (uint32_t*)&recv_buffer[3], MAX_MEM_SIZE, 8 );
                Utils_sleep_ms( 500 );
            }
            else {
                int idx;
                uint32_t *buffer_ptr = (uint32_t*) &recv_buffer[3];
                for( idx = 0; idx < MAX_MEM_SIZE; ++idx ) {
                    fprintf( in_out_file, "%08X;\n", buffer_ptr[idx] );
                }
                printf( "Memory contents read successfully\n" );
                break;
            }

        }
        // Write operation
        if( read_or_write_mem == 2 ) {
            // Compare received data
            if( memcmp(&recv_buffer[3],mem_array,mem_size*sizeof(uint32_t)) == 0 ) {
                printf( "Memory contents written successfully\n" );
                break;
            }
            else {
                ++write_tries;
                if( write_tries == MAX_WRITE_TRIES ) {
                    printf( "Error: Maximum write tries. Can't write correctly.\n" );
                    return -1;
                }
                else
                    printf( "Error: (%i/%i) Memory contents don't match\n", write_tries, MAX_WRITE_TRIES );
            }
        }

    }

    // Close port
    SerialWrapper_ClosePort();




    return 0;
}
