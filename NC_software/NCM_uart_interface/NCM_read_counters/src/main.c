#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void print_array( uint8_t *array_ptr, uint32_t array_size, uint32_t N_columns );

void print_uint32_array( uint32_t *array_ptr, uint32_t array_size, uint32_t N_columns );

// Manual platform selection. For use without build targets on Code::Blocks
#ifndef auto_sel_platform
 //#define platform_windows
 //#define platform_linux
#endif


#include <serial_wrapper.h>
#include <utils_wrapper.h>

// -- Misc --
#define MAX_ERROR_STR_LEN 2048
#define MAX_WRITE_TRIES   4
#define PROG_VERSION      "0.1"
// -- File parsing --
// In words (uint32_t)
#define MAX_MEM_SIZE 256
// In chars
#define LINE_BUFF_SIZE     20
#define MIN_VALID_DATA_LEN 8

// -- OP codes: 1 byte values --
#define OP_WRITE_MEM  0
#define OP_READ_MEM   1
#define OP_RESP_WRITE 2 // TODO
#define OP_RESP_READ  3 // TODO

// -- Serial port --
#define SERIAL_START_CHAR 129
// In bytes
#define SERIAL_MAX_PAYLOAD_LEN 1400


void print_array( uint8_t *array_ptr, uint32_t array_size, uint32_t N_columns ) {

    uint32_t columns_count, items_count;

    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "%02i  ", columns_count );
    printf( "\n" );
    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "----" );
    printf( "\n" );

    for( items_count = 0, columns_count = 0; items_count < array_size; ++items_count ) {
        printf( "%02X  ", array_ptr[items_count] );

        ++columns_count;
        if( columns_count == N_columns ) {
            printf( "\n" );
            columns_count = 0;
        }
    }
    printf( "\n" );

    return;
}

void print_uint32_array( uint32_t *array_ptr, uint32_t array_size, uint32_t N_columns ) {

    uint32_t columns_count, items_count = 0;

    printf( "      " );
    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "   %02i      ", columns_count );
    printf( "\n     " );
    for( columns_count = 0; columns_count < N_columns; ++columns_count )
        printf( "-----------" );
    printf( "\n%3i | ", items_count );
    for( items_count = 0, columns_count = 0; items_count < array_size; ++items_count ) {
        printf( "%08X | ", array_ptr[items_count] );

        ++columns_count;
        if( columns_count == N_columns ) {
            if( items_count+1 != array_size )
                printf( "\n%3i | ", items_count+1 );
            columns_count = 0;
        }
    }
    printf( "\n" );

    return;
}

// Conversion constant: To bits -> To seconds -> To Mbps
#define BW_CONV_CONST 8.0*1000000/(1024*1024)

void print_counters( uint32_t *array_ptr, uint32_t array_size, uint32_t N_columns, uint32_t elap_time_us, FILE * out_file ) {

    static unsigned int old_total_tx_count = 0, old_total_rx_count = 0, old_RT_rx_count = 0, old_non_RT_rx_count = 0, old_rcv_fault = 0, old_synq_fault = 0;

    double total_tx_bw  = BW_CONV_CONST*(array_ptr[14]-old_total_tx_count) / (double) elap_time_us;
    double total_rx_bw  = BW_CONV_CONST*(array_ptr[16]-old_total_rx_count) / (double) elap_time_us;
    double RT_rx_bw     = BW_CONV_CONST*(array_ptr[29]-old_RT_rx_count) / (double) elap_time_us;
    double non_RT_rx_bw = BW_CONV_CONST*(array_ptr[34]-old_non_RT_rx_count) / (double) elap_time_us;
    double rcv_fault_s  = 1000000*(array_ptr[11]-old_rcv_fault) / (double) elap_time_us;
    double synq_fault_s = 1000000*(array_ptr[13]-old_synq_fault) / (double) elap_time_us;
    //
    old_total_tx_count  = array_ptr[14];
    old_total_rx_count  = array_ptr[16];
    old_RT_rx_count     = array_ptr[29];
    old_non_RT_rx_count = array_ptr[34];
    old_rcv_fault       = array_ptr[11];
    old_synq_fault      = array_ptr[13];

    printf( "--------Network-Code-----------\n" );
    printf( "created_data_frames_cnt:     %u\n", array_ptr[0]  );
    printf( "sent_data_frames_cnt:        %u\n", array_ptr[1]  );
    printf( "rcv_cmd_ok_cnt:              %u\n", array_ptr[10] );
    printf( "rcv_cmd_fault_cnt:           %.1f faults/s (%u)\n", rcv_fault_s, array_ptr[11] );
    printf( "sent_ack_frames_cnt:         %u\n", array_ptr[3]  );
    printf( "bytes_read_from_txfifo_cnt:  %u\n", array_ptr[30] );
    printf( "tx_fifo_overflow_cnt:        %u\n", array_ptr[18] );
    printf( "total_tx_bytes_cnt:          %.1f Mbps (%u)\n", total_tx_bw, array_ptr[14] );
    printf( "rx_RT_bytes_cnt:             %.1f Mbps (%u)\n", RT_rx_bw, array_ptr[29] );
    printf( "rx_bytes_non_RT_cnt:         %.1f Mbps (%u)\n", non_RT_rx_bw, array_ptr[34] );
    printf( "total_rx_bytes_cnt:          %.1f Mbps (%u)\n", total_rx_bw, array_ptr[16] );
    printf( "rx_goodframes_error_cnt:     %u\n", array_ptr[19] );
    printf( "rx_goodframes_non_RT_cnt:    %u\n", array_ptr[8]  );
    printf( "rx_badframes_non_RT_cnt:     %u\n", array_ptr[9]  );
    printf( "rx_total_goodframes_cnt:     %u\n", array_ptr[24] );
    printf( "rx_total_badframes_cnt:      %u\n", array_ptr[25] );
    printf( "running_time_tx_cnt:         %u\n", array_ptr[15] );
    printf( "running_time_rx_cnt:         %u\n", array_ptr[17] );
    printf( "------------Sync----------------\n" );
    printf( "sent_sync_frames_cnt:        %u\n", array_ptr[2]  );
    printf( "expected_sync_ok_cnt:        %u\n", array_ptr[12] );
    printf( "sync_timeout_cnt:            %.1f faults/s (%u)\n", synq_fault_s, array_ptr[13] );
    printf( "total_sync_cnt:              %u\n", array_ptr[31] );
    printf( "master_sync_cnt:             %u\n", array_ptr[32] );
    printf( "slave_sync_cnt:              %u\n", array_ptr[33] );
    printf( "-----------Autorec-------------\n" );
    printf( "autorec_goodcnt_data:        %u\n", array_ptr[26] );
    printf( "autorec_badcnt_data:         %u\n", array_ptr[27] );
    printf( "autorec_goodcnt_sync:        %u\n", array_ptr[28] );
    printf( "----------Classifier-----------\n" );
    printf( "rx_total_RT_dataframes_cnt:  %u\n", array_ptr[20] );
    printf( "rx_total_RT_syncframes_cnt:  %u\n", array_ptr[21] );
    printf( "rx_total_RT_ackframes_cnt:   %u\n", array_ptr[22] );
    printf( "rx_total_nonRT_frames_cnt:   %u\n", array_ptr[23] );
    printf( "----------MAC-logic-----------\n" );
    printf( "rx_goodframes_RT_data_cnt:   %u\n", array_ptr[4]  );
    printf( "rx_goodframes_RT_sync_cnt:   %u\n", array_ptr[5]  );
    printf( "rx_badframes_RT_data_cnt:    %u\n", array_ptr[6]  );
    printf( "rx_badframes_RT_syn_cnt:     %u\n", array_ptr[7]  );
    printf( "-------------------------------\n" );

    printf( "\n" );

    // Log to file
    static uint8_t iter_count        = 0;
    static double total_elapsed_time = 0;
    if( (out_file != NULL) && (total_elapsed_time != 0) ) {
        fprintf( out_file, "%.2f, %.1f, %.1f, %.1f, %.1f, %.1f, %.1f\n", total_elapsed_time, total_tx_bw, RT_rx_bw, non_RT_rx_bw, total_rx_bw, rcv_fault_s, synq_fault_s );
        // Force write to file
        if( iter_count == 20 ) {
            fflush( out_file );
            iter_count = 0;
        }
    }
    total_elapsed_time += ((double) elap_time_us)/ (1e6);

    return;
}



void print_commands( void ) {
    printf( "Available commands:\n" );
    printf( " -ser: Connect using serial port\n" );
    printf( " -m: Monitor mode (continuous)\n" );
    printf( " -f: Output file (optional)\n" );
    return;
}


int main( int argc, char* argv[] ) {
    char error_str[MAX_ERROR_STR_LEN];
    char *out_filename = NULL;
    //
    //const uint8_t station_ID = 1;
    const uint8_t memory_ID  = 1;
    const uint8_t memory_section = 0;
    uint8_t monitor_mode = 0;
    // Serial interface
    char    *serial_port_name = "COM4";
    uint32_t serial_baud_rate = 0;
    // Interface selection
    char serial_selected = 0; // 0=not selected 1=serial

    printf( "NCM read counters %s\n", PROG_VERSION );

    // Validate and parse commands
    int cmd_idx;
    if( argc > 1 ) {
        for( cmd_idx = 1; cmd_idx < argc; ++cmd_idx ) {
            if( strcmp(argv[cmd_idx],"-ser") == 0 && argc > cmd_idx+2 ){
                serial_port_name = argv[++cmd_idx];
                if( serial_selected != 0 ) {
                    printf( "Only one communication interface permitted\n" );
                    return 0;
                }
                serial_baud_rate = atoi( argv[++cmd_idx] );
                serial_selected = 1;
            }
            else if( strcmp(argv[cmd_idx],"-m") == 0 )
                monitor_mode = 1;
            else if( strcmp(argv[cmd_idx],"-f") == 0 && argc > cmd_idx+1 )
                out_filename = argv[++cmd_idx];
            else
                printf( "cmd %i: %s\n", cmd_idx, argv[cmd_idx] );
        }

    }
    else if( argc == 1 ) {
        print_commands();
        return 0;
    }

    // -- Check conditions --
    if( serial_selected == 0 ) {
        printf( "No communication port selected\n" );
        return 0;
    }
    if( serial_baud_rate == 0 ) {
        printf( "Invalid baud rate\n" );
        return 0;
    }

    // ---- Open the input/output file ----
    FILE * out_file = NULL;
    if( out_filename != NULL ) {
        out_file = fopen( out_filename, "wb" );
        if( out_file == NULL ) {
            printf( "Error: Can't open file for write (%s)\n", out_filename );
            return -1;
        }
    }

    uint32_t mem_array[MAX_MEM_SIZE] = {0};
    uint32_t mem_size = 0;

    // -- Serial interface --
    uint8_t  payload_buffer[SERIAL_MAX_PAYLOAD_LEN] = { 0 };
    uint8_t  recv_buffer[SERIAL_MAX_PAYLOAD_LEN] = { 0 };
    uint32_t payload_len = 0;
    // Header
    payload_buffer[0] = SERIAL_START_CHAR ;
    payload_buffer[1] = memory_ID;
    payload_buffer[2] = memory_section;
    payload_buffer[3] = OP_READ_MEM;
    // Data
    memcpy( &payload_buffer[4], mem_array, mem_size*sizeof(uint32_t) );
    // Final length
    //payload_len = 4 + mem_size*sizeof(uint32_t);
    payload_len = 4 + 256*sizeof(uint32_t);

    // Open port
    if( SerialWrapper_OpenPort(serial_port_name,serial_baud_rate,3000,error_str) != 0 ) {
        printf( "Error: Unable to open serial port \"%s\"\n Info: \"%s\"", serial_port_name, error_str );
        return -1;
    }

    // Init log file
    if( out_file != NULL ) {
        fprintf( out_file, "time_s, total_tx_bytes_cnt, rx_RT_bytes_cnt, rx_bytes_non_RT_cnt, total_rx_bytes_cnt, rcv_cmd_fault_sec, synq_fault_s\n" );
    }


    Utils_timer_us_init();
    Utils_timer_us_start();

    while( 1 ) {
        //SerialWrapper_FlushRxBuff();
        // Send data (request)
        if( SerialWrapper_SendData(payload_buffer,payload_len) != 0 ) {
            printf( "Error: On sending data\n" );
            return -1;
        }
        // Receive data (response)
        if( SerialWrapper_RecvData(recv_buffer,payload_len-1) != 0 ) {
            printf( "Error: On receiving data\n" );
            return -1;
        }

        // Read operation
        Utils_clear_console();
        printf( "NCM read counters %s\n", PROG_VERSION );
        //print_uint32_array( (uint32_t*)&recv_buffer[3], MAX_MEM_SIZE, 8 );

        uint32_t elapsed_us = Utils_timer_us_elapsed();
        Utils_timer_us_start();
        print_counters( (uint32_t*)&recv_buffer[3], MAX_MEM_SIZE, 8, elapsed_us, out_file );

        Utils_sleep_ms( 200 );

        // Monitor mode
        if( monitor_mode == 0 ) {
            printf( "Memory contents read successfully\n" );
            break;
        }

    }

    // Close port
    SerialWrapper_ClosePort();


    return 0;
}
