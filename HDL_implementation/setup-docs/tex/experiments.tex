This document provides a general overview of the experimental setups used to demonstrate the achieved real-time properties of the Atacama framework. The reference application is real-time video streaming in a multi-hop setup, which enables easy visual assessment of the timing properties. All material required to implement and run the setups are accessible through the repository for the open-source framework. In this version, real-time capable stations are implemented using low-cost Atlys boards with a Spartan-6 FPGA, and real-time capable switches are implemented using NetFPGA-1G boards with Virtex-2 chips. All devices operate at 1 Gbit/s. The HDL designs contain specific code to interface the hardware provided in these boards. Porting to different hardware will require modifications in the HDL designs.

The following paragraphs provide a high-level overview of the physical setup and details of the prototypes. A more intuitive overview of the applications and videos summarizing the results are available in~\cite{supplemental-material}.

\subsection{Reference application}

The reference application consists of a video source station transmitting a scheduled \emph{reference stream} of periodic Ethernet frames encapsulating raw pixels of a complete video line. These reference Ethernet frames traverse a multi-hop network to reach remote displays. Remote displays perform the matching schedule to retrieve the pixels and store them into a real-time buffer. The real-time task in the displays reads the real-time buffer at the \emph{line-period} required for correct display of the specific video format. To stress the timing requirements, displays operate in a \emph{buffer-less} fashion: the real-time buffer only stores the latest line, which remains valid for one line-period. The video source, in turn, transmits the lines exactly at the required line-period, and thus any variation in the communication latency will degrade the quality of the displayed video. Table~\ref{table:reference-stream} summarizes the timing and bandwidth requirements for streaming a video of 640x480 pixels, 8-bit gray-scale encoding at 60 frames-per-second (fps). The video source also serves as the master station to synchronize the real-time domain, emitting periodical synchronization beacons encoded in Ethernet frames of minimum length. The slot sizing and the parameters required to design and verify the required Network Code schedules are derived from the latency models provided in~\cite{Atacama-FCCM}. \GC{pending: add a time diagram to describe the schedules used for each application}


\begin{table}\centering
\ra{1.3}
\caption{Timing and bandwidth requirements for the reference video stream}
\label{table:reference-stream}
\begin{tabular}{@{}lcc@{}}\toprule
 & Ref. Video &  Sync. \\
\midrule
Line period [$\mu s$]                         &  33   & 33    \\%\midrule
Ethernet frame length (with overhead) [Bytes] &  752  & 72    \\
Ethernet frames per second                    & 30300 & 30300 \\
Required bandwidth [Mbit/s]                   &  174  &  16   \\
\bottomrule
\end{tabular}
\vspace{0mm}
\end{table}

\subsection{Experiment 1: Bandwidth vs. timing guarantees}

Fig.~\ref{fig:experiment-1} shows the multi-hop setup used to validate the achieved timing guarantees for scheduled real-time frames in the video streaming application. Station $\textrm{S}_1$ is the source transmitting both the reference video stream and the synchronization frames for the real-time domain. On each line-period, $\textrm{S}_1$ transmits two consecutive frames with the same pixel line, but with different values in the EthType field denoting real-time (0x5CE0) and best-effort (0x0805) streams. Stations $\textrm{S}_3$ and $\textrm{S}_4$ execute the matching schedule to retrieve and display the pixels from the real-time and best-effort streams, respectively. The input classifier in the MAC of $\textrm{S}_4$ is tweaked to process incoming reference frames tagged as 0x0805 through the real-time path, but these frames still flow through the COTS stacks in the rest of the devices. This configuration allows us to characterize the effects of statistical arbitration in the timing of the best-effort reference stream.

\begin{figure}[t]
\centering
\includegraphics[scale=1]{figures/setup-experiment-1}
\caption{Experimental setup for real-time video streaming}
\label{fig:experiment-1}
\vspace{-3mm}
\end{figure}

The COST station $\textrm{C}$ injects unscheduled best-effort frames to generate congestion in the network. This station does not execute any real-time task, and it can be implemented using a regular workstation or a traffic injector with COTS NICs. Special care must be taken to ensure that the EthType field of injected frames is different from the ones reserved for real-time frames and reference streams. To magnify the effects of interference with a single COTS station, the unscheduled should also be broadcast. It is possible to generate different congestion scenarios using multiple COTS stations to inject traffic with different patterns. Station ${S}_4$ executes a predefined schedule that transmits a secondary real-time stream in coordination with the reference stream. This secondary stream allows to illustrate the effects of reduced best-effort bandwidth and interruption of conflicting low-priority frames.

Figs.~\ref{fig:Atlys-Tx} and~\ref{fig:Atlys-Rx} show a general diagram of the prototypes for video sources and remote displays. The current release include the complete editable projects and source files to verify, modify, and re-synthesize the designs from scratch. For convenience, pre-compiled and ready-to-run .bit files for the switches and real-time capable stations in the setup are also provided in the files \code{stable\_\_NetFPGA\_firmware.zip} and \code{stable\_\_demo\_0\_atlys\_firmware.zip}, respectively. Hardware projects use the term node instead of stations, but the functionality is the same. Each .bit file contains a default hard-coded memory configuration for the intended application (schedule, real-time buffers), so no additional configuration is needed. It is still possible to change the configuration of the real-time core using the provided software tools (check details about the NCP and memory space in related documentation). Real-time capable NICs include hardware support to monitor multiple runtime metrics.\GC{Pending: instructions on how to read the counters and their meaning}

\begin{figure}
  \centering
  \subfloat[Acknowledge mechanism during synchronization cycle]{\label{fig:Atlys-Tx}
  \includegraphics[scale=0.91]{figures/Atlys_TX}}
  \vspace{-1mm}
  
	\subfloat[Real-time path after synchronization cycle]{\label{fig:Atlys-Rx}
  \includegraphics[scale=0.91]{figures/Atlys_RX}}
  \vspace{-1mm}
  
	%%%%%%%%%%	
	\caption{Discovery of real-time devices and segmentation of real-time domains}
  \label{fig:Atlys-boards}
	\vspace{-3.5mm}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%

Even without connecting any HDMI source or display to the boards, the schedules will still run and transmit whatever content they find in the RAM memory. This will affect only the payload, but the real-time communication will still apply. Even without visualization of the video is still possible to verify the traffic patterns and timing properties using packet capture tools (e.g., Wireshark). More details and a summary of the results are available in~\cite{supplemental-material}.

\GC{summary of results have been hidden in this draft, and will be included later after passing the review process in current submissions.}


\subsection{Experiment 2: Segmentation and dynamic bandwidth allocation}

To be added...
%\begin{figure*}[t]
%\centering
%\includegraphics[scale=0.79]{figures/setup-experiment-2}
%\caption{Experimental setup for distributed video processing with dynamic bandwidth management and segmentation of real-time domains}
%\label{fig:experiment-2}
%\vspace{-2mm}
%\end{figure*}
%
%The setup in Fig.~\ref{fig:experiment-2} illustrates the segmentation and dynamic bandwidth allocation capabilities of the devices. In this case, $\textrm{RT}_1$ transmits a single real-time reference stream. Stations $\textrm{RT}_2$ and $\textrm{RT}_3$ are coordinated to receive interleaved pieces of the original video, apply a Sobel filter for border detection on their corresponding pieces of video, and then transmit the processed lines. Station $\textrm{RT}_4$ receives and displays the processed lines using a buffer-less configuration. Correct assembling of the video using lines from distributed sources puts an additional level of stress to the timing properties compared to the previous setup. 
%
%The schedules also use the \ac{TTL} to prevent unnecessarily broadcasting scheduled frames outside of their intended scope. $\textrm{RT}_1$ is the synchronization master for the entire network and generates synchronization frames with TTL=5 to cover all switches. Frames containing original video lines have a TTL=4, just enough to reach the distributed real-time video filters. Likewise, since the remote display is located only two switches away, the real-time filters emit frames with TTL=2.
%
%To illustrate the dynamic allocation of slots, schedules in real-time video filters skip transmission of processed video lines containing only black pixels. When writing the processed lines in the corresponding real-time buffer for transmission, the filtering task running in the host asserts a flag to indicate whether the line contains only black pixels. The schedule in the NIC then checks this flag, and use the \code{if} instruction to decide whether to transmit the line or leave the slot free until the next line period. The remote display will automatically show a black line whenever it detects that a scheduled line did not arrive. The bandwidth of the filtered video will then depend on the number of objects (borders) detected in the original video. A first experiment considers a set of synthetic videos generated from the three benchmarking images in Fig.~\ref{fig:experiment-2}, which produce different numbers of black lines when processed. Fig.~\ref{fig:experiment-2-results} summarizes the rate of received real-time data in stations located exclusively in the original video domain ($\textrm{RT}_2$), exclusively in the processed video domain ($\textrm{RT}_5$), and the intersection of both domains ($\textrm{RT}_4$). 
%
%The original video source always transmits all video lines, and thus the received data rate for $\textrm{RT}_2$ remains constant for all benchmark images. $\textrm{RT}_4$ and $\textrm{RT}_5$ share the domain with the real-time filters executing the conditional schedules, thus their total received data rates will vary depending on the number of black lines generated from the filtered benchmarking images. The received traffic in $\textrm{RT}_4$ accounts for the original video lines (fixed amount) and the non-black lines from the real-time filter processing the other half of the video (depends on input). The display $\textrm{RT}_5$ only receives non-black lines from the distributed video filters. All stations always receive a fixed amount of traffic required for synchronization frames. 
%
%In Fig.~\ref{fig:experiment-2}, a best-effort stream flowing from $\textrm{C}_1$ to $\textrm{RT}_6$ within the processed video domain is subject to the bandwidth limitations imposed by the scheduled frames from the video filters. A detailed experiment illustrating how the best-effort stream benefits from dynamic bandwidth allocation is available in the supplemental material~\cite{supplemental-material}.
%
%
%%Step & \multicolumn{2}{c} {RT2} &  \multicolumn{2}{c}{RT3} & C1\\
%%\cmidrule(lr){2-3} \cmidrule(lr){4-5}
%
%%\begin{table}\centering
%%\ra{1.3}
%%\caption{YYY Reception rate [Mbit/s]}
%%\label{table:exp2-results}
%%\begin{tabular}{@{}ccccccc@{}}\toprule
 %%Benchmark Image & \multicolumn{2}{c} {RT2} & \multicolumn{2}{c} {RT4} & \multicolumn{2}{c} {RT5} \\
%%\cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(l){6-7}
 %%&  RT   & BE  &  RT   & BE  &  RT   & BE \\
%%\midrule
%%1&  193  & 0   &  277  & 193 &  185  & 81  \\
%%2&  193  & 0   &  236  & 193 &  100  & 137 \\
%%3&  193  & 0   &  193  & 193 &  14   & 193 \\
%%\bottomrule
%%\end{tabular}
%%\vspace{3mm}
%%\end{table}
%
%\begin{figure}[t]
%\centering
%\includegraphics[scale=0.55]{figures/bandwidth_experiment_B}
%\caption{Rate of received real-time data for stations located in different domains and different benchmarking images (see the setup in Fig.~\ref{fig:experiment-2})}
%\label{fig:experiment-2-results}
%\vspace{-3mm}
%\end{figure}