library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ncm_package.all;

entity MC_NCP is
    Port ( clk                : in  STD_LOGIC;

			  tx_fifo_to_MAC		: out std_logic_vector(8 downto 0);
			  wr_to_mac_fifo		: out std_logic;
			  tx_sync_resp			: out std_logic_vector(8 downto 0);

		     rx_data_from_MAC	: in std_logic_vector(8 downto 0); 
		     rx_fifo_empty_from_MAC : in std_logic;
			  RT_sync_marker		: in std_logic;
			  transmiting_sync	: out std_logic;
			  rx_rden_to_MAC	   : out std_logic			  

           );
end MC_NCP;

architecture Behavioral of MC_NCP is

begin

   sndcmd0 : send_cmd port map (
      clk 					=> clk,
	
      rx_fifo_data 		=> rx_data_from_MAC, 
		rx_fifo_empty 		=> rx_fifo_empty_from_MAC, 
      rx_fifo_rden 		=> rx_rden_to_MAC,
		
      tx_en 				=> wr_to_mac_fifo,
      tx_data 				=> tx_fifo_to_MAC,
		tx_sync_resp		=> tx_sync_resp,
		RT_sync_marker		=> RT_sync_marker,
		transmiting_sync	=> transmiting_sync
   );
      
end Behavioral;