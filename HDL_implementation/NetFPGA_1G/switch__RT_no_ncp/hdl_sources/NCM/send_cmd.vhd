--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.ncm_package.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity send_cmd is
    Port ( clk : in  STD_LOGIC;
           rdy : out  STD_LOGIC;
           -- PDU data interface
           rx_fifo_data : in  STD_LOGIC_VECTOR (8 downto 0);
           rx_fifo_empty : in  STD_LOGIC;
           rx_fifo_rden : out  STD_LOGIC;
           -- tx fifo interface - 32 bit
           tx_en : out  STD_LOGIC;
           tx_data : out  STD_LOGIC_VECTOR (8 downto 0);
           tx_sync_resp : out  STD_LOGIC_VECTOR (8 downto 0);
			  RT_sync_marker: in std_logic;
			  transmiting_sync: out std_logic
			  );
end send_cmd;

architecture Behavioral of send_cmd is

	type states is (s_idle, s0, s1, s2, s3, s_rdy, s_wait, s_update_TTL);

	signal currentstate : states := s_idle;
   signal rx_fifo_rden_i : std_logic;  

begin
   rx_fifo_rden <= rx_fifo_rden_i;
   FSM: process (clk)
      variable nextstate : states;
      variable cnt : integer range 0 to 2047 := 0;

   begin
   
      if (rising_edge(clk)) then
         nextstate := currentstate;
      
         case currentstate is
         
            when s_idle =>
				   tx_data <= "111100000";
               rdy <= '0';
               tx_en <= '0';
               rx_fifo_rden_i <= '0';
					cnt := 0;
					transmiting_sync <= '0';
               
               if (rx_fifo_empty = '0') then
						 if (RT_sync_marker = '1') then
							transmiting_sync	<= '1';
						 end if;
						 
						 rx_fifo_rden_i <= '1';
						 nextstate := s0;
               else
						 nextstate := s_idle;
					end if;
					
				when s0 =>
						 nextstate := s1;

				when s1 =>
					tx_en <= '1';
					tx_data <=  rx_fifo_data;
					tx_sync_resp <= rx_fifo_data;
					nextstate := s2;
					
					if (rx_fifo_data(8) = '1' or rx_fifo_empty = '1') then
                   rx_fifo_rden_i <= '0';
						 nextstate := s_rdy;
					end if;

				when s2 =>
					tx_en <= '1';
					tx_data <=  rx_fifo_data;
					tx_sync_resp <= rx_fifo_data;
					cnt := cnt + 1;
					if (cnt = 13) then
						cnt := 0;
						tx_sync_resp <= "0" & X"AA";
						nextstate := s_update_TTL;
					elsif (rx_fifo_data(8) = '1' or rx_fifo_empty = '1') then
                   rx_fifo_rden_i <= '0';
						 nextstate := s_rdy;
					else
						 nextstate := s2;
					end if;
					
				when s_update_TTL =>
					tx_en <= '1';
					tx_data <=  rx_fifo_data - 1;
					nextstate := s3;

				when s3 =>
					tx_en <= '1';
					tx_data <=  rx_fifo_data;
					tx_sync_resp <= rx_fifo_data;
					
					if (rx_fifo_data(8) = '1' or rx_fifo_empty = '1') then
                   rx_fifo_rden_i <= '0';
						 nextstate := s_rdy;
					else 
						 nextstate := s3;
					end if;
					
            when s_rdy =>
					tx_data <= "111100000";
					transmiting_sync <= '0';
					tx_en <= '0';
               rdy <= '1';
               nextstate := s_idle;

            when others =>
               nextstate := s_idle;
         end case;
         
         currentstate <= nextstate;
         
      end if;
   end process;

end Behavioral;