`timescale 1ns / 1ps


module rx_signals_for_RT(

	input 			clk,
	input 			dvld,
	input 			RT_frame_marker,
	input 			reset,

	output reg 		eop,
	output reg		rx_fifo_wr_en,
	output reg 		reset_fifo
    );


   reg dvld_d1;
	reg [5:0]                       rx_state;
   reg [5:0]                       rx_state_nxt;
	
   parameter RX_IDLE            = 1;
   parameter RX_RCV_PKT         = 2;

	always @(*) begin

      rx_state_nxt           = rx_state;
      eop                    = 0;
		reset_fifo = 0;
		rx_fifo_wr_en = 0;
      case(rx_state)

        RX_IDLE: begin
           if(dvld_d1 && RT_frame_marker) begin
				  rx_fifo_wr_en   = 1;
				  rx_state_nxt    = RX_RCV_PKT;
           end
			  else begin
				  rx_state_nxt    = RX_IDLE;
			  end
		end
        
      RX_RCV_PKT: begin
           rx_fifo_wr_en = 1;
           if(!dvld) begin
              eop = 1;
              rx_state_nxt = RX_IDLE;
           end
			  else begin
				  rx_state_nxt = RX_RCV_PKT;
			  end
		 end
		  
        default: begin end

      endcase // case(rx_state)
   end // always @ (*)
	
   always @(posedge clk) begin
      if(reset) begin
         rx_state             <= RX_IDLE;
			dvld_d1              <= 0;
      end
      else begin
         rx_state           <= rx_state_nxt;
			dvld_d1            <= dvld;
      end
   end // always @ (posedge rxcoreclk)	
	
endmodule