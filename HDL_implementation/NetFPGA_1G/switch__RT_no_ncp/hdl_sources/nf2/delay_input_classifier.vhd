library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;
use work.ncm_package.all;

entity input_class_delay is
    Port ( clk : in  STD_LOGIC;
			  rst : in std_logic;
           -- emac interface
           
				gmac_rx_data_in 				: in std_logic_vector (7 downto 0);
				gmac_rx_data_out 				: out std_logic_vector (7 downto 0);
				gmac_rx_dvld_in 				: in std_logic;
				gmac_rx_dvld_out 				: out std_logic;
				gmac_rx_goodframe_in 		: in std_logic;
				gmac_rx_goodframe_out		: out std_logic;
				gmac_rx_badframe_in 			: in std_logic;
				gmac_rx_badframe_out 		: out std_logic;

				rx_nonRT_frame_counter 		: out std_logic_vector (31 downto 0);
				rx_data_frame_counter 		: out std_logic_vector (31 downto 0);
				rx_sync_frame_counter 		: out std_logic_vector (31 downto 0);
				rx_total_frame_counter 		: out std_logic_vector (31 downto 0);
				rx_ack_frame_counter 		: out std_logic_vector (31 downto 0);
				
				RT_data			 				: out std_logic;
				RT_sync			 				: out std_logic;
				RT_sync_ack						: out std_logic;
				rx_valid_sync_ID				: in 	std_logic;
				sync_ack_received				: out std_logic;
				sync_received					: out std_logic;
				sync_received_delay 			: out std_logic;
				current_sync_frame_ID		: out std_logic_vector (15 downto 0);
				check_sync_frame_ID			: out std_logic
           );
end input_class_delay;

architecture Behavioral of input_class_delay is

	type states is (s_init, s_idle, s_wait, s_dest, s_src, s_type_h, s_type_l, s_rdy, s_wait_for_count, s_retrieve_counter_h, s_retrieve_counter_l, s_sync_received, s_check_TTL);
   signal currentstate : states := s_init;
	signal RT_data_marker_temp 		: std_logic;
	signal RT_sync_marker_temp 		: std_logic;
	signal RT_data_marker 				: std_logic;
	signal RT_sync_marker 				: std_logic;
	signal RT_sync_valid 				: std_logic;
	signal RT_sync_ack_marker 			: std_logic;
	signal sync_ack_received_i 		: std_logic;
	signal forward_it						: std_logic;
	signal sync_received_i 				: std_logic;
	signal check_sync_frame_ID_i 		: std_logic;
	signal rx_nonRT_frame_counter_i 	: std_logic_vector (31 downto 0);
	signal rx_data_frame_counter_i 	: std_logic_vector (31 downto 0);
	signal rx_sync_frame_counter_i 	: std_logic_vector (31 downto 0);
	signal rx_total_frame_counter_i 	: std_logic_vector (31 downto 0);
	signal rx_ack_frame_counter_i 	: std_logic_vector (31 downto 0);
	
begin

rx_ack_frame_counter 	<= rx_ack_frame_counter_i;
rx_nonRT_frame_counter 	<= rx_nonRT_frame_counter_i;
rx_data_frame_counter 	<= rx_data_frame_counter_i;
rx_sync_frame_counter 	<= rx_sync_frame_counter_i;
rx_total_frame_counter 	<= rx_total_frame_counter_i;

sync_ack_received 		<= sync_ack_received_i;
sync_received				<= sync_received_i;
check_sync_frame_ID		<= check_sync_frame_ID_i;

RT_data_marker		<= RT_data_marker_temp and forward_it;
RT_sync_marker		<= RT_sync_marker_temp and forward_it and RT_sync_valid;

shiftreg1 : shiftreg
		generic map (n =>17)
		port map (
			d => RT_data_marker,
			clk => clk,
			rst => '0',
			q => RT_data
);

shiftreg2 : shiftreg
		generic map (n =>4)
		port map (
			d 		=> RT_sync_marker,
			clk 	=> clk,
			rst 	=> '0',
			q 		=> RT_sync
);

shiftreg3 : shiftreg
		port map (
			d 		=> RT_sync_ack_marker,
			clk 	=> clk,
			rst 	=> '0',
			q 		=> RT_sync_ack
);

shiftreg4 : shiftreg
		generic map (n =>4)
		port map (
			d 		=> sync_received_i,
			clk 	=> clk,
			rst 	=> '0',
			q 		=> sync_received_delay
);

shift_register : shift_ram_v9_0
		port map (
			d(15 downto 11)	=> "00000",
			d(10) 				=> gmac_rx_badframe_in,
			d(9)					=> gmac_rx_goodframe_in,
			d(8) 					=> gmac_rx_dvld_in,
			d(7 downto 0) 		=> gmac_rx_data_in,
			clk 					=> clk,
			ce 					=> '1',
			q(15 downto 11) 	=> open,
			q(10) 				=> gmac_rx_badframe_out,
			q(9) 					=> gmac_rx_goodframe_out,
			q(8) 					=> gmac_rx_dvld_out,
			q(7 downto 0) 		=> gmac_rx_data_out);		

   FSM : process(clk)
      variable nextstate : states := s_init;
      variable cnt : integer range 0 to 1500 := 0;
   begin

		if (rst = '1') then

        currentstate <= s_init;
         nextstate := s_init;
      
		elsif (rising_edge(clk)) then
        
		  nextstate := currentstate;
                  
         case currentstate is
     
            when s_init => 
					current_sync_frame_ID <= X"0000";
					RT_data_marker_temp <= '0';
					RT_sync_marker_temp <= '0';
					RT_sync_ack_marker <= '0';
					RT_sync_valid		 <= '0';
					sync_ack_received_i <= '0';
					sync_received_i <= '0';
					rx_nonRT_frame_counter_i <= (others => '0');
					rx_data_frame_counter_i <= (others => '0');
					rx_sync_frame_counter_i <= (others => '0');
               rx_total_frame_counter_i <= (others => '0');
					check_sync_frame_ID_i <= '0';

					cnt := 0;
               nextstate := s_idle;

            when s_idle =>
            
					cnt := 0;
               sync_ack_received_i <= '0';
               sync_received_i <= '0';
					check_sync_frame_ID_i <= '0';
					
					if (gmac_rx_dvld_in = '1') then        
                  cnt := cnt + 1;
						nextstate := s_dest;
						RT_data_marker_temp <= '0';
						RT_sync_marker_temp <= '0';
						RT_sync_valid		 <= '0';
						RT_sync_ack_marker <= '0';
						forward_it		  <= '0';
               end if;
					
            when s_dest =>            
               
					if (gmac_rx_dvld_in = '0') then
                     nextstate := s_wait;
               else
                     cnt := cnt + 1;

                     if (cnt = 6) then                   -- 6 bytes read
                           nextstate := s_src;
                           cnt := 0;
                     end if;
   
					end if;
                              
            when s_src =>                   
                  if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                  else
                     cnt := cnt + 1;

                     if (cnt = 6) then                   -- 6 bytes read
                           nextstate := s_type_h;
                           cnt := 0;
                     end if;
   
                  end if;
                  
            when s_type_h =>
  
               	if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_rx_data_in = NCM_ETH_TYPEH ) then
									nextstate := s_type_l;
							else
									rx_nonRT_frame_counter_i <= rx_nonRT_frame_counter_i +1;
									nextstate  := s_wait;
							end if;

						end if;
					
            when s_type_l =>        
         
						if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_rx_data_in = NCM_ETH_TYPE) then
									RT_data_marker_temp <= '1';
									rx_data_frame_counter_i <= rx_data_frame_counter_i +1;
									nextstate := s_check_TTL;
							end if;
                     
							if (gmac_rx_data_in = NCM_SYNC_TYPE ) then
									RT_sync_marker_temp 	<= '1';
									rx_sync_frame_counter_i <= rx_sync_frame_counter_i +1;
									nextstate := s_check_TTL;
							end if;

							if (gmac_rx_data_in = NCM_SYNC_RESPONSE ) then
									RT_sync_ack_marker <= '1';
									rx_ack_frame_counter_i <= rx_ack_frame_counter_i +1;
									sync_ack_received_i 	 <= '1';
									nextstate := s_wait;
							end if;
							
						end if;
						
            when s_check_TTL =>
					if (gmac_rx_data_in > 0) then
						forward_it <= '1';
						if (RT_sync_marker_temp = '1') then
							nextstate := s_wait_for_count;
						else
							nextstate := s_wait;
						end if;
					else
						nextstate := s_wait;
						RT_sync_marker_temp 	<= '0';
						RT_data_marker_temp <= '0';
						RT_sync_ack_marker <= '1';
					end if;
				
				when s_wait_for_count =>            
               
					if (gmac_rx_dvld_in = '0') then
                     nextstate := s_wait;
               else
                     cnt := cnt + 1;

                     if (cnt = 3) then                   
                           nextstate := s_retrieve_counter_h;
                           cnt := 0;
                     end if;
					end if;
					
				when s_retrieve_counter_h =>
					if (gmac_rx_dvld_in = '0') then
                     nextstate := s_wait;
               else
                     current_sync_frame_ID (15 downto 8) <= gmac_rx_data_in;
							nextstate := s_retrieve_counter_l;
					end if;

				when s_retrieve_counter_l =>
					if (gmac_rx_dvld_in = '0') then
                     nextstate := s_wait;
               else
                     current_sync_frame_ID (7 downto 0) <= gmac_rx_data_in;
							check_sync_frame_ID_i <= '1';
							nextstate := s_sync_received;
					end if;
					
            when s_sync_received =>            
               
					check_sync_frame_ID_i <= '0';
					if (gmac_rx_dvld_in = '0') then
                     nextstate := s_wait;
               else
                     cnt := cnt + 1;
							if (cnt >= 7) then
								if (rx_valid_sync_ID = '1') then
									sync_received_i	<= '1';
									RT_sync_valid 		<= '1';
								end if;
								nextstate := s_wait;
								cnt := 0;
							end if;
					end if;
					
            when s_wait => 
						sync_received_i	<= '0';
						sync_ack_received_i <= '0';
						if (gmac_rx_dvld_in = '0') then
						--if (gmac_rx_goodframe_in = '1' or gmac_rx_badframe_in = '1') then
                     nextstate := s_rdy;
						end if;
						
            when s_rdy =>        -- we made it
            
               rx_total_frame_counter_i <= rx_total_frame_counter_i +1;
					nextstate := s_idle;

            when others =>
               nextstate := s_idle;       -- to be safe
            
         end case;

         currentstate <= nextstate;

      end if;         
   end process;

end Behavioral;