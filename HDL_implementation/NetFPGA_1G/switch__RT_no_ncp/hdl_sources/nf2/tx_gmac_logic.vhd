library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--library opb_ncm_v2_00_a;
use work.ncm_package.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tx_gmac_logic is
    Port ( clk 						: in  STD_LOGIC;
           gmac_tx_ack 				: in  STD_LOGIC;
			  eop_RT 					: in std_logic;
			  tx_fifo_RT_empty 		: in std_logic;
			  reset						: in std_logic;
			 
			  gmac_tx_data_RT 		: in std_logic_vector (7 downto 0);
			  gmac_tx_data_BE 	: in std_logic_vector (7 downto 0);
			  gmac_tx_dvld_BE	: in std_logic;
			  available_for_BE	: out std_logic;
			  lock_for_BE		: in std_logic;
			  
			  gmac_tx_data 			: out std_logic_vector (7 downto 0);
			  gmac_tx_ack_BE 		: out std_logic;
			  gmac_tx_dvld 			: out std_logic;
			  tx_fifo_RT_rden			: out std_logic;
			  tx_fifo_reset			: out std_logic;
			  txfifo_eop_end_cnt		: out std_logic_vector (31 downto 0);
			  txfifo_empty_end_cnt	: out std_logic_vector (31 downto 0)
			  );
end tx_gmac_logic;


architecture Behavioral of tx_gmac_logic is

	type states is (s_idle, s_init, s_wait_IFG, s_wait_IFG2, s0, s1, s_rdy, s_lock, s_hold_reset);

	signal tx_fifo_RT_rden_i   		: std_logic;
   signal gmac_tx_dvld_RT 				: std_logic;	
   signal aux 								: std_logic;	

	signal currentstate 					: states := s_init;
	signal available_for_BE_i   : std_logic;
	signal txfifo_eop_end_cnt_i   	: std_logic_vector(31 downto 0);	
	signal txfifo_empty_end_cnt_i   	: std_logic_vector(31 downto 0);	

begin

	available_for_BE 			<= 	available_for_BE_i;

	gmac_tx_dvld 				<=  	gmac_tx_dvld_BE when (available_for_BE_i = '1') else
											gmac_tx_dvld_RT;
					  
	gmac_tx_ack_BE 			<=  	gmac_tx_ack when (available_for_BE_i = '1' ) else
											'0';

	gmac_tx_data 				<=  	gmac_tx_data_BE when (available_for_BE_i = '1' ) else
											gmac_tx_data_RT;


	tx_fifo_RT_rden 		<= tx_fifo_RT_rden_i or (gmac_tx_ack and aux);
   txfifo_eop_end_cnt 	<= txfifo_eop_end_cnt_i;
   txfifo_empty_end_cnt <= txfifo_empty_end_cnt_i;	
	
	FSM: process (clk)
      variable nextstate : states;
		variable cnt : integer range 0 to 2047 := 0;

   begin
	     if (reset = '1') then

			currentstate <= s_init;
         nextstate := s_init;
  
			elsif (rising_edge(clk)) then
				nextstate := currentstate;
      
				case currentstate is

					when s_init => 
						txfifo_eop_end_cnt_i <= (others => '0');
						txfifo_empty_end_cnt_i <= (others => '0');
						aux <= '0';
						tx_fifo_RT_rden_i <= '0';
						gmac_tx_dvld_RT <= '0';
						tx_fifo_reset <= '0';
						available_for_BE_i <= '0';
						cnt := cnt + 1;
						if (cnt >= 100) then
							nextstate := s_idle;
						end if;

					when s_idle =>
						cnt := 0;
						gmac_tx_dvld_RT <= '0';
						tx_fifo_RT_rden_i <= '0';
						tx_fifo_reset <= '0';
						available_for_BE_i <= '1';
						aux <= '0';
					
						if (tx_fifo_RT_empty = '0') then
							nextstate := s_wait_IFG;
							available_for_BE_i <= '0';
						else
							nextstate := s_idle;
						end if;
					
					when s_wait_IFG =>
						cnt := cnt + 1;
						if (cnt >= 80) then -- 12 IFG + 64 interrupted frame (worst-case) + 2 just in case
							cnt :=0;
							nextstate := s0;
							tx_fifo_RT_rden_i <= '1';
							gmac_tx_dvld_RT <= '1';
							aux <= '1';
						else
							nextstate := s_wait_IFG;
						end if;	
						
					when s0 =>
						tx_fifo_RT_rden_i <= '0';
					
						if (gmac_tx_ack = '1') then
							tx_fifo_RT_rden_i <= '1';
							nextstate := s1;
						else
							nextstate := s0;
						end if;	

					when s1 =>
				        cnt := 0;
						  if(eop_RT = '1') then
								tx_fifo_RT_rden_i <='0';
								gmac_tx_dvld_RT <='0';
								txfifo_eop_end_cnt_i <= txfifo_eop_end_cnt_i +1;
								tx_fifo_reset <= '1';
								--nextstate := s_wait_IFG2;
								nextstate := s_lock; --s_hold_reset;
						  elsif (tx_fifo_RT_empty = '1') then
								tx_fifo_RT_rden_i <='0';
								gmac_tx_dvld_RT <='0';
								txfifo_empty_end_cnt_i <= txfifo_empty_end_cnt_i +1;
								tx_fifo_reset <= '1';
								--nextstate := s_wait_IFG2;
								nextstate := s_lock; --hold_reset;
						  else
								nextstate := s1;
						  end if;

--					when s_hold_reset =>
--						cnt := cnt + 1;
--					
--						if (cnt >= 3) then
--							cnt :=0;
--							nextstate := s_lock; --s_rdy;
--							tx_fifo_reset <= '0';
--							--available_for_BE_i <= '1';
--						else
--							nextstate := s_hold_reset;
--						end if;	
				
--					when s_wait_IFG2 =>
--						cnt := cnt + 1;
--						if (cnt = 2) then
--							tx_fifo_reset <= '0';
--							nextstate := s_wait_IFG2;
--						elsif (cnt >= 15) then
--							cnt :=0;
--							nextstate := s_lock;
--						else
--							nextstate := s_wait_IFG2;
--						end if;	

					when s_lock =>
						tx_fifo_reset <= '0';
						if (lock_for_BE = '0') then
							nextstate := s_idle;
						elsif (tx_fifo_RT_empty = '0') then
							nextstate := s_wait_IFG;
							available_for_BE_i <= '0';
						else
							nextstate := s_lock;
						end if;
						
            when others =>
               nextstate := s_idle;
         end case;
         
         currentstate <= nextstate;
      end if;
   end process;

end Behavioral;