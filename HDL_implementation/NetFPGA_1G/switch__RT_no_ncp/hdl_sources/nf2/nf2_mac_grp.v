///////////////////////////////////////////////////////////////////////////////
// vim:set shiftwidth=3 softtabstop=3 expandtab:
// $Id: nf2_mac_grp.v 4444 2008-08-27 19:23:02Z grg $
//
// Module: nf2_mac_grp.v
// Project: NetFPGA Rev 2.1
// Description: Upper level module that instantiates the MAC FIFOs
//
///////////////////////////////////////////////////////////////////////////////

`include "registers.v"

  module nf2_mac_grp
    #(parameter DATA_WIDTH = 64,
      parameter CTRL_WIDTH=DATA_WIDTH/8,
      parameter ENABLE_HEADER = 0,
      parameter STAGE_NUMBER = 'hff,
      parameter PORT_NUMBER = 0
      )

   (// --- register interface
    input                                	mac_grp_reg_req,
    input                                	mac_grp_reg_rd_wr_L,
    input  [`MAC_GRP_REG_ADDR_WIDTH-1:0] 	mac_grp_reg_addr,
    input  [`CPCI_NF2_DATA_WIDTH-1:0]     mac_grp_reg_wr_data,
    output [`CPCI_NF2_DATA_WIDTH-1:0]     mac_grp_reg_rd_data,
    output                               	mac_grp_reg_ack,

    // --- output to data path interface
    output [DATA_WIDTH-1:0]              	out_data,
    output [CTRL_WIDTH-1:0]              	out_ctrl,
    output                               	out_wr,
    input                                	out_rdy,

	 output  [9-1:0]        			   	RT_data_from_rx_fifo,
    output                             	rx_fifo_empty_RT,
	 output											RT_sync_marker,
	 output											RT_data_marker,
	 output  										sync_ack_received,
	 output											sync_received,
	 output											sync_received_dly,
	 input  					               	rx_rden_RT,
	 output	[15:0]								current_sync_frame_ID,
	 output											check_sync_frame_ID,
	 input											rx_valid_sync_ID,

    // --- input from data path interface
    input [8:0]      							RT_data_to_tx_fifo,
	 input             							in_wr_RT,

    input  [DATA_WIDTH-1:0]              	in_data,
    input  [CTRL_WIDTH-1:0]              	in_ctrl,
    input                                	in_wr,
    output                               	in_rdy,

    // --- pins
    output [7:0]                         	gmii_tx_d,
    output                               	gmii_tx_en,
    output                               	gmii_tx_er,
    input                                	gmii_crs,
    input                                	gmii_col,
    input [7:0]                          	gmii_rx_d,
    input                                	gmii_rx_dv,
    input                                	gmii_rx_er,

    //--- misc
    input        									txgmiimiiclk,
    input        									rxgmiimiiclk,
    input        									clk,
    input        									reset,
	 
	 //------
	 
	 output [31:0] 								tx_non_RT_frame_counter,
	 output [31:0] 								tx_RT_data_frame_counter,
	 output [31:0] 								tx_RT_sync_frame_counter,
	 output [31:0] 								tx_RT_ack_frame_counter,
	 output [31:0] 								tx_RT_error_frame_counter,
	
	 output [31:0] 								rx_nonRT_frame_counter,
	 output [31:0] 								rx_data_frame_counter,
	 output [31:0] 								rx_sync_frame_counter,
	 output [31:0] 								rx_ack_frame_counter,
	 
	 output [31:0] 								rx_total_frame_counter,
	 output [31:0]			 						txfifo_eop_end_cnt,
	 output [31:0]			 						txfifo_empty_end_cnt,
	 output [31:0]			 						rx_nonRT_dropped_counter,
			 
	 output [31:0]   								vld_bytes_wr_to_RT_rxfifo,
	 output [31:0]	    							vld_bytes_rd_from_RT_txfifo,

	 output reg [31:0]                 		total_tx_bytes_cnt,	 
	 output reg [31:0]                 		total_rx_bytes_cnt,
	 output [31:0]									badframes_RT_data_cnt,	
	 output [31:0]									badframes_RT_sync_cnt,	
	 output [31:0]									goodframes_RT_data_cnt,	
	 output [31:0]									goodframes_RT_sync_cnt		 
    );


   wire          	disable_crc_check;
   wire          	disable_crc_gen;
   wire          	enable_jumbo_rx;
   wire          	enable_jumbo_tx;
   wire          	rx_mac_en;
   wire          	tx_mac_en;

   wire [7:0]     gmac_tx_data;
   wire [7:0]     gmac_rx_data;
   wire           reset_MAC;

   wire [1:0]     mac_speed               = 2'b10;       // set MAC speed to 1G, note: 10M and 100M are not supported

   wire [11:0]    tx_pkt_byte_cnt;
   wire [9:0]     tx_pkt_word_cnt;

   wire [11:0]    rx_pkt_byte_cnt;
   wire [9:0]     rx_pkt_word_cnt;
   wire           rx_pkt_pulled;

gig_eth_mac gig_eth_mac
(
  // Reset, clocks
  .reset 					(reset_MAC),    // asynchronous
  .tx_clk 					(txgmiimiiclk),
  .rx_clk 					(rxgmiimiiclk),

  // Run-time Configuration (takes effect between frames)
  .conf_tx_en 				(1'b1),
  .conf_rx_en 				(1'b1),
  .conf_tx_no_gen_crc 	(1'b0),
  .conf_rx_no_chk_crc 	(1'b0),
  .conf_tx_jumbo_en 		(1'b1),
  .conf_rx_jumbo_en 		(1'b1),

  // TX Client Interface
  .mac_tx_data 			(gmac_tx_data),
  .mac_tx_dvld 			(gmac_tx_dvld),
  .mac_tx_ack 				(gmac_tx_ack),
  .mac_tx_underrun 		(1'b0),

  // RX Client Interface
  .mac_rx_data 			(gmac_rx_data),
  .mac_rx_dvld 			(gmac_rx_dvld),
  .mac_rx_goodframe 		(gmac_rx_goodframe),
  .mac_rx_badframe 		(gmac_rx_badframe),

  // TX GMII Interface
  .gmii_txd 				(gmii_tx_d),
  .gmii_txen 				(gmii_tx_en),
  .gmii_txer 				(gmii_tx_er),

  // RX GMII Interface
  .gmii_rxd 				(gmii_rx_d),
  .gmii_rxdv 				(gmii_rx_dv),
  .gmii_rxer 				(gmii_rx_er)
);


   rx_queue
     #(.DATA_WIDTH(DATA_WIDTH),
       .CTRL_WIDTH(CTRL_WIDTH),
       .ENABLE_HEADER(ENABLE_HEADER),
       .STAGE_NUMBER(STAGE_NUMBER),
       .PORT_NUMBER(PORT_NUMBER)
       ) rx_queue
     (// data path interface
      .out_ctrl                         (out_ctrl),
      .out_wr                           (out_wr),
      .out_data                         (out_data),
      .out_rdy                          (out_rdy),
      // gmac interface
      .gmac_rx_data_temp                (gmac_rx_data),
      .gmac_rx_dvld_temp                (gmac_rx_dvld),
      .gmac_rx_goodframe_temp           (gmac_rx_goodframe),
      .gmac_rx_badframe_temp            (gmac_rx_badframe),
		
	   .RT_data_from_rx_fifo				 (RT_data_from_rx_fifo),
      .rx_fifo_empty_RT        			 (rx_fifo_empty_RT),
		.RT_sync_marker						 (RT_sync_marker),
		.RT_data_marker						 (RT_data_marker),
		.sync_ack_received					 (sync_ack_received),
		.sync_received			 		 		 (sync_received),
		.sync_received_dly	 		 	 	 (sync_received_dly),
		.rx_rden_RT_from_NCP				 	 (rx_rden_RT),
		.current_sync_frame_ID				 (current_sync_frame_ID),
		.check_sync_frame_ID					 (check_sync_frame_ID),
		.rx_valid_sync_ID						 (rx_valid_sync_ID),
      // reg signals
      .rx_pkt_good                      (rx_pkt_good),
      .rx_pkt_bad                       (rx_pkt_bad),
      .rx_pkt_dropped                   (rx_pkt_dropped),
      .rx_pkt_byte_cnt                  (rx_pkt_byte_cnt),
      .rx_pkt_word_cnt                  (rx_pkt_word_cnt),
      .rx_pkt_pulled                    (rx_pkt_pulled),
      .rx_queue_en                      (rx_queue_en),
      // misc
      .reset                            (reset),
      .clk                              (clk),
      .rxcoreclk                        (rxgmiimiiclk),
		
		.rx_nonRT_frame_counter 			 (rx_nonRT_frame_counter),
		.rx_data_frame_counter 				 (rx_data_frame_counter),
		.rx_sync_frame_counter 				 (rx_sync_frame_counter),
		.rx_total_frame_counter 			 (rx_total_frame_counter),
		.rx_ack_frame_counter 			 	 (rx_ack_frame_counter),		
		.rx_nonRT_dropped_counter			 (rx_nonRT_dropped_counter),
		.vld_bytes_wr_to_RT_rxfifo 		 (vld_bytes_wr_to_RT_rxfifo),
		.badframes_RT_data_cnt				 (badframes_RT_data_cnt),
		.badframes_RT_sync_cnt				 (badframes_RT_sync_cnt),		
		.goodframes_RT_data_cnt				 (goodframes_RT_data_cnt),
		.goodframes_RT_sync_cnt				 (goodframes_RT_sync_cnt)	
		);

   tx_queue
     #(.DATA_WIDTH(DATA_WIDTH),
       .CTRL_WIDTH(CTRL_WIDTH),
       .ENABLE_HEADER(ENABLE_HEADER),
       .STAGE_NUMBER(STAGE_NUMBER)
       ) tx_queue
     (// data path interface
      .RT_data_to_tx_fifo						(RT_data_to_tx_fifo),
		.in_wr_RT   								(in_wr_RT),	  
	  
      .in_ctrl                          	(in_ctrl),
      .in_wr                            	(in_wr),
      .in_data                          	(in_data),
      .in_rdy                           	(in_rdy),
      // gmac interface
      .gmac_tx_data                     	(gmac_tx_data),
      .gmac_tx_dvld                     	(gmac_tx_dvld),
      .gmac_tx_ack                      	(gmac_tx_ack),
      // reg signals
      .tx_queue_en                      	(tx_queue_en),
      .tx_pkt_sent                      	(tx_pkt_sent),
      .tx_pkt_stored                    	(tx_pkt_stored),
      .tx_pkt_byte_cnt                  	(tx_pkt_byte_cnt),
      .tx_pkt_word_cnt                  	(tx_pkt_word_cnt),
      // misc
      .reset                            	(reset),
      .clk                              	(clk),
      .txcoreclk                        	(txgmiimiiclk),
		
		/////////////
		.non_RT_frame_counter					(tx_non_RT_frame_counter),
		.RT_data_frame_counter					(tx_RT_data_frame_counter),
		.RT_sync_frame_counter					(tx_RT_sync_frame_counter),
		.RT_ack_frame_counter					(tx_RT_ack_frame_counter),
		.RT_error_frame_counter					(tx_RT_error_frame_counter),
		.txfifo_eop_end_cnt						(txfifo_eop_end_cnt),
		.txfifo_empty_end_cnt					(txfifo_empty_end_cnt),
		.vld_bytes_rd_from_RT_txfifo			(vld_bytes_rd_from_RT_txfifo),
		.txfifo_bytes_written_cnt  			()
		);
		
		
	   always @(posedge txgmiimiiclk) begin
			if(reset) begin
					total_tx_bytes_cnt   <=0;
          end
			 else begin
					if(gmii_tx_en) begin
						total_tx_bytes_cnt <= total_tx_bytes_cnt + 1;
					end
			end
		end

		always @(posedge rxgmiimiiclk) begin
			if(reset) begin
					total_rx_bytes_cnt   <=0;
          end
			 else begin
					if(gmac_rx_dvld) begin
						total_rx_bytes_cnt <= total_rx_bytes_cnt + 1;
					end
			end
		end		

   mac_grp_regs
     #(
        .CTRL_WIDTH(CTRL_WIDTH)
        ) mac_grp_regs
       (
        .mac_grp_reg_req                 	(mac_grp_reg_req),
        .mac_grp_reg_rd_wr_L             	(mac_grp_reg_rd_wr_L),
        .mac_grp_reg_addr                	(mac_grp_reg_addr),
        .mac_grp_reg_wr_data             	(mac_grp_reg_wr_data),

        .mac_grp_reg_rd_data             	(mac_grp_reg_rd_data),
        .mac_grp_reg_ack                 	(mac_grp_reg_ack),

        // interface to mac controller
        .disable_crc_check               	(disable_crc_check),
        .disable_crc_gen                 	(disable_crc_gen),
        .enable_jumbo_rx                 	(enable_jumbo_rx),
        .enable_jumbo_tx                 	(enable_jumbo_tx),
        .rx_mac_en                       	(rx_mac_en),
        .tx_mac_en                       	(tx_mac_en),
        .reset_MAC                       	(reset_MAC),

        // interface to rx queue
        .rx_pkt_good                     	(rx_pkt_good),
        .rx_pkt_bad                      	(rx_pkt_bad),
        .rx_pkt_dropped                  	(rx_pkt_dropped),
        .rx_pkt_byte_cnt                 	(rx_pkt_byte_cnt),
        .rx_pkt_word_cnt                 	(rx_pkt_word_cnt),
        .rx_pkt_pulled                   	(rx_pkt_pulled),

        .rx_queue_en                     	(rx_queue_en),

        // interface to tx queue
        .tx_queue_en                     	(tx_queue_en),
        .tx_pkt_sent                     	(tx_pkt_sent),
        .tx_pkt_stored                   	(tx_pkt_stored),
        .tx_pkt_byte_cnt                 	(tx_pkt_byte_cnt),
        .tx_pkt_word_cnt                 	(tx_pkt_word_cnt),

        .clk                             	(clk),
        .reset                           	(reset)
         );

endmodule // nf2_mac_grp