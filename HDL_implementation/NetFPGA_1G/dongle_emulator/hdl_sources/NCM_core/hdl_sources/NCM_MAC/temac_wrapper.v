`timescale 1ns / 1ps
module temac_wrapper(

			input 				reset,         
			input 				gmii_tx_clk,       
			input 				gmii_rx_clk,      
			input 				sys_clk,     
			input					run_NCM,
			input					control_reg_hit,

			output [7:0] 		gmii_txd,         
			output 				gmii_tx_en,  
			output 				gmii_tx_er, 
			input [7:0] 		gmii_rxd,     
			input 				gmii_rx_dv,
			input 				gmii_rx_er,

			input [15:0] 		NCSync_type,
			input [15:0] 		NCData_type,
			input [15:0] 		NCAck_type,

			input [35:0] 		tx_fifo_data,
			input 				tx_fifo_wren,

			output [35:0] 		rx_fifo_data,     
			output 				autorec_start,
			input 				rx_fifo_rden,

			output 				rx_fifo_empty,
			output 				rx_fifo_data_valid,

			output 				start_send_ack,
			output 				receiving_sync,

			input 				reset_rx_fifo,

		  output [31:0]      rx_goodframes_RT_data_cnt,
		  output [31:0]      rx_goodframes_RT_sync_cnt,
		  output [31:0]      rx_badframes_RT_data_cnt,
		  output [31:0]      rx_badframes_RT_sync_cnt,
		  output [63:0]      rx_bytes_RT_cnt,
		  output [31:0]      rx_goodframes_non_RT_cnt,
		  output [31:0]      rx_badframes_non_RT_cnt,
		  output [63:0]      rx_bytes_non_RT_cnt,
		  output [31:0] 		rx_good_frames_error_cnt,
		  output [31:0] 		rx_total_RT_data_frames_cnt,
		  output [31:0] 		rx_total_RT_sync_frames_cnt,
		  output [31:0] 		rx_total_RT_ack_frames_cnt,
		  output [31:0] 		rx_total_nonRT_frames_cnt,
		  
		  output [31:0] 		rx_goodframes_total_cnt,
		  output [31:0] 		rx_badframes_total_cnt,
		  
		  output [63:0]      running_time_tx_cnt,
		  output [63:0]      total_tx_bytes_cnt,	 
		  output [63:0]      running_time_rx_cnt,
		  output [63:0]      total_rx_bytes_cnt,
		  output	[31:0]		tx_fifo_overflow_cnt,
		  output	[31:0] 		bytes_read_from_txfifo_cnt		

);

wire [7:0] tx_data;
wire [7:0] rx_data;

phy_time_bytes_cnt tx_counter (
			  .clk									(gmii_tx_clk),
			  .run_NCM								(run_NCM),
			  .reset									(reset),
			  .control_reg_hit 					(control_reg_hit),
			  .en_bytes_cnt						(gmii_tx_en),

			  .running_time_cnt 					(running_time_tx_cnt[31:0]),
			  .total_bytes_cnt 					(total_tx_bytes_cnt[31:0])
	  );
	  
phy_time_bytes_cnt rx_counter (
			  .clk									(gmii_rx_clk),
			  .run_NCM								(run_NCM),
			  .reset									(reset),
			  .control_reg_hit 					(control_reg_hit),
			  .en_bytes_cnt						(gmii_rx_dv),

			  .running_time_cnt 					(running_time_rx_cnt[31:0]),
			  .total_bytes_cnt 					(total_rx_bytes_cnt[31:0])
	  );

gig_eth_mac gig_eth_mac
(
  // Reset, clocks
				.reset 					(reset),    // asynchronous
				.tx_clk 					(gmii_tx_clk),
				.rx_clk 					(gmii_rx_clk),

  // Run-time Configuration (takes effect between frames)
				.conf_tx_en 			(1'b1),
				.conf_rx_en 			(1'b1),
				.conf_tx_no_gen_crc 	(1'b0),
				.conf_rx_no_chk_crc 	(1'b0),
				.conf_tx_jumbo_en 	(1'b1),
				.conf_rx_jumbo_en 	(1'b1),

  // TX Client Interface
				.mac_tx_data 			(tx_data),
				.mac_tx_dvld 			(tx_valid),
				.mac_tx_ack 			(tx_ack),
				.mac_tx_underrun 		(1'b0),

  // RX Client Interface
				.mac_rx_data 			(rx_data),
				.mac_rx_dvld 			(rx_valid),
				.mac_rx_goodframe 	(rx_good_frame),
				.mac_rx_badframe 		(rx_bad_frame),

				// TX GMII Interface
				.gmii_txd  				(gmii_txd),
				.gmii_txen 				(gmii_tx_en),
				.gmii_txer 				(gmii_tx_er),

				// RX GMII Interface
				.gmii_rxd  				(gmii_rxd),
				.gmii_rxdv 				(gmii_rx_dv),
				.gmii_rxer 				(gmii_rx_er)
);

         
rx_queue rx_queue (
				.reset									(reset),
				.clk_ncm									(sys_clk),
				.reset_rx_fifo							(reset_rx_fifo),
           
				.rxcoreclk 								(gmii_rx_clk),
				.gmac_rx_data_temp       			(rx_data),
				.gmac_rx_dvld_temp 					(rx_valid),
				.gmac_rx_goodframe_temp 			(rx_good_frame),
				.gmac_rx_badframe_temp 				(rx_bad_frame),
				.rx_fifo_rden							(rx_fifo_rden),
				.start_send_ack						(start_send_ack),
				.receiving_sync						(receiving_sync),
				.data_from_rx_fifo_to_autorec 	(rx_fifo_data),
				.autorec_start							(autorec_start),
				.rx_fifo_empty              		(rx_fifo_empty),
				.rx_fifo_data_valid         		(rx_fifo_data_valid),
			  
				.rx_rden_from_autorec      		(1'b0),
				.rx_fifo_empty_RT          		(),
				.NCSync_type							(NCSync_type),
				.NCData_type							(NCData_type),				
				.NCAck_type								(NCAck_type),

			  .rx_goodframes_RT_data_cnt			(rx_goodframes_RT_data_cnt ),
			  .rx_goodframes_RT_sync_cnt 			(rx_goodframes_RT_sync_cnt ),
			  .rx_badframes_RT_data_cnt			(rx_badframes_RT_data_cnt),
			  .rx_badframes_RT_sync_cnt			(rx_badframes_RT_sync_cnt),
			  .rx_bytes_RT_cnt    					(rx_bytes_RT_cnt),
			  .rx_goodframes_non_RT_cnt   		(rx_goodframes_non_RT_cnt ), 
			  .rx_badframes_non_RT_cnt   			(rx_badframes_non_RT_cnt),
			  .rx_bytes_non_RT_cnt   				(rx_bytes_non_RT_cnt),
			  .rx_good_frames_error_cnt 			(rx_good_frames_error_cnt)	,
			  .rx_total_RT_data_frames_cnt		(rx_total_RT_data_frames_cnt),
			  .rx_total_RT_sync_frames_cnt		(rx_total_RT_sync_frames_cnt),
			  .rx_total_nonRT_frames_cnt	  		(rx_total_nonRT_frames_cnt),
			  .rx_goodframes_total_cnt 			(rx_goodframes_total_cnt),
			  .rx_badframes_total_cnt 				(rx_badframes_total_cnt)
	  );

    tx_queue tx_queue ( 
			.reset 										(reset),
			.run_NCM 									(run_NCM),	
			.tx_clk 										(gmii_tx_clk),
			.clk_ncm 									(sys_clk),
			.data_from_ncm_to_tx_fifo 				(tx_fifo_data),
			.tx_fifo_wren 								(tx_fifo_wren),
			.data_from_tx_fifo_to_mac 				(tx_data),
			.tx_data_valid_to_mac 					(tx_valid),
			.tx_ack_from_mac_to_fifo		 		(tx_ack),
			  .tx_fifo_overflow_cnt				(tx_fifo_overflow_cnt),
			  .tx_bytes_read_cnt					(bytes_read_from_txfifo_cnt)
      );	  
	  
endmodule