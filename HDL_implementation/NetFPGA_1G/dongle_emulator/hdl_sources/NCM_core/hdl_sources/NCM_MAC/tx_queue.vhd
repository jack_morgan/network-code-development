
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:24:44 06/05/2008 
-- Design Name: 
-- Module Name:    temac_fifos_v2pro - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.ncm_package.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tx_queue is
        port( 
		     reset 								: in std_logic;
			  run_NCM							: in std_logic;
		  
		     tx_clk 							: in  STD_LOGIC;
			  clk_ncm							: in std_logic;
			  data_from_ncm_to_tx_fifo		: in std_logic_vector (35 downto 0);
			  tx_fifo_wren						: in std_logic;
			  data_from_tx_fifo_to_mac		: out std_logic_vector (0 to 7);
			  tx_data_valid_to_mac			: out std_logic;
			  tx_ack_from_mac_to_fifo		: in std_logic;			  
			  tx_fifo_overflow_cnt 			: out std_logic_vector (31 downto 0);
			  tx_bytes_read_cnt				: out std_logic_vector (31 downto 0)
			  );
			  
end tx_queue;

architecture Behavioral of tx_queue is

signal tx_fifo_rden: 					std_logic;
signal tx_fifo_rden_aux: 				std_logic;
signal tx_fifo_empty: 					std_logic;
signal tx_fifo_empty_deb: 					std_logic;
signal tx_fifo_full: 					std_logic;
signal tx_data_valid_to_mac_i: 		std_logic;
signal eop: 								std_logic;
signal tx_fifo_overflow_cnt_i: 		std_logic_vector (31 downto 0) := (others => '0');
signal tx_bytes_read_cnt_i: 			std_logic_vector (31 downto 0) := (others => '0');
signal tx_data_valid_to_mac_temp: 	std_logic;



type states is (s_init, s_empty_trash, s_IFG, s_idle, s_wait_for_ack, s_wait_for_eop, s_wait_for_first_byte, s_wait_init);
signal currentstate : states := s_init;
	
begin

tx_fifo_overflow_cnt 	<= tx_fifo_overflow_cnt_i;
tx_bytes_read_cnt 		<= tx_bytes_read_cnt_i;


tx_overflow : process (clk_ncm, reset)
   begin
     if (reset = '1') then
			tx_fifo_overflow_cnt_i <= (others => '0');
			
	  elsif (rising_edge(clk_ncm)) then
         if (tx_fifo_full = '1' and run_NCM = '1') then
				tx_fifo_overflow_cnt_i <= tx_fifo_overflow_cnt_i + 1;
			end if;
		end if;
	end process;


tx_bytes_read : process (tx_clk, reset)
   begin
     if (reset = '1') then
			tx_bytes_read_cnt_i <= (others => '0');
			
	  elsif (rising_edge(tx_clk)) then
         if (tx_fifo_rden = '1' and tx_data_valid_to_mac_temp='1') then
				tx_bytes_read_cnt_i <= tx_bytes_read_cnt_i + 1;
			end if;
		end if;
	end process;


clientemac_txctrl: process (tx_clk, reset)
   variable nextstate 	: states;
	variable ifg			: integer range 0 to 100 := 0;
	variable init_delay 	: integer range 0 to 100 := 0;
   begin
     if (reset = '1') then
			init_delay := 0;
			tx_fifo_rden_aux <='0';
			tx_data_valid_to_mac_i <='0';
			nextstate := s_init;
			currentstate <= s_init;
	  
	  elsif (rising_edge(tx_clk)) then
         nextstate := currentstate;
      
         case currentstate is
         
					when s_init =>
						tx_fifo_rden_aux <='0';
						tx_data_valid_to_mac_i <='0';
						init_delay := init_delay + 1;
						if (init_delay >= 100) then
							init_delay := 0;
							nextstate := s_idle;
						end if;

					when s_idle =>
						ifg := 0;
						tx_fifo_rden_aux <= '0';
						tx_data_valid_to_mac_i<= '0';
						if (tx_fifo_empty = '0' and run_NCM='1') then					
								tx_fifo_rden_aux <= '1';
								nextstate := s_wait_for_first_byte;
						end if;

					when s_wait_for_first_byte =>
						tx_fifo_rden_aux <='0';
						tx_data_valid_to_mac_i <='1';
						nextstate := s_wait_for_ack;
									
					when s_wait_for_ack =>
						tx_fifo_rden_aux <='0';
						if tx_ack_from_mac_to_fifo ='1' then
							tx_fifo_rden_aux <='1';
							nextstate := s_wait_for_eop;
						end if;
					
					when s_wait_for_eop =>
						tx_fifo_rden_aux <='1';

						if (eop = '1' or tx_fifo_empty = '1' ) then			
							tx_data_valid_to_mac_i <='0';
							nextstate := s_empty_trash;
						end if;
					
					when s_empty_trash =>
					   if (tx_fifo_empty = '1' ) then			
							tx_fifo_rden_aux <='0';
							nextstate := s_IFG;
						end if;
					
					when s_IFG =>
						tx_fifo_rden_aux <='0';
						if (ifg = 12) then         -- originally 96 bit times, but we spend time somewhere else, too
							nextstate := s_idle;
						end if;               
						ifg := ifg + 1;
				
					when others =>
						nextstate := s_idle;
					
         end case;
         
         currentstate <= nextstate;
      end if;
         
end process;

tx_fifo_rden 					<= (tx_fifo_rden_aux or tx_ack_from_mac_to_fifo);
tx_data_valid_to_mac 		<= tx_data_valid_to_mac_i and (not eop);
tx_data_valid_to_mac_temp 	<= tx_data_valid_to_mac_i and (not eop);

	tx_fifo : fifo_36x9
  PORT MAP (
    din               => data_from_ncm_to_tx_fifo(35 downto 0),
    rd_clk            => tx_clk,
    rd_en             => tx_fifo_rden,
    rst               => reset,
    wr_clk            => clk_ncm,

    wr_en             => tx_fifo_wren,

    dout(7 downto 0)  => data_from_tx_fifo_to_mac,
    dout(8) 			 => eop,
	 
	 empty 				 => tx_fifo_empty,
	 full 				 => tx_fifo_full
			);
			
end Behavioral;