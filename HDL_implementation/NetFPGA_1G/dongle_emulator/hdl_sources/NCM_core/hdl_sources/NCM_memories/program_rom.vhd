
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

----------------------------------------------------------------------------------
-- Company: 		 FHWN
-- Engineer: 		 TR
-- 
-- Create Date:    21:36:01 10/27/2006 
-- Design Name: 	 NCM basic
-- Module Name:    program_rom - Behavioral 
-- Project Name: 	 NCM
-- Target Devices: 
-- Tool versions: 
-- Description: 	the program rom for the Network Code Machine
--						dual port - A for cpu bus R/W
--										B for NCM processor RO
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity program_rom is
    Port ( clk_ncm : in  STD_LOGIC;
			  clk_ext_bus : in  STD_LOGIC;
           bus_address : in  STD_LOGIC_VECTOR (7 downto 0);
           bus_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           bus_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           bus_rw : in  STD_LOGIC;
           ncm_address : in  STD_LOGIC_VECTOR (7 downto 0);
           ncm_prog_data : out  STD_LOGIC_VECTOR (31 downto 0));
end program_rom;

architecture Behavioral of program_rom is

	--signal wea : std_logic_vector(3 downto 0);
	--signal wea : std_logic;
	signal addressa : std_logic_vector(8 downto 0);
	signal addressb : std_logic_vector(8 downto 0);

begin

	--wea <= bus_rw;-- & bus_rw & bus_rw & bus_rw;
	addressa <= '0' & bus_address; --"00" & bus_address & "00000";
	addressb <= '0' & ncm_address; --"00" & ncm_address & "00000";

	PROG_ROM : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE

-- The following INIT_xx declarations specify the initial contents of the RAM
		
      -- for testing purposes the following program is contained in the rom:
      -- 00:   L0:   CREATE   V1          10 00 01 00
      -- 01:         SEND     #1          20 01 00 00
      -- 02:         FUTURE   1, L1       50 40 00 01
      -- 03:         HALT                 40 00 00 00
      -- 04:   L1:   RECEIVE  #1, V2      30 01 02 00
      -- 05:         FUTURE   1, L2       50 90 00 01
      -- 06:         HALT                 40 00 00 00
      -- 07:         NOP                  00 00 00 00
      -- 08:         NOP                  00 00 00 00
      -- 09:   L2:   COUNT INC            84 00 00 00
      -- 0A:         IF (count = 10), L3  70 C2 00 0A
      -- 0B:         IF (true), L0        70 07 00 00
      -- 0C:   L3:   SYNC ACT             F8 00 00 00
      -- 0D:         COUNT RES            81 00 00 00
      -- 0E:         IF (true), L0        70 07 00 00
      -- 0F:         COUNT DEC            82 00 00 00
      -- 10:         IF (true), L0        70 07 00 00
		
--		70950100;  //if (queue on var 1 is empty) then jump to 9
--10000100;  //create from var 1
--20000000;  //send using channel 0
--
--5050FFFF;  //just a couple of delay statements
--40000000;
--5070FFFF;
--40000000;
--5000FFFF;  //jump to 0 and start again
--40000000;  
--5090FFFF;  // if the queue was empty, system stay in this infinite loop
--40000000
		
		
      --           07      06      05      04      03      02      01      00
      --INIT_00 => X"400000005080FFFF400000005060FFFF400000005040FFFF400000005020FFFF",
		INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000")

   port map (
      DOA => bus_data_out,      -- 32-bit A port Data Output
      DOB => ncm_prog_data,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => X"00000000",      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => '1',      -- 1-bit  A port Enable Input
      ENB => '1',      -- 1-bit  B port Enable Input

      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => bus_rw,      -- 4-bit  A port Write Enable Input
      WEB => '0'       -- 4-bit  B port Write Enable Input
   );

end Behavioral;