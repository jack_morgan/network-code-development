
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

---------------------------------------------------------------------------------
-- Company: 		 FHWN
-- Engineer: 		 TR
-- 
-- Create Date:    21:36:01 10/27/2006 
-- Design Name: 	 NCM basic
-- Module Name:    variable_ram - Behavioral 
-- Project Name: 	 NCM
-- Target Devices: 
-- Tool versions: 
-- Description: 	the shared variable space for the Network Code Machine
--						dual port - A for cpu bus R/W
--										B for NCM processor R/W
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity variable_ram is
    Port ( clk_ncm : in  STD_LOGIC;
			  clk_ext_bus : in  STD_LOGIC;
           -- interface to uP
           bus_address : in  STD_LOGIC_VECTOR (15 downto 0);
           bus_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           bus_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           bus_rw : in  STD_LOGIC;
           -- interface to NCM
           ncm_address : in  STD_LOGIC_VECTOR (15 downto 0);
           ncm_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           ncm_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           ncm_rw : in  STD_LOGIC);
end variable_ram;

architecture Behavioral of variable_ram is

	signal wea : std_logic; --_vector(3 downto 0);
	signal web : std_logic; --_vector(3 downto 0);
	signal addressa : std_logic_vector(8 downto 0);
	signal addressb : std_logic_vector(8 downto 0);
	
	signal ena : std_logic_vector(7 downto 0);		-- max 8 blocks
	signal enb : std_logic_vector(7 downto 0);		-- max 8 blocks

	signal bus_data_out_0 : std_logic_vector(31 downto 0);
	signal bus_data_out_1 : std_logic_vector(31 downto 0);
	signal bus_data_out_2 : std_logic_vector(31 downto 0);
	signal bus_data_out_3 : std_logic_vector(31 downto 0);
	signal bus_data_out_4 : std_logic_vector(31 downto 0);
	signal bus_data_out_5 : std_logic_vector(31 downto 0);
	signal bus_data_out_6 : std_logic_vector(31 downto 0);
	signal bus_data_out_7 : std_logic_vector(31 downto 0);

	signal ncm_data_out_0 : std_logic_vector(31 downto 0);
	signal ncm_data_out_1 : std_logic_vector(31 downto 0);
	signal ncm_data_out_2 : std_logic_vector(31 downto 0);
	signal ncm_data_out_3 : std_logic_vector(31 downto 0);
	signal ncm_data_out_4 : std_logic_vector(31 downto 0);
	signal ncm_data_out_5 : std_logic_vector(31 downto 0);
	signal ncm_data_out_6 : std_logic_vector(31 downto 0);
	signal ncm_data_out_7 : std_logic_vector(31 downto 0);

begin

	-- write signals
	wea <= bus_rw;-- & bus_rw & bus_rw & bus_rw;
	web <= ncm_rw;-- & ncm_rw & ncm_rw & ncm_rw;
	
	-- address vectors per block
	addressa <= bus_address(8 downto 0); --"0" & bus_address(8 downto 0) & "00000";	-- 512 / block
	addressb <= ncm_address(8 downto 0); --"0" & ncm_address(8 downto 0) & "00000";	-- 512 / block
	
	-- block decoder
	ena <= 	"00000001" when bus_address(11 downto 9) = "000" else
	       	"00000010" when bus_address(11 downto 9) = "001" else
	       	"00000100" when bus_address(11 downto 9) = "010" else
	       	"00001000" when bus_address(11 downto 9) = "011" else
	       	"00010000" when bus_address(11 downto 9) = "100" else
	       	"00100000" when bus_address(11 downto 9) = "101" else
	       	"01000000" when bus_address(11 downto 9) = "110" else
	       	"10000000" when bus_address(11 downto 9) = "111" else
	       	"00000000";

	enb <= 	"00000001" when ncm_address(11 downto 9) = "000" else
	       	"00000010" when ncm_address(11 downto 9) = "001" else
	       	"00000100" when ncm_address(11 downto 9) = "010" else
	       	"00001000" when ncm_address(11 downto 9) = "011" else
	       	"00010000" when ncm_address(11 downto 9) = "100" else
	       	"00100000" when ncm_address(11 downto 9) = "101" else
	       	"01000000" when ncm_address(11 downto 9) = "110" else
	       	"10000000" when ncm_address(11 downto 9) = "111" else
	       	"00000000";

	-- data bus multiplexer
	bus_data_out <=	bus_data_out_0  when bus_address(11 downto 9) = "000" else
			bus_data_out_1  when bus_address(11 downto 9) = "001" else
			bus_data_out_2  when bus_address(11 downto 9) = "010" else
			bus_data_out_3  when bus_address(11 downto 9) = "011" else
			bus_data_out_4  when bus_address(11 downto 9) = "100" else
			bus_data_out_5  when bus_address(11 downto 9) = "101" else
			bus_data_out_6  when bus_address(11 downto 9) = "110" else
			bus_data_out_7  when bus_address(11 downto 9) = "111" else
			(others => '0');

	ncm_data_out <=	ncm_data_out_0  when ncm_address(11 downto 9) = "000" else
			ncm_data_out_1  when ncm_address(11 downto 9) = "001" else
			ncm_data_out_2  when ncm_address(11 downto 9) = "010" else
			ncm_data_out_3  when ncm_address(11 downto 9) = "011" else
			ncm_data_out_4  when ncm_address(11 downto 9) = "100" else
			ncm_data_out_5  when ncm_address(11 downto 9) = "101" else
			ncm_data_out_6  when ncm_address(11 downto 9) = "110" else
			ncm_data_out_7  when ncm_address(11 downto 9) = "111" else
			(others => '0');

	BLOCK0 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
		
      -- The following INIT_xx declarations specify the initial contents of the RAM
      --                  7       6       5       4       3       2       1       0 
      -- cfg_rom:X"000A000F00080002000600020004000200030001000200010001000100000001",

      INIT_00 => X"06000002060000010500000104000001000000010200000101000001FFFFFFFF",
      INIT_01 => X"0700000607000005070000040700000307000002070000010800000208000001",
      INIT_02 => X"0700000E0700000D0700000C0700000B0700000A070000090700000807000007",
      INIT_03 => X"00000000000000000000000000000000CCCCCCCCDDDDDDDDEEEEEEEE0700000F",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0220002001100010044000400330003002200020011000100000000000110011")

   port map (

      DOA => bus_data_out_0,      -- 32-bit A port Data Output
      DOB => ncm_data_out_0,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(0),      -- 1-bit  A port Enable Input
      ENB => enb(0),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK1 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST" -- WRITE_FIRST, READ_FIRST or NO_CHANGE
      )

   port map (

      DOA => bus_data_out_1,      -- 32-bit A port Data Output
      DOB => ncm_data_out_1,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(1),      -- 1-bit  A port Enable Input
      ENB => enb(1),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK2 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST" -- WRITE_FIRST, READ_FIRST or NO_CHANGE
      )
   port map (

      DOA => bus_data_out_2,      -- 32-bit A port Data Output
      DOB => ncm_data_out_2,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(2),      -- 1-bit  A port Enable Input
      ENB => enb(2),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK3 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST" -- WRITE_FIRST, READ_FIRST or NO_CHANGE
)
   port map (
      DOA => bus_data_out_3,      -- 32-bit A port Data Output
      DOB => ncm_data_out_3,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(3),      -- 1-bit  A port Enable Input
      ENB => enb(3),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK4 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST") -- WRITE_FIRST, READ_FIRST or NO_CHANGE

   port map (
      DOA => bus_data_out_4,      -- 32-bit A port Data Output
      DOB => ncm_data_out_4,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(4),      -- 1-bit  A port Enable Input
      ENB => enb(4),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK5 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST" -- WRITE_FIRST, READ_FIRST or NO_CHANGE
)
   port map (

      DOA => bus_data_out_5,      -- 32-bit A port Data Output
      DOB => ncm_data_out_5,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(5),      -- 1-bit  A port Enable Input
      ENB => enb(5),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK6 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST" -- WRITE_FIRST, READ_FIRST or NO_CHANGE
)
   port map (
      DOA => bus_data_out_6,      -- 32-bit A port Data Output
      DOB => ncm_data_out_6,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(6),      -- 1-bit  A port Enable Input
      ENB => enb(6),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

	BLOCK7 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST" -- WRITE_FIRST, READ_FIRST or NO_CHANGE
)
   port map (
      DOA => bus_data_out_7,      -- 32-bit A port Data Output
      DOB => ncm_data_out_7,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_data_in,      -- 32-bit B port Data Input
      DIPA => "0000",    -- 4-bit  A port parity Input
      DIPB => "0000",    -- 4-bit  B port parity Input
      ENA => ena(7),      -- 1-bit  A port Enable Input
      ENB => enb(7),      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => wea,      -- 4-bit  A port Write Enable Input
      WEB => web       -- 4-bit  B port Write Enable Input
   );

end Behavioral;