//`default_nettype none
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// TODO: Add base address
module NCM_mem_manager (
        clk,
        start_op,
        busy,
        op_buffer_addr,
        op_buffer_we,
        op_buffer_data_in,
        op_buffer_data_out,
        //
        all_mems_addr,
        all_mems_data_in,
        // Memories interface
        mem0_we,
        mem0_data_out,
        mem1_we,
        mem1_data_out,
        mem2_we,
        mem2_data_out
    );


// ---------- INCLUDES ----------


// ---------- PARAMETERS ----------
    parameter MEMS_WORD_BITS = 32;
    parameter MEMS_ADDR_BITS = 8;


// ---------- LOCAL PARAMETERS ----------


// ---------- INPUTS AND OUTPUTS ----------
    // Clock
    input wire                      clk;
    // Start operation
    input                           start_op;
    output reg                      busy              = 0;
    // Operation buffer
    output reg [10:0]               op_buffer_addr    = 0;
    output reg                      op_buffer_we      = 0;
    output reg [7:0]                op_buffer_data_in = 0;
    input wire [7:0]                op_buffer_data_out;
    //
    output reg [MEMS_ADDR_BITS-1:0] all_mems_addr    = 0;
    output reg [MEMS_WORD_BITS-1:0] all_mems_data_in = 0;
    // Memories interface
    output reg                      mem0_we = 0;
    input wire [MEMS_WORD_BITS-1:0] mem0_data_out;
    output reg                      mem1_we = 0;
    input wire [MEMS_WORD_BITS-1:0] mem1_data_out;
    output reg                      mem2_we = 0;
    input wire [MEMS_WORD_BITS-1:0] mem2_data_out;


// ---------- MODULE ----------

    // Control variables
    reg [1:0] memory_ID     = 0;
    reg [7:0] mem_sector    = 0;
    reg [7:0] operation_req = 0;

    // -- Memory multiplexors
    reg        mux_mem_we = 0;
    reg [31:0] mux_mem_data_out;
    
    always @(*) begin
        case( memory_ID[1:0] )
            0: begin
                mux_mem_data_out <= mem0_data_out;
                mem0_we <= mux_mem_we;
                mem1_we <= 0;
                mem2_we <= 0;
            end
            1: begin 
                mux_mem_data_out <= mem1_data_out;
                mem0_we <= 0;
                mem1_we <= mux_mem_we;
                mem2_we <= 0;
            end
            2: begin
                mux_mem_data_out <= mem2_data_out;
                mem0_we <= 0;
                mem1_we <= 0;
                mem2_we <= mux_mem_we;
            end
            default: begin 
                mux_mem_data_out <= 0;
                mem0_we <= 0;
                mem1_we <= 0;
                mem2_we <= 0;
            end
        endcase
    end
    

    // -- FSM
    reg [3:0]  ctrl_state = 0;
    reg [2:0]  word_bytes_counter = 0;
    reg [7:0]  words_counter = 0;

    always @( posedge clk ) begin
    
        // State 0: Wait for start command
        if( ctrl_state == 0 ) begin
            mux_mem_we         <= 0;
            all_mems_addr      <= 0;
            word_bytes_counter <= 0;
            op_buffer_we       <= 0;
            if( start_op ) begin
                ctrl_state     <= 1;
                busy           <= 1;
                op_buffer_addr <= op_buffer_addr + 1'b1;
            end
            else begin
                op_buffer_addr <= 0;
                busy           <= 0;
            end
        end
        
        // State 1: Read "memory ID"
        if( ctrl_state == 1 ) begin
            memory_ID      <= op_buffer_data_out[1:0];
            //
            op_buffer_addr <= op_buffer_addr + 1'b1;
            ctrl_state     <= 2;
        end
        
        // State 2: Read "memory sector"
        if( ctrl_state == 2 ) begin
            mem_sector     <= op_buffer_data_out;
            //
            op_buffer_addr <= op_buffer_addr + 1'b1;
            ctrl_state     <= 3;
        end
        
        // State 3: Read "operation requested"
        if( ctrl_state == 3 ) begin
            operation_req  <= op_buffer_data_out;
            //
            //op_buffer_addr <= op_buffer_addr + 1'b1;
            ctrl_state     <= 4;
        end
        
        // State 4: Determine operation type
        if( ctrl_state == 4 ) begin
            case( operation_req )
                0: begin // Write
                   ctrl_state     <= 5;
                   op_buffer_addr <= op_buffer_addr + 1'b1;
                end
                1: begin // Read
                    ctrl_state     <= 6;
                    op_buffer_addr <= op_buffer_addr - 1'b1;
                end
                default: ctrl_state <= 0; // Invalid code
            endcase
        end

        // State 5: Write operation
        if( ctrl_state == 5 ) begin
            // Assemble 32 bit word from little-endian byte order
            if( word_bytes_counter == 0 )
                all_mems_data_in[7:0] <= op_buffer_data_out;
            if( word_bytes_counter == 1 )
                all_mems_data_in[15:8] <= op_buffer_data_out;
            if( word_bytes_counter == 2 )
                all_mems_data_in[23:16] <= op_buffer_data_out;
            // Last byte
            if( word_bytes_counter == 3 ) begin
                all_mems_data_in[31:24] <= op_buffer_data_out;
                mux_mem_we              <= 1;
                // Reset bytes counter
                word_bytes_counter <= 0;
                // Update counters
                words_counter      <= words_counter + 1'b1;
                // Operation finished
                if( words_counter == 255 ) begin
                    ctrl_state <= 0;
                end
                else begin
                    op_buffer_addr <= op_buffer_addr + 1'b1;
                end
            end
            else begin
                mux_mem_we         <= 0;
                op_buffer_addr     <= op_buffer_addr + 1'b1;
                word_bytes_counter <= word_bytes_counter + 1'b1;
            end
            // "Big" memory address
            if( mux_mem_we )
                all_mems_addr <= all_mems_addr + 1'b1;
            
        end // state
        
        // State 6: Read operation
        if( ctrl_state == 6 ) begin
            if( word_bytes_counter == 0 )
                op_buffer_data_in <= mux_mem_data_out[7:0];
            if( word_bytes_counter == 1 )
                op_buffer_data_in <= mux_mem_data_out[15:8];
            if( word_bytes_counter == 2 ) begin
                op_buffer_data_in <= mux_mem_data_out[23:16];
                // Read new 32 bit word
                all_mems_addr <= all_mems_addr + 1'b1;
            end
            
            // Last byte
            if( word_bytes_counter == 3 ) begin
                op_buffer_we      <= 1;
                op_buffer_addr    <= op_buffer_addr + 1'b1;
                op_buffer_data_in <= mux_mem_data_out[31:24];
                // Reset bytes counter
                word_bytes_counter <= 0;
                // Update counters
                words_counter      <= words_counter + 1'b1;
                // Operation finished
                if( words_counter == 255 ) begin
                    ctrl_state <= 0;
                end 
            end
            else begin
                op_buffer_we       <= 1;
                op_buffer_addr     <= op_buffer_addr + 1'b1;
                word_bytes_counter <= word_bytes_counter + 1'b1;
            end
            
        end // state
    
    end // FSM



endmodule
