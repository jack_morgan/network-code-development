----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:24:43 04/25/2011 
-- Design Name: 
-- Module Name:    shifreg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shiftreg is
	generic (n: INTEGER :=14);
	port (d, clk, rst: in std_logic;
		q : out std_logic);
end shiftreg;

architecture behavior of shiftreg is
	signal internal: std_logic_vector (n-1 downto 0);
begin
	process (clk,rst)
	begin
		if(rst = '1') then
			internal <= (others => '0');
		elsif (clk'event and clk='1') then
			internal <= d & internal(internal'LEFT DOWNTO 1);
		end if;
	end process;
	q <= internal(0);
end behavior;
