--Copyright 2007-2015, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NCM_fetch is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           fetchnext : in  STD_LOGIC;
           dobranch : in  STD_LOGIC;  -- ends before fetchnext
           memdata : in  STD_LOGIC_VECTOR (31 downto 0);
           halt_wakeup : in  STD_LOGIC;
           jumpaddr : in  STD_LOGIC_VECTOR (7 downto 0);
           memaddr : out  STD_LOGIC_VECTOR (7 downto 0); -- to progROM
           cmd : out  STD_LOGIC_VECTOR (31 downto 0);
           cmdok : out  STD_LOGIC;
           branchaddr : in  STD_LOGIC_VECTOR (7 downto 0));
end NCM_fetch;

architecture Behavioral of NCM_fetch is

   signal altdata : std_logic_vector(31 downto 0) := (others => '0');
   signal PC : std_logic_vector(7 downto 0) := X"01";

   type states is (s_idle, s_mem, s_wait);
   signal currentstate : states := s_idle;

begin

   -- branch prefetch cmd
   cmd <= altdata when dobranch = '1' else memdata;

   -- fetch process
   fsm: process(clk, rst)
      variable nextstate : states;
   begin
   
      if (rst = '1') then
         currentstate <= s_idle;
         cmdok <= '0';
         PC <= X"01";
         memaddr <= (others => '0');
         
      elsif (rising_edge(clk)) then
      
         nextstate := currentstate;
         
         case currentstate is
            
            when s_idle =>
               
               if (halt_wakeup = '1') then
                  cmdok <= '0';
                  PC <= jumpaddr + 1;
                  memaddr <= jumpaddr;
						nextstate := s_wait;
                  
               elsif (fetchnext = '1') then
                  cmdok <= '0';
                  memaddr <= branchaddr;
                  nextstate := s_mem;
               
               else
                  cmdok <= '1';  -- active while executing the previous command
						
                  if (dobranch = '1') then
                     PC <= branchaddr + 1;
                  end if;
               end if;
                        
            when s_mem =>
               memaddr <= PC;
               PC <= PC + 1;
               nextstate := s_wait;
            
            when s_wait =>
               altdata <= memdata;
               nextstate := s_idle;
            
            when others =>
               nextstate := s_idle;
         
         end case;
         
         currentstate <= nextstate;
      
      end if;
   
   end process;

end Behavioral;