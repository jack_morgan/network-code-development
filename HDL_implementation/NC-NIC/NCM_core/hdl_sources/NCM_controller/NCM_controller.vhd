
--Copyright 2007-2015, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.ncm_package.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity NCM_controller is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           run : in std_logic;         -- switch on/off controller
           controller_clock : in std_logic;     -- 10 us pulses
           
           softmode : out std_logic;    -- switch on/off soft mode
           
           controlstate : out std_logic_vector(4 downto 0);    -- for debug
           app_sync_pulse : out std_logic;
           
           -- NCM internals
           createcmd_busy : in std_logic;
           sendcmd_busy : in std_logic;
           rcvcmd_busy : in std_logic;
           branch_busy : in std_logic;
           
           -- create cmd
           start_create : out std_logic;
           createcmd_varnum : out std_logic_vector(7 downto 0);
           createcmd_dlen : in std_logic_vector(15 downto 0);
           createcmd_rdy : in std_logic;                       -- capture len
           
           -- send cmd
           sendfifo_empty : in std_logic;
           start_send : out std_logic;
           sendcmd_sync : out std_logic;
           sendcmd_channel : out std_logic_vector(7 downto 0);
           sendcmd_dlen : out std_logic_vector(15 downto 0);
			  TTL		: out std_logic_vector(7 downto 0);
			  sendcmd_BE_mode : out std_logic;

           -- autorcv
           sync_received : in std_logic;                       -- one clock sync pulse
           sync_timeout : out std_logic;
           sync_timeout_pulse : out std_logic;                 -- one clock pulse
			  sync_timeout_cnt : out std_logic_vector(31 downto 0);
			  expected_sync_ok_cnt : out std_logic_vector(31 downto 0);
			  total_sync_cnt : out std_logic_vector(31 downto 0);
			  master_sync_cnt: out std_logic_vector(31 downto 0);
			  slave_sync_cnt: out std_logic_vector(31 downto 0);
           -- rcv cmd
           rcv_start : out std_logic;
           rcv_varnum : out std_logic_vector(7 downto 0);
           rcv_channel : out std_logic_vector(7 downto 0);
           rcv_offset : out std_logic_vector(7 downto 0);
           rcv_fault : in std_logic;
           rcv_rdy : in std_logic;

           -- future
           future_reset : out std_logic;
           future_set : out std_logic;
           future_timetowait : out std_logic_vector(15 downto 0);
           future_address : out std_logic_vector(7 downto 0);
           future_ack : in std_logic;
           future_int : in std_logic;
           future_vector : in std_logic_vector(7 downto 0);

           -- branch
           branch_start : out std_logic;
           branch_guard : out std_logic_vector(19 downto 0);
           branch_count : out std_logic_vector(15 downto 0);
           branch_result : in std_logic;
           branch_rdy : in std_logic;        -- get result

           -- switch mode
           -- new mode is in softmode
           mode_switch : out std_logic;      -- switch pulse
           mode_delay : out std_logic_vector(15 downto 0);

           -- program rom
           ncm_data : in  STD_LOGIC_VECTOR (31 downto 0);
           ncm_addr : out  STD_LOGIC_VECTOR (7 downto 0)
           );
end NCM_controller;

architecture Behavioral of NCM_controller is

   signal command : std_logic_vector(31 downto 0);
   signal cmdok : std_logic;
   signal fetchnext : std_logic := '0';
   signal rst_i : std_logic;
   signal start_NCM : std_logic := '0';
   signal dobranch : std_logic;
   signal softmode_i : std_logic := '1';
   signal wakeup_i : std_logic := '0';
   signal sync_timeout_i : std_logic := '0';
   signal branchaddr : std_logic_vector(7 downto 0) := (others => '0');
   signal jumpaddr : std_logic_vector(7 downto 0) := (others => '0');
   signal app_sync_pulse_i : std_logic := '0';

   signal counter_i : std_logic_vector(15 downto 0);
   signal inc_counter : std_logic := '0';
   signal dec_counter : std_logic := '0';
   signal res_counter : std_logic := '1';

   type states is (s_passive, s_idle, s_decode, s_waitforstart, s_waitres, s_waitforsync, s_rdy);
   signal currentstate : states := s_passive;
   
   signal IP : std_logic_vector(7 downto 0);
   signal sendfifolen : std_logic_vector(15 downto 0);
   
   signal readyforcreate : std_logic := '0';
   signal readyforsend : std_logic := '0';
   signal readyforsync : std_logic := '0';
   signal readyforreceive : std_logic := '0';
   signal readyforbranch : std_logic := '0';

   signal c_instruction : std_logic_vector(3 downto 0);
   signal c_channel : std_logic_vector(7 downto 0);
   signal c_varid : std_logic_vector(7 downto 0);
   signal c_offset : std_logic_vector(7 downto 0);
   signal c_syncmode : std_logic;
   signal c_jump : std_logic_vector(7 downto 0);
   signal c_delay : std_logic_vector(15 downto 0);
   signal c_guard : std_logic_vector(19 downto 0);
   signal c_count_op : std_logic_vector(1 downto 0);
   signal c_TTL : std_logic_vector(7 downto 0);
   signal c_send_mode : std_logic;
   --signal c_TTL_sync : std_logic_vector(7 downto 0);

	signal sync_timeout_cnt_i : std_logic_vector(31 downto 0) := (others => '0'); 
	signal expected_sync_ok_cnt_i : std_logic_vector(31 downto 0) := (others => '0'); 
	signal total_sync_cnt_i : std_logic_vector(31 downto 0) := (others => '0'); 
	signal master_sync_cnt_i : std_logic_vector(31 downto 0) := (others => '0'); 
	signal slave_sync_cnt_i : std_logic_vector(31 downto 0) := (others => '0'); 

begin

   rst_i <= rst or start_NCM;
   sync_timeout_cnt <= sync_timeout_cnt_i;
	expected_sync_ok_cnt <= expected_sync_ok_cnt_i;
	total_sync_cnt <= total_sync_cnt_i;
	master_sync_cnt <= master_sync_cnt_i;
	slave_sync_cnt <= slave_sync_cnt_i;
   softmode <= softmode_i;
   NCM_addr <= IP;
   branch_count <= counter_i;
   
   sync_timeout <= sync_timeout_i;
   app_sync_pulse <= app_sync_pulse_i;
   
   -- create needs varram & sendfifo
   readyforcreate <= not rcvcmd_busy and not branch_busy and not createcmd_busy;
   -- send needs fifo only
   readyforsend <= not sendcmd_busy; -- and not createcmd_busy;
   -- sync is always ready
   readyforsync <= not sendcmd_busy;
   -- receive needs varram
   readyforreceive <= not createcmd_busy and not rcvcmd_busy and not branch_busy;
   -- branch needs varram
   readyforbranch <= not rcvcmd_busy and not branch_busy and not createcmd_busy;
  
   -- split command to parts
   c_instruction <= command(31 downto 28);
   c_channel <= command(23 downto 16);
   c_varid <= command(15 downto 8);
   c_offset <= command(7 downto 0);
   c_syncmode <= command(27);
   c_jump <= command(27 downto 20);
   c_delay <= command(15 downto 0);
   c_guard <= command(19 downto 0);
   c_count_op <= command(25 downto 24);
	c_TTL	 <= command(7 downto 0);
	c_send_mode	 <= command(24);
	--c_TTL_sync	 <= command(7 downto 0);
  
   fetch_unit : NCM_fetch port map (
      clk => clk,
      rst => rst_i,
      fetchnext => fetchnext,
      dobranch => dobranch,
      memdata => ncm_data,
      halt_wakeup => wakeup_i,
      jumpaddr => jumpaddr,
      memaddr => IP,
      cmd => command,
      cmdok => cmdok,
      branchaddr => branchaddr
      );
      
   count_unit : NCM_counter port map (
      clk => clk,
      rst => res_counter,
      inc => inc_counter,
      dec => dec_counter,
      value => counter_i
      );

   fsm: process (clk, rst)
      variable nextstate : states := s_passive;
      variable instruction : std_logic_vector(3 downto 0) := (others => '0');
      variable synccount : std_logic_vector(15 downto 0) := (others => '0');
   begin
   
      if (rst = '1') then
         nextstate := s_passive;
         currentstate <= s_passive;
         -- all start flags to 0
         start_create <= '0';
         start_send <= '0';
         rcv_start <= '0';
         future_set <= '0';
         future_reset <= '1';
         branch_start <= '0';
         mode_switch <= '0';
         wakeup_i <= '0';
         inc_counter <= '0';
         dec_counter <= '0';
         res_counter <= '1';
         dobranch <= '0';
         sync_timeout_pulse <= '0';
         softmode_i <= '1';
         
         createcmd_varnum <= (others => '0');     
         sendcmd_sync <= '0';
         sendcmd_channel <= (others => '0');     
         sendcmd_dlen <= (others => '0');     
         rcv_varnum <= (others => '0');
         rcv_channel <= (others => '0');
         rcv_offset <= (others => '0');
         future_timetowait <= (others => '0');
         future_address <= (others => '0');
         branch_guard <= (others => '0');
         controlstate <= "11111";
      
      elsif (rising_edge(clk)) then
      
         nextstate := currentstate;
      
         -- check if we are alive
         if (run = '0') then
            nextstate := s_passive;
            softmode_i <= '1';
         else
         
            -- get the length of send fifo here
            if (createcmd_rdy = '1') then
               sendfifolen <= createcmd_dlen;
               nextstate := s_rdy;
            end if;

            case currentstate is
               when s_passive =>
               
                  controlstate <= "00001"; --1
                  future_reset <= '1';
                  fetchnext <= '0';
                  sync_timeout_pulse <= '0';
						nextstate := s_passive;
               
                  if (run = '1') then
                     nextstate := s_idle;
                     start_NCM <= '1';
                     softmode_i <= '0';
                  end if;
               
               when s_idle => 
               
                  controlstate <= "00010"; --2

                  -- all start flags to 0
                  start_create <= '0';
                  start_send <= '0';
                  rcv_start <= '0';
                  future_set <= '0';
                  branch_start <= '0';
                  mode_switch <= '0';
                  app_sync_pulse_i <= '0';

                  start_NCM <= '0';
                  future_reset <= '0';
                  wakeup_i <= '0';
                  inc_counter <= '0';
                  dec_counter <= '0';
                  res_counter <= '0';
                  fetchnext <= '0';
                  sync_timeout_pulse <= '0';
						nextstate := s_idle;
                  
                  if (cmdok = '1') then
                     nextstate := s_decode;
                  end if;
               
               when s_decode =>
               
                  controlstate <= "00011"; --3
                  instruction := c_instruction;    -- store for further use
                  dobranch <= '0';
                  fetchnext <= '1';
                  sync_timeout_pulse <= '0';
						sendcmd_sync <= '0';
               
                  case instruction is
                  
                     when CONTROL_NOP =>
                        nextstate := s_rdy;
                  
                     when CONTROL_CREATE =>
                        controlstate <= "00100"; --4
								createcmd_varnum <= c_varid;
                        
                        if (readyforcreate = '1') then
                           start_create <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_SEND =>
								controlstate <= "00101"; --5
                        sendcmd_channel <= c_channel;
                        sendcmd_sync <= '0';
								TTL	<= c_TTL;
								sendcmd_BE_mode <= c_send_mode;
                        
                        if (softmode_i = '1') then          -- dont send in softmode
                           nextstate := s_rdy;
                        elsif (readyforsend = '1') then
                           start_send <= '1';
                           sendcmd_dlen <= sendfifolen;
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_RCV =>
								controlstate <= "00110"; --6
                        rcv_varnum <= c_varid;
                        rcv_channel <= c_channel;
                        rcv_offset <= c_offset;
                        
                        if (softmode_i = '1') then          -- dont receive in softmode
                           nextstate := s_rdy;
                        elsif (readyforreceive = '1') then
                           rcv_start <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_HALT =>
                        -- stay here until interrupt occurs or ncm is deactivated
                        --------------
								controlstate <= "00111";  --7
								fetchnext <= '0';
								mode_switch <= '0';
								start_create <= '0';
								start_send <= '0';
								rcv_start <= '0';
								future_set <= '0';
								future_reset <= '0';
								branch_start <= '0';
								--------------------------
								nextstate := s_waitres;
                     
                     when CONTROL_FUTURE =>

                        controlstate <= "01000";  --8
								future_address <= c_jump;
                        future_timetowait <= c_delay;
                        
                        future_set <= '1';
                        
                        nextstate := s_waitres;
                     
                     when CONTROL_BRANCH =>
								controlstate <= "01001";  --9
                        branch_guard <= c_guard;
                        branchaddr <= c_jump;
                        
                        if (readyforbranch = '1') then
                           branch_start <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_COUNT =>
                        if (c_count_op = "01") then
                           inc_counter <= '0';
                           dec_counter <= '0';
                           res_counter <= '1';
                        elsif (c_count_op = "10") then
                           inc_counter <= '1';
                           dec_counter <= '0';
                           res_counter <= '0';
                        elsif (c_count_op = "11") then
                           inc_counter <= '0';
                           dec_counter <= '1';
                           res_counter <= '0';
                        else
                           inc_counter    <= '0';
                           dec_counter    <= '0';
                           res_counter    <= '0';
                           app_sync_pulse_i <= '1';
                        end if;
                        
                        --nextstate := s_rdy;
								nextstate := s_waitres;
                     
                     when CONTROL_SYNC =>
								controlstate <= "01010";  --A
                        sync_timeout_i <= '0';          -- clear last state
								total_sync_cnt_i <= total_sync_cnt_i + 1;
								TTL	<= c_TTL;

                        if (softmode_i = '1') then          -- dont send in softmode
                           nextstate := s_rdy;
                        elsif (c_syncmode = '1') then
                           sendcmd_BE_mode <= '0';
									master_sync_cnt_i <= master_sync_cnt_i + 1;
                           sendcmd_channel <= (others => '0');
                           sendcmd_dlen <= (others => '0');
                           sendcmd_sync <= '1';
                           
                           if (readyforsync = '1') then
                              start_send <= '1';
                              nextstate := s_waitres;
                           else
                              nextstate := s_waitforstart;
                           end if;
                        else
                           slave_sync_cnt_i <= slave_sync_cnt_i + 1;
									synccount := c_delay;
                           nextstate := s_waitforsync;
                        end if;
                     
                     when others =>
								controlstate <= "01011"; --B
                        nextstate := s_passive;    -- do internal reset
                  
                  end case;
               
               when s_waitforstart => 
               
                  controlstate <= "01100"; --C
                  fetchnext <= '0';

                  case instruction is
                     when CONTROL_CREATE =>
								controlstate <= "01101"; --D

                        if (readyforcreate = '1') then
                           start_create <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_SEND =>
								controlstate <= "01110"; --E
                        if (readyforsend = '1') then
                           start_send <= '1';
                           sendcmd_dlen <= sendfifolen;
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_RCV =>
                     
								controlstate <= "01111"; --F
								if (readyforreceive = '1') then
                           rcv_start <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;

                     when CONTROL_BRANCH =>

								controlstate <= "10000"; --10
                        if (readyforbranch = '1') then
                           branch_start <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;
                     
                     when CONTROL_SYNC =>
                     
								controlstate <= "10001";  --11                      
								if (readyforsync = '1') then
                           start_send <= '1';
                           nextstate := s_waitres;
                        else
                           nextstate := s_waitforstart;
                        end if;

                     when others =>
                         nextstate := s_rdy;
                         
                  end case;
               
               when s_waitres => 

						controlstate <= "10010";   --12
						fetchnext <= '0';
                  mode_switch <= '0';
                  start_create <= '0';
                  start_send <= '0';
                  rcv_start <= '0';
                  future_set <= '0';
                  future_reset <= '0';
                  branch_start <= '0';
                  inc_counter <= '0';
                  dec_counter <= '0';
                  res_counter <= '0';
						
                  case instruction is

                     -- async blocks
                     -- when CONTROL_CREATE =>
                     -- when CONTROL_SEND =>
                     -- when CONTROL_SYNC =>
                     -- when CONTROL_RCV =>
                     
                     when CONTROL_HALT =>
                        -- stay here until interrupt occurs or ncm is deactivated
								controlstate <= "10011";  --13
								nextstate := s_waitres;
								if (future_int = '1') then
									controlstate <= "10100"; --14
                           wakeup_i <= '1';
                           jumpaddr <= future_vector;
                           nextstate := s_rdy;
                        end if;
                     
                     -- future cmd should always work
                     -- when CONTROL_FUTURE =>
                     -- we could check the future_rdy bit with timeout here ...
                     
                     -- when CONTROL_MODE =>
                     
                     when CONTROL_BRANCH =>
								nextstate := s_waitres;
								controlstate <= "10101"; --15
                        if (branch_rdy = '1') then
                           dobranch <= branch_result;
                           nextstate := s_rdy;
                        end if;
                     
                     when others =>
                        nextstate := s_rdy;   
                  
                  end case;
                  
               when s_waitforsync =>

						controlstate <= "10110";  --16
                  fetchnext <= '0';
                  if (sync_received = '1') then
							expected_sync_ok_cnt_i <= expected_sync_ok_cnt_i + 1;
                     sync_timeout_i <= '0';
                     nextstate := s_rdy;
                     
                  elsif (controller_clock = '1') then
                     if (synccount = X"0000") then
                        sync_timeout_i <= '1';
                        sync_timeout_pulse <= '1';
                        sync_timeout_cnt_i <= sync_timeout_cnt_i + 1;
								nextstate := s_rdy;
                     end if;

                     synccount := synccount - 1;
                  else 
							nextstate := s_waitforsync;
                  end if;
               
               when s_rdy =>
               
						controlstate <= "10111";  --17
                  fetchnext <= '0';
                  sync_timeout_pulse <= '0';

--                  inc_counter <= '0';
--                  dec_counter <= '0';
--                  res_counter <= '0';

                  wakeup_i <= '0';
                  nextstate := s_idle;
               
               when others =>
  					  controlstate <= "11000";  --18
                  nextstate := s_passive;
                  
            end case;
         
         end if;
      
         currentstate <= nextstate;
      
      end if;
   
   end process;

end Behavioral;