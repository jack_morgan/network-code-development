
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

---------------------------------------------------------------------------------
-- Company: 		 FHWN
-- Engineer: 		 TR
-- 
-- Create Date:    21:36:01 10/27/2006 
-- Design Name: 	 NCM basic
-- Module Name:    variable_ram - Behavioral 
-- Project Name: 	 NCM
-- Target Devices: 
-- Tool versions: 
-- Description: 	the shared variable space for the Network Code Machine
--						dual port - A for cpu bus R/W
--										B for NCM processor R/W
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity variable_ram is
    Port ( clk_ext_bus : in  STD_LOGIC;
           clk_ncm : in std_logic;
			  -- interface to uP
           bus_address : in  STD_LOGIC_VECTOR (15 downto 0);
           bus_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           bus_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           bus_rw : in  STD_LOGIC;
           -- interface to NCM
           ncm_address : in  STD_LOGIC_VECTOR (15 downto 0);
           ncm_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           ncm_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           ncm_rw : in  STD_LOGIC);
end variable_ram;

architecture Behavioral of variable_ram is

component var_ram is
    Port ( clka : in  STD_LOGIC;
           wea : in  STD_LOGIC_vector (0 downto 0);
           addra : in  STD_LOGIC_VECTOR (10 downto 0);
           dina : in  STD_LOGIC_VECTOR (31 downto 0);
           douta : out  STD_LOGIC_VECTOR (31 downto 0);

			  clkb : in  STD_LOGIC;
           web : in  STD_LOGIC_vector (0 downto 0);
           addrb : in  STD_LOGIC_VECTOR (10 downto 0);
           dinb : in  STD_LOGIC_VECTOR (31 downto 0);
           doutb : out  STD_LOGIC_VECTOR (31 downto 0)
			  );
end component;

signal bus_rw_vec: std_logic_vector (0 downto 0);
signal ncm_rw_vec: std_logic_vector (0 downto 0);

begin 

bus_rw_vec (0) <= bus_rw;
ncm_rw_vec (0) <= ncm_rw;

var_ram0: var_ram port map(
  clka => clk_ext_bus,
  wea => bus_rw_vec, 
  addra => bus_address(10 downto 0), 
  dina => bus_data_in, 
  douta => bus_data_out,
  clkb => clk_ncm,
  web => ncm_rw_vec,
  addrb => ncm_address(10 downto 0),
  dinb => ncm_data_in, 
  doutb => ncm_data_out
);

--	signal wea : std_logic_vector(3 downto 0);
--	signal web : std_logic_vector(3 downto 0);
--	signal addressa : std_logic_vector(14 downto 0);
--	signal addressb : std_logic_vector(14 downto 0);
--	
--	signal ena : std_logic_vector(7 downto 0);		-- max 8 blocks
--	signal enb : std_logic_vector(7 downto 0);		-- max 8 blocks
--
--	signal bus_data_out_0 : std_logic_vector(31 downto 0);
--	signal bus_data_out_1 : std_logic_vector(31 downto 0);
--	signal bus_data_out_2 : std_logic_vector(31 downto 0);
--	signal bus_data_out_3 : std_logic_vector(31 downto 0);
--	signal bus_data_out_4 : std_logic_vector(31 downto 0);
--	signal bus_data_out_5 : std_logic_vector(31 downto 0);
--	signal bus_data_out_6 : std_logic_vector(31 downto 0);
--	signal bus_data_out_7 : std_logic_vector(31 downto 0);
--
--	signal ncm_data_out_0 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_1 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_2 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_3 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_4 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_5 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_6 : std_logic_vector(31 downto 0);
--	signal ncm_data_out_7 : std_logic_vector(31 downto 0);
--
--begin
--
--	-- write signals
--	wea <= bus_rw & bus_rw & bus_rw & bus_rw;
--	web <= ncm_rw & ncm_rw & ncm_rw & ncm_rw;
--	
--	-- address vectors per block
--	addressa <= "0" & bus_address(8 downto 0) & "00000";	-- 512 / block
--	addressb <= "0" & ncm_address(8 downto 0) & "00000";	-- 512 / block
--	
--	-- block decoder
--	ena <= 	"00000001" when bus_address(11 downto 9) = "000" else
--	       	"00000010" when bus_address(11 downto 9) = "001" else
--	       	"00000100" when bus_address(11 downto 9) = "010" else
--	       	"00001000" when bus_address(11 downto 9) = "011" else
--	       	"00010000" when bus_address(11 downto 9) = "100" else
--	       	"00100000" when bus_address(11 downto 9) = "101" else
--	       	"01000000" when bus_address(11 downto 9) = "110" else
--	       	"10000000" when bus_address(11 downto 9) = "111" else
--	       	"00000000";
--
--	enb <= 	"00000001" when ncm_address(11 downto 9) = "000" else
--	       	"00000010" when ncm_address(11 downto 9) = "001" else
--	       	"00000100" when ncm_address(11 downto 9) = "010" else
--	       	"00001000" when ncm_address(11 downto 9) = "011" else
--	       	"00010000" when ncm_address(11 downto 9) = "100" else
--	       	"00100000" when ncm_address(11 downto 9) = "101" else
--	       	"01000000" when ncm_address(11 downto 9) = "110" else
--	       	"10000000" when ncm_address(11 downto 9) = "111" else
--	       	"00000000";
--
--	-- data bus multiplexer
--	bus_data_out <=	bus_data_out_0  when bus_address(11 downto 9) = "000" else
--			bus_data_out_1  when bus_address(11 downto 9) = "001" else
--			bus_data_out_2  when bus_address(11 downto 9) = "010" else
--			bus_data_out_3  when bus_address(11 downto 9) = "011" else
--			bus_data_out_4  when bus_address(11 downto 9) = "100" else
--			bus_data_out_5  when bus_address(11 downto 9) = "101" else
--			bus_data_out_6  when bus_address(11 downto 9) = "110" else
--			bus_data_out_7  when bus_address(11 downto 9) = "111" else
--			(others => '0');
--
--	ncm_data_out <=	ncm_data_out_0  when ncm_address(11 downto 9) = "000" else
--			ncm_data_out_1  when ncm_address(11 downto 9) = "001" else
--			ncm_data_out_2  when ncm_address(11 downto 9) = "010" else
--			ncm_data_out_3  when ncm_address(11 downto 9) = "011" else
--			ncm_data_out_4  when ncm_address(11 downto 9) = "100" else
--			ncm_data_out_5  when ncm_address(11 downto 9) = "101" else
--			ncm_data_out_6  when ncm_address(11 downto 9) = "110" else
--			ncm_data_out_7  when ncm_address(11 downto 9) = "111" else
--			(others => '0');
--
--RAMB16BWER_inst : RAMB16BWER
--generic map (
--	-- DATA_WIDTH_A/DATA_WIDTH_B: 0, 1, 2, 4, 9, 18, or 36
--	DATA_WIDTH_A => 36,
--	DATA_WIDTH_B => 36,
--	-- DOA_REG/DOB_REG: Optional output register (0 or 1)
--	DOA_REG => 0,
--	DOB_REG => 0,
--	-- EN_RSTRAM_A/EN_RSTRAM_B: Enable/disable RST
--	EN_RSTRAM_A => TRUE,
--	EN_RSTRAM_B => TRUE,
--	-- INIT_A/INIT_B: Initial values on output port
--	INIT_A => X"000000000",
--	INIT_B => X"000000000",
--	-- INIT_FILE: Optional file used to specify initial RAM contents
--	INIT_FILE => "NONE",
--	-- RSTTYPE: "SYNC" or "ASYNC"
--	RSTTYPE => "SYNC",
--	-- RST_PRIORITY_A/RST_PRIORITY_B: "CE" or "SR"
--	RST_PRIORITY_A => "CE",
--	RST_PRIORITY_B => "CE",
--	-- SIM_COLLISION_CHECK: Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
--	SIM_COLLISION_CHECK => "ALL",
--	-- SIM_DEVICE: Must be set to "SPARTAN6" for proper simulation behavior
--	SIM_DEVICE => "SPARTAN6",
--	-- SRVAL_A/SRVAL_B: Set/Reset value for RAM output
--	SRVAL_A => X"000000000",
--	SRVAL_B => X"000000000",
--	-- WRITE_MODE_A/WRITE_MODE_B: "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
--	WRITE_MODE_A => "WRITE_FIRST",
--	WRITE_MODE_B => "WRITE_FIRST",
--   INIT_00 => X"06000002060000010500000104000001000000010200000101000001FFFFFFFF",
--   INIT_01 => X"0700000607000005070000040700000307000002070000010800000208000001",
--   INIT_02 => X"0700000E0700000D0700000C0700000B0700000A070000090700000807000007"
--)
--
--port map (
--	-- Port A Data: 32-bit (each) output Port A data
--	DOA => bus_data_out_0, 				-- 32-bit output A port data output
--	DOPA => open, 							-- 4-bit output A port parity output
--	-- Port B Data: 32-bit (each) output Port B data
--	DOB => ncm_data_out_0, 				-- 32-bit output B port data output
--	DOPB => open, 							-- 4-bit output B port parity output
--	-- Port A Address/Control Signals: 14-bit (each) input Port A address and control signals
--	ADDRA => addressa(13 downto 0), 	-- 14-bit input A port address input
--	CLKA => app_clk, 							-- 1-bit input A port clock input
--	ENA => ena(0), 							-- 1-bit input A port enable input
--	REGCEA => '0', 						-- 1-bit input A port register clock enable input
--	RSTA => '0', 							-- 1-bit input A port register set/reset input
--	WEA => wea, 							-- 4-bit input Port A byte-wide write enable input
--	-- Port A Data: 32-bit (each) input Port A data
--	DIA => bus_data_in, 					-- 32-bit input A port data input
--	DIPA => "0000", 						-- 4-bit input A port parity input
--	-- Port B Address/Control Signals: 14-bit (each) input Port B address and control signals
--	ADDRB => addressb(13 downto 0), 	-- 14-bit input B port address input
--	CLKB => ncm_clk, 							-- 1-bit input B port clock input
--	ENB => enb(0), 							-- 1-bit input B port enable input
--	REGCEB => '0',							-- 1-bit input B port register clock enable input
--	RSTB => '0', 							-- 1-bit input B port register set/reset input
--	WEB => web, 						-- 4-bit input Port B byte-wide write enable input
--	-- Port B Data: 32-bit (each) input Port B data
--	DIB => ncm_data_in, 					-- 32-bit input B port data input
--	DIPB => "0000" 						-- 4-bit input B port parity input
--);

end Behavioral;