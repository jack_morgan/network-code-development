`timescale 1ns / 1ps

/*
-Copyright 2007-2010, Embedded Software Group at the University of
Waterloo. All rights reserved.  By using this software the USER
indicates that he or she has read, understood and will comply with the
following:

- Embedded Software Group at the University of Waterloo hereby
grants USER nonexclusive permission to use, copy and/or modify this
software for internal, noncommercial, research purposes only. Any
distribution, including commercial sale or license, of this software,
copies of the software, its associated documentation and/or
modifications of either is strictly prohibited without the prior
consent of the Embedded Software Group at the University of Waterloo.
Title to copyright to this software and its associated documentation
shall at all times remain with the Embedded Software Group at the
University of Waterloo.  Appropriate copyright notice shall be placed
on all software copies, and a complete copy of this notice shall be
included in all copies of the associated documentation.  No right is
granted to use in advertising, publicity or otherwise any trademark,
service mark, or the name of the Embedded Software Group at the
University of Waterloo.


- This software and any associated documentation is provided "as is"

THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
PROPERTY RIGHTS OF A THIRD PARTY.

The Embedded Software Group at the University of Waterloo shall not be
liable under any circumstances for any direct, indirect, special,
incidental, or consequential damages with respect to any claim by USER
or any third party on account of or arising from the use, or inability
to use, this software or its associated documentation, even if The
Embedded Software Group at the University of Waterloo has been advised
of the possibility of those damages.

-------------------------------------------------------------------------------- 
 Create Date: 2014/05/09
 Description: A generic dual port ram:
              - A: R/W access
              - B: Read only
--------------------------------------------------------------------------------
*/


module generic_ram_dual_port(
        p1_clk,
        p1_addr,
        p1_data_out,
        //
        p2_clk,
        p2_addr,
        p2_we,
        p2_data_in,
        p2_data_out
    );

// ---------- PARAMETERS ----------
    parameter DATA_WIDTH = 8;
    parameter ADDR_WIDTH = 8;
    parameter TOTAL_ELEM = 256;
    parameter INIT_FILE = "";

 
// ---------- LOCAL PARAMETERS ----------


// ---------- INPUTS AND OUTPUTS ----------
    input  wire                  p1_clk;
    input  wire [ADDR_WIDTH-1:0] p1_addr;
    output reg  [DATA_WIDTH-1:0] p1_data_out;
    //
    input  wire                  p2_clk;
    input  wire [ADDR_WIDTH-1:0] p2_addr;
    input  wire                  p2_we;
    input  wire [DATA_WIDTH-1:0] p2_data_in;
    output reg  [DATA_WIDTH-1:0] p2_data_out;


// ---------- MODULE ----------    
    
    reg [DATA_WIDTH-1:0] memory_storage [0:TOTAL_ELEM-1];
    
    generate if( INIT_FILE != "" )
        initial $readmemh( INIT_FILE, memory_storage, 0, TOTAL_ELEM-1 );
    endgenerate

    // Port 1
    always @( posedge p1_clk ) begin
        p1_data_out <= memory_storage[p1_addr];
    end
    
    // Port 2
    always @( posedge p2_clk ) begin
        p2_data_out <= memory_storage[p2_addr];
        if( p2_we )
            memory_storage[p2_addr] <= p2_data_in;
    end


endmodule
