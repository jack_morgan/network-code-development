
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity counters is
    Port(
        -- User bus interface
        clk_bus                     : in  STD_LOGIC;
        bus_address                 : in  STD_LOGIC_VECTOR (7 downto 0);
        bus_data_in                 : in  STD_LOGIC_VECTOR (31 downto 0);
        bus_data_out                : out STD_LOGIC_VECTOR (31 downto 0);
        bus_rw                      : in  STD_LOGIC;
        -- NCM interface
        clk_ncm                     : in  STD_LOGIC;
        ncm_rw                      : in  STD_LOGIC;
        -- Counters (using clk_ncm)
        created_frames_cnt          : in  STD_LOGIC_VECTOR (31 downto 0);
        data_frames_sent_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        rcv_cmd_ok_cnt              : in  STD_LOGIC_VECTOR (31 downto 0);
        rcv_cmd_fault_cnt           : in  STD_LOGIC_VECTOR (31 downto 0);
        ack_sync_sent_cnt           : in  STD_LOGIC_VECTOR (31 downto 0);
        bytes_read_from_txfifo_cnt  : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        tx_fifo_overflow_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        total_tx_bytes_cnt          : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_RT_bytes_cnt             : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_bytes_non_RT_cnt         : in  STD_LOGIC_VECTOR (31 downto 0);
        total_rx_bytes_cnt          : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_good_frames_error_cnt    : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_goodframes_non_RT_cnt    : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_non_RT_cnt     : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_goodframes_total_cnt     : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_total_cnt      : in  STD_LOGIC_VECTOR (31 downto 0);
        running_time_tx_cnt         : in  STD_LOGIC_VECTOR (31 downto 0);
        running_time_rx_cnt         : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        master_sync_sent_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        expected_sync_ok_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        sync_timeout_cnt            : in  STD_LOGIC_VECTOR (31 downto 0);
        total_sync_cnt              : in  STD_LOGIC_VECTOR (31 downto 0);
        master_sync_cnt             : in  STD_LOGIC_VECTOR (31 downto 0);
        slave_sync_cnt              : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        autorec_goodcnt_data        : in  STD_LOGIC_VECTOR (31 downto 0);
        autorec_badcnt_data         : in  STD_LOGIC_VECTOR (31 downto 0);
        autorec_goodcnt_sync        : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_total_RT_data_frames_cnt : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_total_RT_sync_frames_cnt : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_total_RT_ack_frames_cnt  : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_total_nonRT_frames_cnt   : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_goodframes_RT_data_cnt   : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_goodframes_RT_sync_cnt   : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_RT_data_cnt    : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_RT_sync_cnt    : in  STD_LOGIC_VECTOR (31 downto 0)
    );
end counters;


architecture Behavioral of counters is
    signal addressa : STD_LOGIC_VECTOR(8 downto 0);
    signal addressb : STD_LOGIC_VECTOR(8 downto 0);
    signal datab    : STD_LOGIC_VECTOR(31 downto 0);
    signal counter  : STD_LOGIC_VECTOR(7 downto 0); 
begin
    
    addressa <= "0" & bus_address;
    addressb <= "0" & counter;

    datab <= created_frames_cnt             when counter = X"00" else
             data_frames_sent_cnt           when counter = X"01" else
             master_sync_sent_cnt           when counter = X"02" else
             ack_sync_sent_cnt              when counter = X"03" else
             rx_goodframes_RT_data_cnt      when counter = X"04" else
             rx_goodframes_RT_sync_cnt      when counter = X"05" else
             rx_badframes_RT_data_cnt       when counter = X"06" else
             rx_badframes_RT_sync_cnt       when counter = X"07" else
             rx_goodframes_non_RT_cnt       when counter = X"08" else
             rx_badframes_non_RT_cnt        when counter = X"09" else
             rcv_cmd_ok_cnt                 when counter = X"0A" else
             rcv_cmd_fault_cnt              when counter = X"0B" else
             expected_sync_ok_cnt           when counter = X"0C" else
             sync_timeout_cnt               when counter = X"0D" else
             total_tx_bytes_cnt             when counter = X"0E" else
             running_time_tx_cnt            when counter = X"0F" else
             total_rx_bytes_cnt             when counter = X"10" else
             running_time_rx_cnt            when counter = X"11" else
             tx_fifo_overflow_cnt           when counter = X"12" else
             rx_good_frames_error_cnt       when counter = X"13" else
             rx_total_RT_data_frames_cnt    when counter = X"14" else
             rx_total_RT_sync_frames_cnt    when counter = X"15" else
             rx_total_RT_ack_frames_cnt     when counter = X"16" else
             rx_total_nonRT_frames_cnt      when counter = X"17" else
             rx_goodframes_total_cnt        when counter = X"18" else
             rx_badframes_total_cnt         when counter = X"19" else
             autorec_goodcnt_data           when counter = X"1A" else
             autorec_badcnt_data            when counter = X"1B" else
             autorec_goodcnt_sync           when counter = X"1C" else
             rx_RT_bytes_cnt                when counter = X"1D" else
             bytes_read_from_txfifo_cnt     when counter = X"1E" else
             total_sync_cnt                 when counter = X"1F" else
             master_sync_cnt                when counter = X"20" else
             slave_sync_cnt                 when counter = X"21" else
             rx_bytes_non_RT_cnt            when counter = X"22" else
             (others => '1');

    FSM2 : process(clk_ncm) begin
        if( rising_edge(clk_ncm) ) then
            counter <= counter + 1;
        end if;
    end process;

    CFG_ROM_QUEUE : RAMB16_S36_S36
    generic map(
        INIT_A              => X"000000000",  -- Value of output RAM registers on Port A at startup
        INIT_B              => X"000000000",  -- Value of output RAM registers on Port B at startup
        SIM_COLLISION_CHECK => "ALL",         -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
        SRVAL_A             => X"000000000",  -- Port A ouput value upon SSR assertion
        SRVAL_B             => X"000000000",  -- Port B ouput value upon SSR assertion
        WRITE_MODE_A        => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
        WRITE_MODE_B        => "WRITE_FIRST"  -- WRITE_FIRST, READ_FIRST or NO_CHANGE
    )
    port map(
        DOA   => bus_data_out,  -- 32-bit A port Data Output
        DOB   => open,          -- 32-bit B port Data Output
        DOPA  => open,          -- 4-bit  A port Parity Output
        DOPB  => open,          -- 4-bit  B port Parity Output
        ADDRA => addressa,      -- 15-bit A port Address Input
        ADDRB => addressb,      -- 15-bit B port Address Input
        CLKA  => clk_bus,       -- Port A Clock
        CLKB  => clk_ncm,       -- Port B Clock
        DIA   => bus_data_in,   -- 32-bit A port Data Input
        DIB   => datab,         -- 32-bit B port Data Input
        DIPA  => "1111",        -- 4-bit  A port parity Input
        DIPB  => "1111",        -- 4-bit  B port parity Input
        ENA   => '1',           -- 1-bit  A port Enable Input
        ENB   => '1',           -- 1-bit  B port Enable Input
        SSRA  => '0', --rstA,   -- 1-bit  A port Synchronous Set/Reset Input
        SSRB  => '0',           -- 1-bit  B port Synchronous Set/Reset Input
        WEA   => bus_rw,        -- 4-bit  A port Write Enable Input
        WEB   => ncm_rw         -- 4-bit  B port Write Enable Input
    );

end Behavioral;
