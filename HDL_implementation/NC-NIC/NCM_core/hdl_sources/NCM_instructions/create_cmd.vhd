
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:08:34 11/12/2006 
-- Design Name: 
-- Module Name:    create_cmd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity create_cmd is
    Port(
        clk                      : in  STD_LOGIC;
        -- cmd interface
        start                    : in  STD_LOGIC;
        rdy                      : out STD_LOGIC;
        --acces_flag_varram : out STD_LOGIC;
        varnum                   : in  STD_LOGIC_VECTOR (7 downto 0);
        datalen                  : out STD_LOGIC_VECTOR (15 downto 0); -- register of the last data length
        varnum_to_send           : out STD_LOGIC_VECTOR (7 downto 0);  -- send id of created variable to send
        -- read config setup
        cfgaddress               : out STD_LOGIC_VECTOR (7 downto 0);
        cfgdata                  : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgdata_queue            : in  STD_LOGIC_VECTOR (31 downto 0); -- config queue info
        cfgdata_queue_write_back : out STD_LOGIC_VECTOR (31 downto 0); -- queue info update
        cfgdata_queue_wen        : out STD_LOGIC;                      -- write enable queue config
        -- copy variable
        varaddress               : out STD_LOGIC_VECTOR (15 downto 0);
        vardata                  : in  STD_LOGIC_VECTOR (31 downto 0);
        fifo_rst                 : out STD_LOGIC;
        fifo_en                  : out STD_LOGIC;
        fifo_data                : out STD_LOGIC_VECTOR (31 downto 0);
        --
        created_frames_cnt       : out STD_LOGIC_VECTOR (31 downto 0)
    );
end create_cmd;


architecture Behavioral of create_cmd is
    type states is (s_init, s_idle, s_eval_queue, s1, s2, s3, s4, s5, s6, s7, s_rdy );
    --
    signal currentstate        : states := s_idle;
    signal varaddress_i        : STD_LOGIC_VECTOR (15 downto 0) := X"FFFF";
    signal varaddress_tmp      : STD_LOGIC_VECTOR (15 downto 0) := X"FFFF";
    signal varlen_i            : STD_LOGIC_VECTOR (15 downto 0) := X"FFFF";
    signal head_i              : STD_LOGIC_VECTOR (7 downto 0)  := X"FF";
    signal num_elements        : STD_LOGIC_VECTOR (7 downto 0);
    signal queue_size_i        : STD_LOGIC_VECTOR (6 downto 0);
    signal created_frames_cnt_i: STD_LOGIC_VECTOR (31 downto 0) := X"00000000";
begin

    fifo_data          <= vardata;              -- variable data shortcut
    varaddress         <= varaddress_i;         -- address out for data copy
    created_frames_cnt <= created_frames_cnt_i; 
    
    -- queue config updated
    cfgdata_queue_write_back <= cfgdata_queue(31 downto 16) & head_i & num_elements;
   
    -- cfgdata_queue is a 32 bit word where:
    -- cfgdata_queue(31) : queue flag (1 if variable support queue)
    -- cfgdata_queue(30 downto 24): queue size (in number of elements)
    -- cfgdata_queue(23 downto 16): element size (size of every element in 32 bits words)
    -- cfgdata_queue(15 downto 8): start pointer (first element in the queue)
    -- cfgdata_queue(7 downto 0): number of valid elements ( >= 0 and < queue size)
    
    FSM: process( clk )
        variable nextstate : states;
    begin
        if( rising_edge(clk) ) then
            nextstate := currentstate;
            
            case currentstate is
                when s_idle =>
                    rdy               <= '0';
                    fifo_en           <= '0';
                    fifo_rst          <= '0';
                    --datalen           <= (others => '0');
                    cfgdata_queue_wen <= '0';  
                    if( start = '1' ) then
                        cfgaddress     <= varnum; -- request config data
                        fifo_rst       <='1';     -- clear fifo
                        varnum_to_send <= varnum;
                        nextstate      := s1;
                    else
                        cfgaddress <= X"00";
                    end if;
                when s1 =>
                    fifo_rst  <= '0';
                    nextstate := s_eval_queue;     -- wait for config data
                when s_eval_queue =>
                    varaddress_tmp <= cfgdata_queue(15 downto 8)*cfgdata_queue(23 downto 16);
                    if( cfgdata_queue(31) = '0' ) then      -- varlen 0
                        nextstate := s2;
                    elsif( cfgdata_queue(31) = '1' ) then
                        nextstate := s5;
                    end if;
                ---No queue procedure, it has no changes since NCM2 -----
                when s2 =>
                    --fifo_rst <= '0';
                    -- config entries: address (31 - 16) in 32 bit words
                    --                 len (15 - 0) in 32 bit words
                   varaddress_i <= cfgdata(31 downto 16);
                   varlen_i     <= cfgdata(15 downto 0);
                   datalen      <= cfgdata(15 downto 0);
                   if( cfgdata(15 downto 0) = X"0000" ) then   -- varlen 0
                      nextstate := s_rdy;
                   else
                      nextstate := s3;
                   end if;
                when s3 =>
                    fifo_rst     <= '0';
                    fifo_en      <= '1';
                    varaddress_i <= varaddress_i + 1;
                    varlen_i     <= varlen_i - 1;
                    nextstate    := s4;
                when s4 =>
                    if (varlen_i = X"0000") then
                        nextstate := s_rdy;
                        fifo_en   <= '0';
                    else
                        fifo_en      <= '1';
                        varaddress_i <= varaddress_i + 1;
                        varlen_i     <= varlen_i - 1;
                        nextstate    := s4;
                    end if;
--------Queue procedure-----------------------
                when s5 =>
                    -- config entries: address (31 - 16) in 32 bit words
                    -- len (15 - 0) in 32 bit words
                    -- current element on queue = initial position of variable + start pointer * element size 
                    varaddress_i <= cfgdata(31 downto 16) + varaddress_tmp;
                    -- real queue_size to index form
                    queue_size_i <= cfgdata_queue(30 downto 24) - 1;
                    -- varlen is the size of the elements
                    varlen_i     <= X"00" & cfgdata_queue(23 downto 16);
                    -- save current start pointer and number of elements
                    head_i       <= cfgdata_queue(15 downto 8);
                    num_elements <= cfgdata_queue(7 downto 0);
                    if (cfgdata_queue(23 downto 16) = X"00" or cfgdata_queue(7 downto 0) = X"00") then  -- if element_size = 0 or queue is empty
                        datalen   <=  X"0000";                                                          -- nothing to do
                        nextstate := s_rdy;
                    else
                        datalen   <=  X"00" & cfgdata_queue(23 downto 16);
                        nextstate := s6;
                    end if;
                when s6 =>
                    fifo_rst     <= '0';
                    fifo_en      <= '1';
                    varaddress_i <= varaddress_i + 1;
                    varlen_i     <= varlen_i - 1;
                    -- queue start pointer and number of elements are updated
                    num_elements <= num_elements - 1;
                    if( head_i = queue_size_i ) then -- queue is a circular buffer
                        head_i <= X"00";
                    else
                        head_i <= head_i + 1;
                    end if;
                    cfgdata_queue_wen <= '1';
                    nextstate         := s7;
                -- send the current element
                when s7 =>
                    cfgdata_queue_wen <= '0';
                    if( varlen_i = X"0000" ) then
                        nextstate := s_rdy;
                        fifo_en   <= '0';
                    else
                        fifo_en      <= '1';
                        varaddress_i <= varaddress_i + 1;
                        varlen_i     <= varlen_i - 1;
                        nextstate    := s7;
                    end if;
                    --- end of queue procedure ----
                when s_rdy =>
                    --acces_flag_varram <= '0';
                    rdy                  <= '1';
                    fifo_en              <= '0';
                    nextstate            := s_idle;
                    created_frames_cnt_i <= created_frames_cnt_i + 1;
                when others =>
                    nextstate := s_idle;
            end case;
            
            currentstate <= nextstate;
        end if;
    end process;

end Behavioral;