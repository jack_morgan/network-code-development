
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:14:57 01/22/2007 
-- Design Name: 
-- Module Name:    countdown - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity countdown is
    Port(
        clk       : in  STD_LOGIC;
        reset     : in  STD_LOGIC;
        tick      : in  STD_LOGIC;
        set       : in  STD_LOGIC;
        t_in      : in  STD_LOGIC_VECTOR (15 downto 0);
        active_ct : out STD_LOGIC;
        timeover  : out STD_LOGIC;
        --allzero : out STD_LOGIC;  -- not active_ct
        remaining : out STD_LOGIC_VECTOR (15 downto 0)
    );
end countdown;


architecture Behavioral of countdown is
    signal allzero_i   : STD_LOGIC;
    signal remaining_i : STD_LOGIC_VECTOR (15 downto 0);
begin

    -- allzero <= allzero_i;
    remaining <= remaining_i;
    timeover  <= '1' when remaining_i = X"0001" else '0';
    allzero_i <= '1' when remaining_i = X"0000" else '0';

    doit: process( clk, reset )
        variable cnt : STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    begin
        if( reset = '1' ) then
            active_ct   <= '0';
            remaining_i <= (others => '0');
            cnt         := (others => '0');
        elsif( rising_edge(clk) ) then
            if( set = '1' ) then
                active_ct <= '1';
                cnt       := t_in;
            elsif( tick = '1' ) then
                if( allzero_i = '0' ) then
                    cnt := cnt - 1;     -- one timetick is over
                else
                    active_ct <= '0';
                end if;
            elsif( allzero_i = '1' ) then
                active_ct <= '0';
            end if;
            remaining_i <= cnt;
        end if;
    end process;

end Behavioral;
