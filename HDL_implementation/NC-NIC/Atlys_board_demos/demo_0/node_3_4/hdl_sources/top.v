//`default_nettype none
`timescale 1ns / 1ps

module top (
        // General
        input  wire        clk100_i,
        input  wire        rst_n,
        input  wire        rst_button_n,
        // HDMI-OUT
        output wire [3:0]  TMDS,
        output wire [3:0]  TMDSB,
        // PMOD Connector
        //output wire [7:0] JB,
        // USB UART Connector
        input  wire PIN_UartRx,
        output wire PIN_UartTx,
        // DDR2
        output wire        DDR2CLK_P,
        output wire        DDR2CLK_N,
        output wire        DDR2CKE,
        output wire        DDR2RASN,
        output wire        DDR2CASN,
        output wire        DDR2WEN,
        inout  wire        DDR2RZQ,
        inout  wire        DDR2ZIO,
        output wire [2:0]  DDR2BA,
        output wire [12:0] DDR2A,
        inout  wire [15:0] DDR2DQ,
        inout  wire        DDR2UDQS_P,
        inout  wire        DDR2UDQS_N,
        inout  wire        DDR2LDQS_P,
        inout  wire        DDR2LDQS_N,
        output wire        DDR2LDM,
        output wire        DDR2UDM,
        output wire        DDR2ODT,
        // Ethernet
        input  wire        gmii_rx_clk,
        input  wire        gmii_rx_dv,
        input  wire        gmii_rx_er,
        input  wire [7:0]  gmii_rxd, 
        output             gmii_tx_clk,
        output             gmii_tx_en,
        output             gmii_tx_er,
        output      [7:0]  gmii_txd,
        output             phy_rst_n,
        // LEDS
        output wire [7:0]  led,
        input  wire [2:0]  switch
        );


// ---------- PARAMETERS ----------
    // Value overwritten by synthesis tools for different configs
    parameter SYNT_RT = 1;


// ---------- LOCAL PARAMETERS ----------
    // -- Rom configurations
    // Version 0: RT Rx Only
    localparam PROG_MEM_RT_FILE = "../hdl_sources/NCM_mem_node_3__prog.coe";
    localparam CONF_MEM_RT_FILE = "../hdl_sources/NCM_mem_node_3__cfg.coe";
    // Version 1: BE Rx Only
    localparam PROG_MEM_BE_FILE = "../hdl_sources/NCM_mem_node_4__prog.coe";
    localparam CONF_MEM_BE_FILE = "../hdl_sources/NCM_mem_node_4__cfg.coe";
    // -- Other params
    localparam [15:0] NC_TYPE_SYNC    = 16'h5CFF;
    localparam [15:0] NC_TYPE_ACK     = 16'h5CAA;
    localparam [15:0] NC_TYPE_DATA_RT = 16'h5CE0;
    localparam [15:0] NC_TYPE_DATA_BE = 16'h0805;
    // -- Final configuration to use
    localparam        PROG_MEM_INIT_FILE = (SYNT_RT)? (PROG_MEM_RT_FILE) : (PROG_MEM_BE_FILE);
    localparam        CONF_MEM_INIT_FILE = (SYNT_RT)? (CONF_MEM_RT_FILE) : (CONF_MEM_BE_FILE);
    localparam [15:0] NC_TYPE_DATA       = (SYNT_RT)? (NC_TYPE_DATA_RT)  : (NC_TYPE_DATA_BE);
    localparam        NC_MY_DEVICE_TYPE  = 1'b1; // 0=Master; 1=Slave
    


// ---------- MODULE ----------
    wire        reset, reset_sync, rst_button;
    wire [10:0] hpos, vpos;
    wire [23:0] disp_pixel_data;
    wire [7:0]  userbits;

    wire [7:0] txd_from_mac, rxd_to_mac;     
    wire tx_en_from_mac, tx_er_from_mac, rx_dv_to_mac, rx_er_to_mac, rx_clk;

    assign rst_button = ~rst_button_n;

    assign led = switch;

    assign userbits = {6'b111100, switch[1:0]};

    // -- Clocks
    wire pll_in_clk, pll_feed_back;
    wire pll_clk_25MHz, pll_clk_50MHz, pll_clk_250MHz, pll_clk_125MHz, pll_clk_100MHz;
    wire clk_25MHz, clk_50MHz, clk_250MHz, clk_125MHz, clk_100MHz, pll_locked, serdesstrobe;
    
    IBUFG pllinibufg_1( .I(clk100_i), .O(pll_in_clk)  );
    
    // F_VCO must be between 400 MHz and 1000 MHz.
    // F_VCO = F_CLKIN * CLKFBOUT_MULT
    PLL_BASE #(
        .CLKIN_PERIOD   (10),  //(real)[1.408-52.630] input period in ns.
        .CLKFBOUT_MULT  (10),  //(int) [1-64] Feedback multiplier.
        // Outputs clocks, 0 to 5.
        .CLKOUT0_DIVIDE (4),   // (int)[1-128] 10 x Pixel clock
        .CLKOUT1_DIVIDE (20),  // (int)[1-128]  2 x Pixel clock
        .CLKOUT2_DIVIDE (40),  // (int)[1-128]      Pixel clock
        .CLKOUT3_DIVIDE (8),   // (int)[1-128] Ethernet and NCM clock
        .CLKOUT4_DIVIDE (10),  // (int)[1-128] Memory controller (MCB) clock
        .COMPENSATION("INTERNAL")
    )
    PLL_clocks_gen (
        .CLKIN    (pll_in_clk),
        .CLKFBIN  (pll_feed_back),
        .CLKFBOUT (pll_feed_back),
        .RST      (1'b0),
        // Outputs
        .CLKOUT0  (pll_clk_250MHz), // 10 x Pixel clock
        .CLKOUT1  (pll_clk_50MHz),  //  2 x Pixel clock
        .CLKOUT2  (pll_clk_25MHz),  //      Pixel clock
        .CLKOUT3  (pll_clk_125MHz), // Ethernet and NCM clock
        .CLKOUT4  (pll_clk_100MHz), // Memory controller (MCB) clock
        // Status
        .LOCKED   (pll_locked)
    );
  
  
    BUFG clk_bufg_1 ( .I(pll_clk_25MHz),  .O(clk_25MHz)  );
    BUFG clk_bufg_2 ( .I(pll_clk_50MHz),  .O(clk_50MHz)  );
    BUFG clk_bufg_3 ( .I(pll_clk_125MHz), .O(clk_125MHz) );
    BUFG clk_bufg_4 ( .I(pll_clk_100MHz), .O(clk_100MHz) );
    
    BUFPLL #(
        .DIVIDE(5)
    )
    ioclk_buf (
        .PLLIN        (pll_clk_250MHz),
        .GCLK         (clk_50MHz),
        .IOCLK        (clk_250MHz),
        .LOCKED       (pll_locked),
        .SERDESSTROBE (serdesstrobe),
        .LOCK()
    );
    // -------------------

    debounce
    debounce_1 (
        .reset  (1'b0),      
        .clk    (clk_25MHz),
        .noisy  (rst_button),
        .clean  (reset)
    );

    wire preload_vid_line;
    wire vid_HSync, app_timer;
    
    one_shot
    one_shot_1 (
        .sigOut (app_timer_tick),
        .sigIn  (vid_HSync),
        .clk    (clk_25MHz)
    );
    
    
    wire        ncm_varram_wen;
    wire [15:0] ncm_varram_addr;
    wire [31:0] ncm_varram_din;
    wire [31:0] ncm_varram_dout;
    
         
    user_app # (
        .DISP_H_RES_PIX (640),
        .DISP_V_RES_PIX (480),
        .H_POS_BITS     (11),
        .V_POS_BITS     (11)
    )
    user_app_1 (
        .DDR2CLK_P        (DDR2CLK_P),
        .DDR2CLK_N        (DDR2CLK_N),
        .DDR2CKE          (DDR2CKE),
        .DDR2RASN         (DDR2RASN),
        .DDR2CASN         (DDR2CASN),
        .DDR2WEN          (DDR2WEN),
        .DDR2RZQ          (DDR2RZQ),
        .DDR2ZIO          (DDR2ZIO),
        .DDR2BA           (DDR2BA),
        .DDR2A            (DDR2A),
        .DDR2DQ           (DDR2DQ),
        .DDR2UDQS_P       (DDR2UDQS_P),
        .DDR2UDQS_N       (DDR2UDQS_N),
        .DDR2LDQS_P       (DDR2LDQS_P),
        .DDR2LDQS_N       (DDR2LDQS_N),
        .DDR2LDM          (DDR2LDM),
        .DDR2UDM          (DDR2UDM),
        .DDR2ODT          (DDR2ODT),
        // Clocks an timer ticks
        .app_clk          (clk_25MHz),
        .mem_clk          (clk_100MHz),
        .app_timer_tick   (app_timer_tick),
        // NCM interface
        .ncm_varram_addr  (ncm_varram_addr),
        .ncm_varram_wen   (ncm_varram_wen),
        .ncm_varram_din   (ncm_varram_din),
        .ncm_varram_dout  (ncm_varram_dout),
        //
        .vid_clk          (clk_25MHz),
        .vid_preload_line (preload_vid_line),
		.vid_hpos         (hpos),
		.vid_vpos         (vpos),
        .vid_data_out     (disp_pixel_data)
    );
                    
    resetsync
    resetsync_1 (
        .iClk     (clk_25MHz),
        .iRst     (reset),
        .oRstSync (reset_sync)
    );

    phy_mac_interface
    phy_mac_interface_1 (
        // Synchronous resets
        .tx_reset       (1'b0),
        .rx_reset       (1'b0),
        // The following ports are the GMII physical interface: these will be at pins on the FPGA
        .gmii_txd       (gmii_txd),
        .gmii_tx_en     (gmii_tx_en),
        .gmii_tx_er     (gmii_tx_er),
        .gmii_tx_clk    (gmii_tx_clk),
        .gmii_rxd       (gmii_rxd),
        .gmii_rx_dv     (gmii_rx_dv),
        .gmii_rx_er     (gmii_rx_er),
        .gmii_rx_clk    (gmii_rx_clk),
        // The following ports are the internal GMII connections from IOB logic to the TEMAC core
        .txd_from_mac   (txd_from_mac),
        .tx_en_from_mac (tx_en_from_mac),
        .tx_er_from_mac (tx_er_from_mac),
        .tx_clk         (clk_125MHz),
        .rxd_to_mac     (rxd_to_mac),
        .rx_dv_to_mac   (rx_dv_to_mac),
        .rx_er_to_mac   (rx_er_to_mac),
        // Receiver clock for the MAC and Client Logic
        .rx_clk         (rx_clk)
    );

    // -- Memory manager (uart interface)
    wire uart_unit_busy;
    //
    wire [7:0]  all_mems_addr;
    wire [31:0] all_mems_data_in;
    //
    wire        mem0_we;
    wire [31:0] mem0_data_out;
    wire        mem1_we;
    wire [31:0] mem1_data_out;
    wire        mem2_we;
    wire [31:0] mem2_data_out;
    
    uart_mem_manager  #(
        .IN_CLK_FR      (25000000),
        .UART_BAUD_RATE (230400),
        .MY_STATION_ID  (1)
    )
    uart_mem_manager_1 (
        .clk                   (clk_25MHz),
        // Control and status
        .reset                 (~pll_locked),
        .busy                  (uart_unit_busy),
        // Memories interface
        .all_mems_addr         (all_mems_addr),
        .all_mems_data_in      (all_mems_data_in),
        .mem0_we               (mem0_we),
        .mem0_data_out         (mem0_data_out),
        .mem1_we               (mem1_we),
        .mem1_data_out         (mem1_data_out),
        .mem2_we               (mem2_we),
        .mem2_data_out         (mem2_data_out),
        // Serial signals
        .PIN_UartRx            (PIN_UartRx),
        .PIN_UartTx            (PIN_UartTx)
    );
    

    // -- NCP
    ncm_main #(
        .PROG_MEM_INIT_FILE ( PROG_MEM_INIT_FILE ),
        .CONF_MEM_INIT_FILE ( CONF_MEM_INIT_FILE )
    )
    ncm_main_1 (
        .sys_clk         (clk_125MHz), //(clk_25MHz),
        .app_clk         (clk_25MHz),
        .clk_ext_bus     (clk_25MHz), // Mem manager clk
        //
        .start           (1'b1),
        .stop            (1'b0),
        .rst             (1'b0), // switch[7]
        //
        .varram_app_wen  (ncm_varram_wen),
        .varram_app_addr (ncm_varram_addr), // 15:0
        .varram_app_din  (ncm_varram_din),  // 31:0
        .varram_app_dout (ncm_varram_dout), // 31:0
        //
        .progrom_bus_address  (all_mems_addr),
        .progrom_bus_din      (all_mems_data_in),
        .progrom_bus_dout     (mem0_data_out),
        .progrom_bus_wen      (mem0_we),
        .cfgrom_bus_address   (all_mems_addr),
        .cfgrom_bus_din       (all_mems_data_in),
        .cfgrom_bus_dout      (mem2_data_out),
        .cfgrom_bus_wen       (mem2_we),
        .counters_bus_address (all_mems_addr),
        .counters_bus_din     (all_mems_data_in),
        .counters_bus_dout    (mem1_data_out),
        .counters_bus_wen     (mem1_we),
        //        
        .NCData_type     (NC_TYPE_DATA),
        .NCSync_type     (NC_TYPE_SYNC),
        .NCAck_type      (NC_TYPE_ACK),
        .sync_ack_en     (NC_MY_DEVICE_TYPE),
        //
        .rx_clk          (rx_clk),
        .rx_dv_to_mac    (rx_dv_to_mac),
        .rx_er_to_mac    (rx_er_to_mac),
        .rxd_to_mac      (rxd_to_mac),
        .tx_clk          (clk_125MHz),
        .tx_en_from_mac  (tx_en_from_mac),
        .tx_er_from_mac  (tx_er_from_mac),
        .txd_from_mac    (txd_from_mac),
        .phy_rst_n       (phy_rst_n),
        //
        .userbits        (userbits)
    );
    
    
    // -- Video output
    dvi_tx_fixed_res #(
        // Resolution (active pixels)
        .H_RES_PIX  (640),
        .V_RES_PIX  (480),
        // Horizontal timing
        .H_FN_PRCH  (32),
        .H_SYNC_PW  (88),
        .H_BK_PRCH  (32),
        // Vertical timing
        .V_FN_PRCH  (10),
        .V_SYNC_PW  (5),
        .V_BK_PRCH  (10),
        // Signal polarity
        .H_SYNC_POL (0),
        .V_SYNC_POL (0),
        // Data read latency
        .LATENCY    (1)
    )
    HDMI_out_1 (
        // Clocks
        .p_clk_x1         (clk_25MHz),
        .p_clk_x2         (clk_50MHz),
        .p_clk_x10        (clk_250MHz),
        .serdesstrobe     (serdesstrobe),
        // Reset
        .reset            (~pll_locked || reset_sync),
        // Pixel coordinates
        .HPos             (hpos),
        .VPos             (vpos),
        // Video data
        .red_data         (disp_pixel_data[23:16]),
        .green_data       (disp_pixel_data[15:8]),
        .blue_data        (disp_pixel_data[7:0]),
        //
        .active           (),
        .preload_vid_line (preload_vid_line),
        .h_sync           (vid_HSync),
        // HDMI physical interface
        .TMDS_OUT         (TMDS),
        .TMDS_OUTB        (TMDSB)
    );
    

endmodule
