# README #

This demo incorporates all previous characteristics for a complete demonstration of the platform capabilities.
Additionally, this demo has a best effort part that uses a modified version of nodes 1 and 4.

### Node's role ###

- NetFPGAs: Real-Time capable switches and 1 as a Best-Effort traffic injector.
- Node 1 [master]: Captures a video signal from HDMI and sends it as RT traffic.
- Node 2 [slave]: Receives RT traffic, applies a sobel filter to data, and sends it as RT traffic.
- Node 3 [slave]: Receives RT traffic, applies an invert filter to data, and sends it as RT traffic.
- Node 4 [slave]: Receives and displays RT traffic from nodes 2 and 3.
- Node 5 [slave]: Captures a video signal from HDMI and sends it as BE traffic.
- Node 6 [slave]: Receives and displays BE traffic from node 5.


### Time slots ###

The time slots are summarized in the following table:

Slot |  node_1 |  node_2 |  node_3 |  node_4
----:|:-------:|:-------:|:-------:|:-------:
  0  | Tx (RT) | Rx (RT) | Rx (RT) | -------
  1  | ------- | Tx (RT) | Tx (RT) | Rx (RT)
  2  | ------- | ------- | ------- | -------

NOTE: Nodes 5 and 6 don't appear in the table because they communicate using Best-Effort traffic, which means that they are not ruled by a schedule, although we can place them inside the time slot 1.


### Time slots details ###

Each slot has different steps (sub-slots):

    + S: Tx unit sends data
    + R: Rx unit receives data


Time, in processor's future() counts:

    + Total time:
        - Master: 3896
        - Slaves: 3875
    + Sync: Time used to send/receive the synchronization frame:
        - Master: 500
        - Slaves: 250
    + Slots end: Time at the and of all slots to synchronize the period
        - Master: 46
        - Slaves: 25
    + Remaining time:
        - Master: 3350
        - Slaves: 3600


Slots time:

    + Slot 0: Total 1200
        - S: 1000
        - R: 200
    + Slot 1: Total 1200
        - S: 1100
        - R: 100
    + Slot 2: Total 1200 (950 for Master)
        - S: 1100 (850 for Master)
        - R: 100


### Node's boards ###

Each node is an Atlys board acting as a communication master or slave.

The boards' slide-switches are numbered from right to left between 0 to 7.
Bellow is a summary of their functions in the different nodes:

+ Node 1: None
+ Node 2:
  - switch[0]: Enable (up) or disable (down) the dynamic-TDMA sending of black video lines.
+ Node 3: None
  - switch[0]: Enable (up) or disable (down) the dynamic-TDMA sending of black video lines.
+ Node 4: None
+ Node 5: None
+ Node 6: None

Additionally, each board has 8 LEDs numbered exactly the same as the switches.
Bellow is a summary of their functions in the different nodes:

+ Node 1: None
+ Node 2:
    - led[1]: Synchronization with master acquired
+ Node 3:
    - led[1]: Synchronization with master acquired
+ Node 4:
    - led[1]: Synchronization with master acquired
+ Node 5:
    - led[1]: Synchronization with master acquired
+ Node 6:
    - led[1]: Synchronization with master acquired
