`timescale 1ns / 1ps

module filter__sobel (
    clk,
    // [in] Control and status signals
    line_in_ready,
    line_in_num,
    line_in_active,
    // [out] Control and status signals
    busy,
    line_out_num,
    line_out_active,
    // Input line-buffer interface
    line_in_addr,
    line_in_data,
    // Output line-buffer interface 1
    line_out_clk_1,
    line_out_addr_1,
    line_out_data_1,
    // Output line-buffer interface 2
    line_out_clk_2,
    line_out_addr_2,
    line_out_data_2
);


// ---------- INCLUDES ----------

// ---------- PARAMETERS ----------
    parameter H_RES_PIX   = 640;
    parameter V_RES_PIX   = 480;
    parameter LINE_N_BITS = 9;  // Bits for the line number
    parameter LINE_B_BITS = 10;  // Bits for the line-buffer address (input and output)

    parameter THRESHOLD   = 15000;

// ---------- LOCAL PARAMETERS ----------

// ---------- INPUTS AND OUTPUTS ----------
    input  wire                   clk;
    // [in] Control and status signals
    input  wire                   line_in_ready;
    input  wire [LINE_N_BITS-1:0] line_in_num;
    input  wire                   line_in_active;
    // [out] Control and status signals
    output reg                    busy            = 0;
    output reg  [LINE_N_BITS-1:0] line_out_num    = 0;
    output reg                    line_out_active = 0;
    // Input line-buffer interface
    output reg  [LINE_B_BITS-1:0] line_in_addr   = 0;
    input  wire [7:0]             line_in_data;
    // Output line-buffer interface 1
    input  wire                   line_out_clk_1;
    input  wire [LINE_B_BITS-1:0] line_out_addr_1;
    output reg  [7:0]             line_out_data_1 = 0;
    // Output line-buffer interface 2
    input  wire                   line_out_clk_2;
    input  wire [LINE_B_BITS-1:0] line_out_addr_2;
    output reg  [7:0]             line_out_data_2 = 0;


// ---------- MODULE ----------

    // By Wolf.
    reg signed [8:0] LineBufferIn_0[0:H_RES_PIX-1];
    reg signed [8:0] LineBufferIn_1[0:H_RES_PIX-1];
    reg signed [8:0] BUFFER_0_READ_REG;
    reg signed [8:0] BUFFER_1_READ_REG;
    wire signed [8:0] BUFFER_2_READ_REG = {1'b0,line_in_data[7:0]};
    reg signed [10:0] Gy_n, Gy_p, Gx_n, Gx_p;
    reg signed [11:0] Gx, Gy;
    reg    signed [23:0] BORDER_DATA;
    reg                      proc_buff_we     = 0;
    reg [LINE_B_BITS-1:0] proc_buffer_addr = 0;
    reg    [3:0]    WE_SHIFT = 0;
    reg INPUT_VALID_PIP_0 = 0;
    // Output line-buffer
    reg [7:0] line_buffer_out_1 [0:H_RES_PIX-1];
    reg [7:0] line_buffer_out_2 [0:H_RES_PIX-1];
    // Write interface
    reg                   line_buffer_we   = 0;
    reg [LINE_B_BITS-1:0] line_buffer_addr = 0;
    wire [7:0]             line_buffer_din ;
    assign line_buffer_din  =  (BORDER_DATA > THRESHOLD )? 255:0;
    // Write interface (internal)
    always @( posedge clk ) begin
        if( line_buffer_we ) begin
            line_buffer_out_1[line_buffer_addr] <= line_buffer_din;
            line_buffer_out_2[line_buffer_addr] <= line_buffer_din;
        end
        if (proc_buff_we) begin
            LineBufferIn_0[proc_buffer_addr]    <= BUFFER_1_READ_REG;
            LineBufferIn_1[proc_buffer_addr]    <= BUFFER_2_READ_REG;
        end
        BUFFER_0_READ_REG <= LineBufferIn_0[line_in_addr];
        BUFFER_1_READ_REG <= LineBufferIn_1[line_in_addr];
    end
    // Output line-buffer interface 1
    always @( posedge line_out_clk_1 ) begin
        line_out_data_1 <= line_buffer_out_1[line_out_addr_1];
    end
    
    // Output line-buffer interface 2
    always @( posedge line_out_clk_2 ) begin
        line_out_data_2 <= line_buffer_out_2[line_out_addr_2];
    end

    reg [1:0]             FSM_state   = 0;
    reg [LINE_N_BITS-1:0] in_line     = 0;
    reg                   in_line_act = 0;
    reg                   line_out_active_pip_0 = 0;

    reg signed [8:0] PIXEL_WINDOW[2:0][2:0];

    reg  [LINE_B_BITS-1:0] line_in_addr_pip_0;
    reg  INPUT_VALID = 0;
    
    always @( posedge clk ) begin

        // State 0: Wait for a new line
        if( FSM_state == 0 ) begin
            proc_buffer_addr <= 0;

            if( line_in_ready ) begin
                in_line      <= line_in_num;    // Copy from input line
                in_line_act  <= line_in_active; // Copy from input line
                line_in_addr <= line_in_addr + 1'b1;
                busy         <= 1;
                FSM_state    <= 1;
                INPUT_VALID  <= 1;
                proc_buff_we     <= 1;


            end
            else begin
                proc_buff_we     <= 0;
                busy         <= 0;
                line_in_addr <= 0;

            end
        end

        // State 1: Copy inverted data
        if( FSM_state == 1 ) begin
            if( proc_buff_we && proc_buff_we<639)
                proc_buffer_addr <= proc_buffer_addr + 1'b1;
            else
                proc_buffer_addr <= 0;
                
            if (proc_buffer_addr == 639)
                proc_buff_we     <= 0;

            line_in_addr     <= line_in_addr + 1'b1;
            if (proc_buffer_addr == (H_RES_PIX-1) ) begin
                INPUT_VALID  <= 0;
            end
            if( proc_buffer_addr == (H_RES_PIX-1) ) begin
                line_out_active_pip_0 <= in_line_act;
                line_out_active       <= line_out_active_pip_0;
                FSM_state             <= 0;
                if( in_line > 0 )
                    line_out_num <= in_line - 1'b1;
                else
                    line_out_num <= 0;
            end
                
        end

        WE_SHIFT[3:0] <= {WE_SHIFT[2:0],proc_buff_we};
        //1 ciclo de retraso. proc_buff_we
        //2 ciclos de retraso. WE_SHIFT[0]
        //3 ciclos de retraso. WE_SHIFT[1]
        //4 ciclos de retraso. WE_SHIFT[2]
        //5 ciclos de retraso. WE_SHIFT[3]
        
        line_buffer_we    <=    WE_SHIFT[2];
        if( line_buffer_we && line_buffer_addr<639)
            line_buffer_addr <= line_buffer_addr + 1'b1;
        else
            line_buffer_addr <= 0;
        
        line_in_addr_pip_0 <= line_in_addr;
        
        if (line_in_addr_pip_0 == 0 && line_in_addr==1) begin
            PIXEL_WINDOW[0][0] <= BUFFER_0_READ_REG;
            PIXEL_WINDOW[0][1] <= BUFFER_0_READ_REG;

            PIXEL_WINDOW[1][0] <= BUFFER_1_READ_REG;
            PIXEL_WINDOW[1][1] <= BUFFER_1_READ_REG;

            PIXEL_WINDOW[2][0] <= BUFFER_2_READ_REG;
            PIXEL_WINDOW[2][1] <= BUFFER_2_READ_REG;
        end
        else begin
            PIXEL_WINDOW[0][0] <= PIXEL_WINDOW[0][1];
            PIXEL_WINDOW[0][1] <= PIXEL_WINDOW[0][2];
            
            PIXEL_WINDOW[1][0] <= PIXEL_WINDOW[1][1];
            PIXEL_WINDOW[1][1] <= PIXEL_WINDOW[1][2];
            
            PIXEL_WINDOW[2][0] <= PIXEL_WINDOW[2][1];
            PIXEL_WINDOW[2][1] <= PIXEL_WINDOW[2][2];
        end
        INPUT_VALID_PIP_0   <= INPUT_VALID;
        if( INPUT_VALID == 1 ) begin
            PIXEL_WINDOW[0][2] <= BUFFER_0_READ_REG;
            PIXEL_WINDOW[1][2] <= BUFFER_1_READ_REG;
            PIXEL_WINDOW[2][2] <= BUFFER_2_READ_REG; //Entrada, asociada a proc_buffer_addr
        end

        Gy_n <= -PIXEL_WINDOW[0][0] -(PIXEL_WINDOW[0][1]<<<1) - PIXEL_WINDOW[0][2];
        Gy_p <=  PIXEL_WINDOW[2][0] +(PIXEL_WINDOW[2][1]<<<1) + PIXEL_WINDOW[2][2];

        Gx_n <= -PIXEL_WINDOW[0][0] -(PIXEL_WINDOW[1][0]<<<1) - PIXEL_WINDOW[2][0];
        Gx_p <=  PIXEL_WINDOW[0][2] +(PIXEL_WINDOW[1][2]<<<1) + PIXEL_WINDOW[2][2];

        Gx   <=  Gx_n + Gx_p;
        Gy   <=  Gy_n + Gy_p;

        BORDER_DATA <= Gx*Gx + Gy*Gy;


    end


endmodule
