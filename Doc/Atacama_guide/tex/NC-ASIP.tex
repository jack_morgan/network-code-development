\section{The Network Code ASIP} \label{sec:NCP-arch}

The Network Code \ac{ASIP} is the unit in charge of executing the pre-designed and verified schedules at run time. Figure~\ref{fig:NCP-architecture} shows the general architecture of the \ac{NC-ASIP}. The version described here is based on an early concept introduced in~\cite{Fischmeister2009a}. The internal architecture of the \ac{ASIP} consists of three sections: (1) the memory space, (2) the \ac{NCC}, and (3) the Ethernet core. The memory space and Ethernet core provide the interfaces to the tasks and physical medium, respectively. The \ac{NCC} contains the logic to move data between the medium and data buffers in a time-triggered manner.

The path between applications and the medium for best-effort messages might follow arbitrary patterns and
can rely on well-known mechanisms and higher communication layers. The NC-ASIP does not consider management of best-effort messages beyond the MAC level, and simply provides the necessary connections to handle them externally.

%%%%%%%%%%%%%%%%
\begin{figure}[] %\centering %
 \centering
 \includegraphics[scale=0.5]{figures/Single-Core-NCP-new}
  \caption{Architecture of the Network Code Processor}
  \label{fig:NCP-architecture}
\end{figure}
%%%%%%%%%%%%%%%%


\subsection{Memory Space}
%%%%%%%%%%%%%%%%
\begin{figure}[]
 \centering
 \includegraphics[scale=0.6]{figures/memory-space}
  \caption{Detailed Description of the Memory Space}
  \label{fig:NCP-memory-space}
\end{figure}
%%%%%%%%%%%%%%%%

The memory space provides the memory resources that implement the data buffers that interface the computation tasks, and hold the system configuration. It contains three blocks:

\begin{itemize}
\item The \code{PROG-ROM} block holds the predesigned schedule.
\item The \code{MSG-CFG} block contains a memory layout that defines the data buffers to store real-time data. Each memory location specifies a data buffer for the local system through an initial address and length, which are mapped to locations on the MSG-DATA. 
\item The \code{MSG-DATA} block is a dual-port memory that provides the interface for data exchange between the computation tasks and the \ac{NCC}. This block stores the actual values of the application data.
\end{itemize}

Both the \code{PROG-ROM} and \code{MSG-CFG} blocks work as read-only memories at run-time. The \ac{ASIP} does not provide any mechanism to handle conflicts at run time. The user is thus responsible for verifying the validity of the stored schedule, and also checking the memory map to prevent inconsistencies (data overlaps, overflows, underflows, etc.) when accessing the data stored in the \code{MSG-DATA} block. Both application and communication layers can access the \code{MSG-DATA} independently, but they must still share a common mapping from buffer IDs to memory locations.
%It is also necessary to verify that both application and communication layers agree in the mapping to the 

The prototype implemented in this thesis uses 32-bits wide buses. Each Network Code instruction is encoded in one 32-bit word, and the atomic unit for application data length is 32-bits words.

%It can also include additional blocks for debugging purposes. Must also Include interfaces for external accesses for configuration and debugging.

\subsection{Network Code Core}

The \ac{NCC} implements the main functionality of the time-triggered communication system and is where coordination takes place. 

The \ac{NCC} features a superscalar architecture that represents each instruction of the Network Code language as a well-defined finite state machine, each one mapped to independent logical resources. The controller block is the brain of the system in charge of triggering execution of the states machines according to the programmed schedule. At run time, the controller sequentially reads and processes the instructions stored in the PROG-ROM, using a traditional fetch-decode-execute pipeline~\cite{Hennessy2006}.  When decoding a valid instruction, the controller sends a start signal to the corresponding state machine, which then executes a predefined number of steps before returning to the idle state.

The controller also manages instruction level parallelism by checking dependencies between consecutive instructions, triggering concurrent execution of multiple state machines whenever it is possible. The instruction loader uses a preload pipeline and provides the next instruction right after the decoding of the previous one. In case of possible parallel execution, the execution stage of one instruction triggers decoding of the next one. 
 
We carefully designed and optimized the \ac{ASIP} by hand to exploit the benefits of hardware parallelism while still keeping the execution time of the schedules predictable. The following paragraphs provides details about the internals of the current version of the \ac{NCC}, including the implementation of the Network Code instructions and conditions for concurrent execution.

\subsubsection{Data Flow Logic}

Figure~\ref{fig:NCP-paths} shows a diagram of the functional blocks and main signals involved on the transmission and reception operations in the \ac{NCC}, ordered from left to right according to the time they are triggered in a typical program. 

Figure~\ref{fig:NCP-tx-path} shows the path to transmit the application data stored in the data buffer $A$ using a \code{create-send} sequence. The operation starts with the \code{create()} block reading address $A$ of the \code{MSG-CFG} block to get the initial address and length of the application data stored in the MSG-DATA block. After getting the location, the block accesses the data and copies it to the internal \code{send-FIFO}. The \code{send()} block then reads this FIFO, encapsulates the application data into the payload of an Ethernet frame, and stores it in the \code{RT-Tx FIFO}. Figure~\ref{fig:RT-frame-format} shows the format of the generated frames. Messages containing real-time data use the special identifier NC-DATA in the EthType field. Apart from the application data, the payload includes information such as the \code{TTL}, the data length (in 32-bit words), the logical channel, the ID of the local buffer (position on the MSG-CFG block), and a sequential frame counter. Receivers use this information for processing received frames and debugging purposes. The minimum size of an Ethernet frame is 64 bytes, and thus the \code{send} block adds padding to the payload in case the data is not large enough. The \ac{NCC} sends this data to Ethernet Core, which adds the preamble and the Frame Check Sequence (FCS) fields to complete the frame before transmitting it.

To favor simplicity on the design and force simpler scheduling tasks, we restrict the intermediate \code{send-FIFO} to hold only one variable. A created message will stay in the FIFO until being either read from a \code{send} instruction or replaced by a new \code{create} instruction. Therefore, in a regular program a \code{create} instruction should always be followed by a \code{send} instruction, although it is possible to implement further instructions in the middle.

Figure~\ref{fig:NCP-rx-path} depicts the receiver path. The Ethernet core signals the \ac{NCC} when receiving a valid real-time frame. Valid frames containing real-time data (\code{goodframe} signal asserted on frames tagged as NC-DATA) automatically triggers the asynchronous \code{autoreceive} block, which parses the frame to extract the application data and store it in the corresponding \code{receive-FIFO} (there is one FIFO per channel). The \code{receive()} block can then read the data from the specified channel and store it on the MSG-DATA location of the local variable $A$. The local variable on the receiver side can be located in any location, but it must at least match the size of the transmitted one. The controller is unaware of the arrival of new data, and triggers the \code{receive()} block according to the specified schedule. The designer must check the distributed schedules in advance to ensure that there will be new data available at the time of executing a \code{receive()} operation. If the schedule does not execute a \code{receive()} instruction, meaning that is not interested in the data from a previous time slot, the data will stay in the FIFO until being replaced by a new data frame arriving from the same channel. If there is no data available at the time of executing a \code{receive()} operation, the block will return to the idle state, signal the error to the controller, which will continue with the execution of the program. The block will also signal an error when the received data does not match the size of the assigned buffer.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
  \centering
  \subfloat[Transmission path]{\label{fig:NCP-tx-path}
  \includegraphics[scale=0.72]{figures/NC_transmit_detailed2}}
  \vspace{10mm}
  \subfloat[Reception path]{\label{fig:NCP-rx-path}
  \includegraphics[scale=0.72]{figures/NC_receive_detailed2}}
  \caption{Block Diagram for Transmission and Reception path in the Network Code Core}
  \label{fig:NCP-paths}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
\begin{figure}[t] \centering 
 \includegraphics[scale=0.8]{figures/frame_format}
  \caption{Real-time frame format}
  \label{fig:RT-frame-format}
\end{figure}
%%%%%%%%%%%%%%%% 
The \ac{FIFO} queues interfacing the Ethernet core use a dual-port configuration with independent clocks. This allows the ASIP to operate each section with a different clock domain. The $\textrm{NC}_\textrm{clk}$ driving the scheduling logic is provided by the user, and the TX and RX clocks come from the Ethernet core and depend on the physical implementation.

%The NCC uses 32-bits data buses, while the Ethernet MAC is restricted to operate with 8-bits buses. This means that the NCC can still operate at line speed running at a fraction (as low as a quarter) of the frequency in the physical link.

\subsubsection{Timing Control Logic}

The \code{sync} instruction enables the synchronization to coordinate the actions across the distributed stations. As explained in Section~\ref{sec:timing-instructions}, the \code{sync} instruction operates in two modes: master and slave. When the controller decodes a \code{sync} instruction in master mode, it triggers execution of the send block with an special flag to indicate the generation of a synchronization beacon. These beacons are minimum sized frame tagged as \code{NC-SYNC} in the EthType field without containing application data. In this case, the \code{send} block fills the payload with dummy data without accessing the \code{send-FIFO}. Slave stations execute the \code{sync} instruction in slave mode. In this case, the controller stalls execution of the schedule waiting for either the expiration of the countdown timer or the arrival of a synchronization beacon. The arrival of a valid beacon (\code{goodframe} signal asserted on frames tagged as \code{NC-SYNC}) will trigger the  \code{autoreceiver} block, which will signal the controller to advertise the event, without processing the data or accessing the \code{receive-FIFO}. If the controller was stalled due to a \code{sync} instruction in slave mode, it will resume the execution of the following instruction. In case the receiver is not waiting for the message, the beacon will have no effect.

The countdown timers for the \code{future} and \code{sync} instructions are based on pulses derived from the \NCclk (NC$_{\textrm{pulse}}$). The core provides a configurable clock divider to adjust the resolution of the pulses as entire multiples of the \NCclk.


\subsubsection{Execution Flow Logic}

The \code{branch} instruction is one of the key components of the Network Code framework, and provides the ability to encode guarded transitions that jump between different preloaded configurations based on conditions that change at run time. If the evaluated guard returns \code{TRUE}, then the controller sets the program counter to the address specified in the label (see Section~\ref{sec:conditional-inst}). Otherwise, the controller increments the program counter normally to execute the following instructions.

The current implementation includes some default guards to check system status and perform operations over data buffers. When decoding a branch instruction, the controller will identify the guard and evaluate the corresponding condition. The hard-coded guards included by default on the current version of the system are (the name represents the corresponding keyword as described in the language specification):

\begin{itemize}
\item \code{AlwaysTrue}: returns the value \code{TRUE}.
\item \code{AlwaysFalse}: returns the value \code{FALSE}.
\item \code{ChannelXReceiveFIFOEmpty}: returns \code{TRUE} if and only if the empty flag of the receive FIFO associated to channel $X$ is set.
\item \code{SendFIFOEmpty}: returns \code{TRUE} if the empty flag of the send FIFO is set.
\item \code{CounterTest}: compare an internal counter with an input value. The guard returns \code{TRUE} of both values match, and \code{FALSE} otherwise.
\item \code{StatusTest}: checks the value of different flags that indicate the system status, returning \code{TRUE} if the evaluated flag is set. Some of the flags include indicators for SyncTimeouts, UserBits, etc.
\item \code{EqualVarVar}: compares the data stored in two buffers. The guard first compares the length of the two buffers. If the lengths match, then the guard performs a word-by-word comparison of the data stored in the buffers. If the data is the same, then the guard returns \code{TRUE}. If the length test or there is a difference in a word, then the guard returns \code{FALSE} .
\item \code{GreaterVarVar}: compares the data stored in two buffers, returning \code{TRUE} if the value stored in the first buffer is greater than the value stored in the second one. 
\item \code{LessVarVar}: compares the data stored in two buffers, returning \code{TRUE} if the value stored in the first buffer is lesser than the value stored in the second one. 
\item \code{TestVar}: returns \code{TRUE} if the given variable fulfills certain criteria (not zero, all zero, odd parity, even parity).
\end{itemize}

The list of evaluated guards can be easily upgraded to include other conditions. However, since guards are hard-coded, the designer must consider the impact on the resource utilization and timing over the complete system. For example, a guard that includes multiplication of values will require either \ac{DSP} blocks for dedicated multipliers or use general logic resources. In general, multiplications are expensive operations and might affect the maximum operation frequency of the system.

The hard-coded guards included in the previous list are designed to be evaluated in a predefined number of steps, and thus it is possible to predict the delay from jumping from one configuration to another. For more details about the implementation and encoding of the \code{branch} instruction, see~\cite{NC-spec}.


\subsection{Ethernet Core}

Figure~\ref{fig:Ethernet-core} shows a detailed diagram of the logic inside the Ethernet core, which implements the interface between the \ac{NCC} and the physical medium. It contains a standard Ethernet \ac{MAC}, the interface to the \ac{PHY}, and exchanges data with the \ac{NCC} through the dual-port \code{RT-Rx} and \code{RT-Tx} FIFOs. The core also includes additional logic for on-the-fly frame processing tasks that separates incoming best-effort from real-time frames, and arbitrates the access to the output port. 

During transmission (Figure~\ref{fig:Ethernet-core-TX}), real-time messages receive strict priority access to the resources in the path to the physical medium. The TX-arbiter implements a state machine, which in its idle state assign the output resources to the external best-effort path. Write operations from the NCC side will deassert the empty flag of the \code{RT-Tx FIFO}, triggering the execution of the state machine that transmit the real-time message. When triggered, the TX arbiter immediately decouples the connection between the best-effort path and the \ac{MAC}, interrupting a potentially on-going transaction and appearing busy to the external path. To prevent the propagation of non-standard frames, the \ac{MAC} will stay busy and add a pad to the payload in case the transaction is interrupted before completing a minimum sized frame. To prevent conflicts in the shared output port, the arbiter must ensure that the path is free and ready for transmission before sending the data from the \code{RT-Tx FIFO}. To do this, the arbiter waits a fixed time that accounts for the transmission time of a minimum sized frame plus the \ac{IFG}, representing the worst case scenario of interrupting a best-effort transmission that had just started. Once emptying the \code{RT-Tx FIFO}, the arbiter waits for the \ac{IFG}, and then reestablish the connection between the \ac{MAC} and the best-effort path. Under this approach, the best-effort path will be able to transmit data using any gap between consecutive real-time messages.

On the receiver side (Figure~\ref{fig:Ethernet-core-RX}), the input classifier performs on-the-fly parsing of the incoming bytes, using a shift-register to delay the signals from the \ac{MAC} until it verifies the EthType value of incoming frames. The classifier sends real-time frames (tagged as either \code{NC-DATA} or \code{NC-SYNC}) to the \code{RT-Rx FIFO}, and all other frames to the best-effort path. This operation adds a fixed delay to the reception process.

It is important to note that the Ethernet core operates on bytes, while the \ac{NCC} operates on 32-bit words. Therefore, the dual-port FIFOs that interface the sections have asymmetric ports and can operate in different clock domains. Because of the wider data buses, the \NCclk can run at a fraction (as low as a quarter) of the clocks in the Ethernet core without being a bottleneck for line speed operation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
  \centering
  \subfloat[Transmission path]{
	\label{fig:Ethernet-core-TX}
  \includegraphics[scale=0.72]{figures/MAC_transmit_detailed}}
  \vspace{10mm}
  \subfloat[Reception path]{
	\label{fig:Ethernet-core-RX}
  \includegraphics[scale=0.72]{figures/MAC_receive_detailed}}
  \caption{Block Diagram for Transmission and Reception Path in the Ethernet Core}
  \label{fig:Ethernet-core}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Instruction Level Parallelism} \label{sec:instruction-parallelism}

The superscalar architecture of the \ac{NCC} allows us to maximize the parallel execution of multiple instructions to increase the throughput. However, traditional hardware acceleration techniques aim to maximize the average performance without considering the effects on the timing predictability. Indeed, traditional techniques consider using buffers and handshaking mechanisms between blocks that share a common resource. The requirement of any kind of arbitration based on run time conditions are not suitable for hard real-time systems.

We designed and optimized the processor by hand, aiming to exploit the advantages of hardware parallelism while still keeping the execution time of the schedules predictable. To do so, we started by carefully analyzing dependencies among instructions. With the full set of dependencies, we encoded the set as a set of conditions inside the controller block. Once an instruction is correctly validated and decoded, the controller will trigger concurrent execution only if the encoded conditions allows it, and wait for the end of the previous instruction if the conditions indicate potential conflicts (or hazards). Without this full analysis, we risk unintended behavior and possible faults during execution which can result in system failures or undesired jitter.

The architecture of the \ac{NCC} is prone to structural and control-flow hazards.

Control-flow hazards indicate that, for two successive instructions, the execution of the second one depends on a result or condition generated from the first one. Instructions \code{branch()}, \code{halt()}, and \code{sync(slave)} are sources of control hazards in the program, since they must wait for specific conditions to decide when and/or where to continue execution. Therefore, instructions following any of the previous ones can not run in parallel.

Structural hazards occur when two consecutive instructions require access to the same resource. From the diagram in Figure~\ref{fig:NCP-architecture} we identify two different sources of structural hazards:
\begin{itemize}
\item Instructions \code{create()}, \code{receive()}, and the guards for \code{branch()} associated to data processing require access to the \code{MSG-DATA} block. Because there is a single memory bus, they cannot run in parallel. In the case of the \code{branch()} instruction, the guards access the memory bus only when operating over variables in the data buffers
\item Instructions \code{send()} and \code{sync()} in master mode use the same block to generate Ethernet frames, and then they cannot run concurrently %Since the active is a send instruction sending a special telegram, it can be handled by the send block, too.
\end{itemize}

The \code{Send-FIFO} also represents a potential structural hazard between the \code{create()} and \code{send()} instructions. However, we overcome this problem using a dual-port FIFO, which allows the \code{send()} block to start reading the FIFO while the \code{create()} block is filling it from the other side. The \code{send()} block must generate the frame headers before reading the application data, generating an intrinsic delay between the writing and reading operations to the queue. We applied the same principle to the \code{receive-FIFO} between the \code{autorec} and \code{receive} blocks.

Table~\ref{tab:dependence-table} summarizes the dependencies for the NC-ASIP. The meaning of the characters are $w$ for wait until finished, $c$ for continue with the next instruction, and $b$ for wait until the memory bus is available. To read the table, consider two sequential instructions \code{x()-y()}. The instruction \code{x()} specifies the column and \code{y()} specifies the row. For example, the sequence \code{branch()-send()} results in a sequential execution as specified by $w$, while the instructions \code{send()-branch()} can be executed in parallel. 

In the implementation, all instruction blocks indicate their running state (idle/busy) to the controller. The controller uses these states to calculate the locking conditions during the decoding stage. After triggering the execution of one instruction, the controller immediately decodes the next one. If Table~\ref{tab:dependence-table} permits concurrent execution, the controller will trigger the second block. Otherwise, it will stay on a waiting state until the lock is resolved, stalling the fetch of subsequent instructions.

\begin{table}[t]
  \centering
  %\normalsize
  \caption{Summary of instruction dependencies}
  \vspace{2em}
  \begin{tabular}{c|ccccccccc}
%    &  \tikz \node [rotate=90] {nop};
%    &  \tikz \node [rotate=90] {create};
    &  \begin{rotate}{90} create \end{rotate}
%    &  \tikz \node [rotate=90] {send};
    &  \begin{rotate}{90} send \end{rotate}
%    &  \tikz \node [rotate=90] {receive};
    &  \begin{rotate}{90} receive \end{rotate}
%    &  \tikz \node [rotate=90] {sync};
    &  \begin{rotate}{90} sync(m) \end{rotate}
%    &  \tikz \node [rotate=90] {halt};
    &  \begin{rotate}{90} sync(s) \end{rotate}
    &  \begin{rotate}{90} halt \end{rotate}
%    &  \tikz \node [rotate=90] {future};
    &  \begin{rotate}{90} future \end{rotate}
%    &  \tikz \node [rotate=90] {if}; \\
    &  \begin{rotate}{90} branch \end{rotate} \\
    \hline
     create  & w & c & b & c & w & w & c & w \\
     send    & c & w & c & w & w & w & c & w \\
     receive & b & c & w & c & w & w & c & w \\
     sync(m) & c & w & c & w & w & w & c & w \\
     sync(s) & c & c & c & c & w & w & c & w \\
     halt    & c & c & c & c & w & w & c & w \\
     future  & c & c & c & c & w & w & w & w \\
     branch  & b & c & b & c & w & w & c & w \\
  \end{tabular}
\label{tab:dependence-table}
\end{table}