\section{Switch with Dedicated Paths for Time-Triggered Frames} \label{sec:enhanced-switch}

This section introduces custom extensions for Ethernet switches that complement the functionality of the \ac{NIC}s based on the \ac{NC-ASIP}. The extensions provide a dedicated path for coordinated real-time frames that runs in parallel with the traditional forwarding mechanism available in \ac{COTS} switches. Figure~\ref{fig:custom-switch} shows a general diagram of the enhanced switch. The modules in gray implement a cut-through broadcast path for real-time frames generated from NC-ASIPs, which resembles the classical bus topology used in most legacy fieldbuses [1]. This path naturally spans across multiple switches, and provides additional functionality for automatic discovery and integration of real-time capable devices, and logical segmentation of real-time domains in large networks. 

%%%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \includegraphics[scale=0.9]{figures/tte-based-switch}
  \caption{Enhanced Switch for the NIC/Switch Configuration}
  \label{fig:custom-switch}
\end{figure}
%%%%%%%%%%%%%%%%

\subsection{Classifying and Forwarding Real-time Frames} \label{sec:rt-switch-forwarding}

Figure~\ref{fig:switch-forwarding-path} shows a detailed diagram of the architecture of the dedicated path for real-time frames between the input and output ports. Each input port includes a classifier, which separates incoming real-time from best-effort frames. This classifier works similarly to the one implemented in the interfaces based on the \ac{NC-ASIP}, but includes additional logic to handle the \code{TTL} value encoded on the frame payload. The switch stores real-time frames (tagged as NC-DATA or NC-SYNC type) with \code{TTL}$\geq 1$ in the dedicated \code{RT-Rx FIFO}, and automatically discards frames with a \code{TTL}$= 0$. The real-time path uses a \emph{cut-through} mechanism that starts forwarding the frame as soon as the \code{RT-Rx FIFO} deasserts its empty flag. When this happens, the \code{send} block starts moving the frame to the dedicated \code{RT-Tx FIFO} in all the other ports, decrementing the \code{TTL} value encoded in the header by one. 

%%%%%%%%%%%%%%%%
\begin{sidewaysfigure}
%\begin{figure*}[t] 
	\centering 
 	\includegraphics[scale=0.8]{figures/switch_receive_detailed}
  \caption{Real-time Path Inside the Switch}
  \label{fig:switch-forwarding-path}
%\end{figure*}
\end{sidewaysfigure}
%%%%%%%%%%%%%%%%

Since time-triggered frames are always broadcast, and considering that a correctly designed schedule will prevent any competition in the medium, the real-time path can avoid any address processing and queuing mechanisms to handle contention. As stated early, these mechanisms are the largest source of latency and jitter in switched networks. In consequence, the real-time path represents a direct connection between the input port and all the output ports in the switch, resembling a classical bus topology typically used in safety-critical fieldbuses.

Best-effort frames follow the traditional path through the \ac{COTS} switching fabric. The \code{Tx-arbiter} in the output ports of the switch works similarly to the one described for the interfaces based on the \ac{NC-ASIP}, providing strict priority access to the real-time path. Figure~\ref{fig:custom-switch-arbitration} illustrates the arbitration process. At the time BE1 arrives to the \code{BE-TX FIFO}, the port is busy transmitting the real-time frame RT1, and thus BE1 must wait until the port becomes available. At the time BE2 arrives to the \code{BE-TX FIFO}, the port is available and thus the arbiter forwards the frame immediately. While the port is still transmitting BE2, a real-time frame RT2 arrives to the output FIFO, and then the switch interrupts the transmission of BE2 to forward the real-time frame. Again, the internal logic of the real-time path was carefully designed to achieve predictable execution since the port reaches the \ac{MAC} at the input port until the frame reaches the \code{RT-TX FIFO} in the output ports. This allows us to know in advance the exact number of cycles necessary to move a byte from one end to another in the switch.

%%%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \includegraphics[scale=0.88]{figures/tte-switch-arbitration}
  \caption{Arbitration in the Output Ports Inside the Enhanced Switch }
  \label{fig:custom-switch-arbitration}
\end{figure}
%%%%%%%%%%%%%%%%

The switch forwards best-effort frames whenever there is no real-time data to transmit, and thus traditional stations using \ac{COTS} components communicate transparently without requiring any modification, but their available bandwidth will be reduced according to the amount of real-time traffic flowing through the network.

The logical real-time path naturally expands across multiple cascaded switches, creating a logical bus that coexist with the traditional switched path. Since the forwarding latency is a fixed value, it is then possible to model the end-to-end latency with high accuracy in a complex switched configuration, independent of the load and behavior of the best-effort traffic.


\subsection{Cyclic Topology Discovery}

The real-time path provides predictable and low-latency forwarding path for stations using NC-ASIPs, which can generate and interpret specific type of coordinated frames. Timing guarantees for real-time data come at the expense of reduced available bandwidth for best-effort frames due to restricted access to the output ports. However, stations using \ac{COTS} NICs are unable to interpret the specific format of real-time frames, and it is then necessary to prevent the propagation of real-time frames to traditional stations operating exclusively with best-effort traffic. 

To reduce the effect of interrupted access to the output ports on stations using \ac{COTS} interfaces, the real-time path implements additional logic for discovery and integration of real-time capable stations in the network. Figure~\ref{fig:RT-programmable-connections} illustrates this mechanism based on the propagation synchronization beacons. Switches always broadcast synchronization beacons to all the output ports different that the one receiving it. Real-time capable devices (either stations using \ac{NC-ASIP}s or switches with dedicated path) automatically respond to these beacons with a special acknowledge frame tagged with EthType \code{NC-ACK} (Figure~\ref{fig:broadcast-sync-ack}). The switch will only assert the programmable connection between the real-time path and the ports that received either a beacon or an acknowledge frame from an external source. Input ports in the switch process acknowledge frames locally, without storing or propagating them to the other ports. The ports use dedicated logic to process the acknowledge messages in parallel to the propagation of the beacons, thus they do not add latency to the regular synchronization processing. At the end of the synchronization slot, the switch will forward real-time data frames only to the ports that are attached to real-time capable devices (Figure~\ref{fig:RT-path-after-sync}). Consequently stations using \ac{COTS} NICs will only suffer from blocking and interruptions during the synchronization slots, and they will remain unaffected during the exchange of real-time data. 

The switch automatically resets and reprograms the connections to the real-time path with the arrival of a new synchronization beacon. By providing an adequate upper limit for the propagation latency along the entire network, there is no need to know the exact topology of the real-time stations in advance, and their locations can even change at run time.

\begin{figure}
  \centering
  \subfloat[Propagation of Sync and Acknowledge Frames]{\label{fig:broadcast-sync-ack}
  \includegraphics[scale=0.7]{figures/sync-prop}}
  \\
  \subfloat[Real-time Path After Synchronization Slot]{\label{fig:RT-path-after-sync}
  \includegraphics[scale=0.7]{figures/after-sync-prop}}
  \caption{Discovery of Real-time Capable Devices}
  \label{fig:RT-programmable-connections}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Segmentation of Real-Time Domains}

Traditional configurations for safety-critical systems tend to use dedicated networks for different applications, generating control islands that operate in physical isolation, preventing the natural exchange of information between them. 

Switched Ethernet provides mechanisms to implement logical segmentation of large networks in separate domains. Multi-segmented networks support higher throughput for the same bandwidth in relation to traditional bus configurations, because each segment can communicate locally at the same time. This property remains mostly unexplored in available real-time Ethernet solutions. Existing approaches that support this property require careful planning of the topology, and the use of cumbersome tools and configuration steps for every single device in the network. In our approach, we propose a simple but effective way to implement logical segmentation using a \ac{TTL} mechanism for propagating the frames. Switches automatically handle the \ac{TTL} of incoming frames, without requiring any additional configuration step.

%The custom forwarding path for real-time frames allow us to define logical real-time segments contained within a larger switched Ethernet network.
We define a real-time segment as a set of devices that synchronize to a single master to exchange real-time data. We also define a data subsegment as a subset of devices within a real-time segment that exchange data between them, but without propagating the frames to devices outside the subsegment. Figure~\ref{fig:RT-segments} shows an example for these two kinds of logical segmentation of a set of distributed real-time stations within a single Ethernet infrastructure.

Figure~\ref{fig:independent-segments} illustrates a segmentation in two independent real-time segments, each one broadcasting real-time data within its own domain. The designer can specify real-time domains by simply tapping a link with a filter/gateway to block real-time frames and prevent their propagation from one segment to another. It
is also possible to accomplish the same behavior using a COTS managed switch for blocking specific frames. This configuration enables easy segmentation of real-time messages,
without affecting the propagation of best-effort frames.

Figure~\ref{fig:data-subsegments} shows a division of a real-time segment in multiple data subsegments. In this case, all switches are real-time capable, and the logical separation is based on the \code{TTL} value embedded in real-time frames. In the example, stations S$_1$ and S$_2$ transmit certain frames which are only useful for stations in their corresponding subsegment. We can implement logical isolation of data frames between subsegments by using a \code{TTL}=2 and \code{TTL}=3 for frames generated from S$_1$ and S$_2$, respectively. Synchronization beacons from the master must have a \code{TTL}$\geq 4$ to reach all the stations in the segment. This approach enables concurrent transmission of data in different subsegments during the same time slot, increasing the effective network utilization. However, it requires careful planning of the schedules considering the specific topology of real-time stations, and its feasibility is subject to the possibility of physically grouping stations belonging to a same logical subsegment. 

As consequence, the presented framework enables the coexistence of multiple applications operating in isolation for time-critical data, but still allowing the exchange of best-effort messages between them. All this is possible using an homogeneous physical infrastructure without requiring expensive and complex gateways.

\begin{figure*}
  \centering
  \subfloat[Independent Real-time Segments]{\label{fig:independent-segments}
  \includegraphics[scale=0.7]{figures/rt_segments}}
  \hspace{3mm}
  \subfloat[Synchronized Data Subsegments]{\label{fig:data-subsegments}
  \includegraphics[scale=0.7]{figures/rt_subsegments}}
  \caption{Logical Real-time Segments Inside a Larger Ethernet Network}
  \label{fig:RT-segments}
\end{figure*}