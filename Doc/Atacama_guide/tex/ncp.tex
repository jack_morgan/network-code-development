\chapter{Hardware Modules for Real-Time Communication in Ethernet} \label{chapter:ncp}

The literature and recent developments clearly show the importance of hardware...

In this section...

\ac{ASIP}
\section{The Network Code ASIP} \label{sec:NCP-arch}

There was a previous version. 

%%%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \includegraphics[scale=0.43]{figures/Single-Core-NCP}
  \caption{Architecture of the Network Code Processor}
  \label{fig:NCP-architecture}
\end{figure}
%%%%%%%%%%%%%%%%
Figure~\ref{fig:NCP-architecture} shows the general architecture of the Network Code Processor (NCP), the ASIP that executes the instructions described in Section~\ref{sec:instruction-set}. We identify three main sections: the memory space, the Network Code Core (NCC), and the Ethernet core.

The memory space contains three memory blocks. The PROG block holds the programmed schedule. Each word on the MSG-CFG block defines a NC data buffer through an initial address and length, which are then mapped to locations on the MSG-DATA block that holds the actual data and serves as the interface between the computation tasks and the communication layer. Both PROG and MSG-CFG blocks must be filled during the configuration steps before runtime. In the current implementation, all memory blocks have 32-bits data buses. The NC instructions use a 32-bits codification, and the length of application data is measured in number of 32-bits words.
%Applications must write correctly, as the processor lacks of any mechanism to check for overflows.

The NCC executes the instructions stored in the PROG block and is where coordination takes place. We define a superscalar architecture that implements each instruction as a well-defined finite state machine mapped to separate hardware resources. The controller processes the instructions using a traditional fetch-decode-execute approach~\cite{Hennessy2006}, checking dependencies between consecutive instructions at runtime and triggering concurrent execution whenever it is possible. Each instruction block preforms a predefined number of steps before returning to the idle state, making the execution time of a program predictable with a single clock cycle precision.

Finally, the Ethernet core provides the interface to the physical medium. This section contains a standard Ethernet MAC, the interface to the physical transceiver (PHY), and exchanges data with the NCC through the dual-port \code{RT-Rx} and \code{RT-Tx} buffers. It also includes additional blocks for on-line frame processing tasks that separate best-effort from real-time frames and arbitrate the access to the output ports. Management of best-effort frames bases on well-known mechanisms and higher communication layers, and are not considered in the current prototype.

%%%%%%%%%%%%%%%%
\begin{figure}[t] \centering 
 \includegraphics[scale=0.64]{figures/frame_format}
  \caption{Real-time frame format}
  \label{fig:RT-frame-format}
\end{figure}
%%%%%%%%%%%%%%%% 

Figure~\ref{fig:NCP-paths} shows a diagram of the blocks and main signals in the transmission and reception paths, ordered from left to right according to the time they are accessed in a typical program. Transmission of a given variable $A$ (Figure~\ref{fig:NCP-tx-path}) starts with the \code{create()} block reading address $A$ of the MSG-CFG block to get the location and length of the application data stored in the MSG-DATA block, and moving it to the internal \code{send buffer}. The \code{send()} block reads this buffer, encapsulates the application data into an special Ethernet frame, and stores it in the \code{RT-Tx buffer}. Figure~\ref{fig:RT-frame-format} shows the format of the generated frames. Real-time data frames use an special value in EthType field (NC-DATA). Apart from the application data, the payload includes information such as the \code{TTL}, the data length (in 32-bit words), the logical channel, the ID of the local NC buffer, and a sequential frame counter. The receiver nodes use this information for processing and debugging purposes. The minimum size of an Ethernet frame is 64 bytes, and then the \code{send} block must add a padding to the payload in case the data is not large enough. The Frame Check Sequence (FCS) and the preamble are added at the MAC and PHY, respectively. The \code{sync()} instruction in master mode uses the \code{send} block to generate a dummy minimum sized frame tagged as NC-SYNC. The \code{Tx-MAC logic} block starts transmitting the real-time data as soon as the \code{RT-Tx buffer} asserts its non-empty flag.

On the receiver side (Figure~\ref{fig:NCP-rx-path}), a shift register delays the signals from the MAC to check the EthType field of incoming frames, separating real-time from best-effort ones. Valid real-time frames (\code{goodframe} signal asserted on frames tagged as either NC-DATA or NC-SYNC) automatically triggers the asynchronous \code{autoreceive} block, which parses the frame fields to execute the corresponding actions. For NC-DATA type frames, the block extracts the application data and store it in the corresponding \code{receive buffer} (there is one buffer per channel). The \code{receive()} block can then read the data from the specified channel and store it on the MSG-DATA location of the local variable $A$. If the schedule does not execute a \code{receive()} instruction, the data will be replaced by another data frame arriving from the same channel. For NC-SYNC type frames, the \code{autoreceive} block signals the controller to advertise the event, interrupting the waiting state from an \code{sync(slave)} instruction, setting the corresponding flag, and continuing with the program execution. 

The dual-port buffers separating the NCC from the Ethernet core allow us to use different clocks for each section. The frequency of the clocks driving both the transmission and reception logic in the Ethernet core is 125 Mhz for 1Gbps links. The \code{TX\textrm{clk}} is provided by an external source, and \code{RX\textrm{clk}} is directly recovered from the medium. The NCC section uses two different clocks: the NCC$_{\textrm{clk}}$ drives all the logic, and the NCC$_{\textrm{pulse}}$ provides the pulses for the countdown timers in the timing control instructions (\code{future} and \code{sync(slave)}). The frequency of these clocks will be restricted by the particular implementation. Because of the wider data buses, the NCC$_{\textrm{clk}}$ can run at a fraction of the clocks in the Ethernet core without being a bottleneck for line speed operation.

%The NCC uses 32-bits data buses, while the Ethernet MAC is restricted to operate with 8-bits buses. This means that the NCC can still operate at line speed running at a fraction (as low as a quarter) of the frequency in the physical link.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
  \centering
  \subfloat[Transmission path]{\label{fig:NCP-tx-path}
  \includegraphics[scale=0.66]{figures/NC_transmit_detailed}}
  \hspace{3mm}
  \subfloat[Reception path]{\label{fig:NCP-rx-path}
  \includegraphics[scale=0.66]{figures/NC_receive_detailed}}
  \caption{Block diagram for transmission and reception path in the NCP}
  \label{fig:NCP-paths}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Instruction Level Parallelism} \label{sec:instruction-parallelism}

To maximize the network utilization, the controller exploits hardware parallelism by calculating dependencies between consecutive instructions, triggering concurrent execution whenever it is possible. In the current architecture, we can identify two types of dependencies between consecutive instructions: control-flow and resource dependencies.

Control-flow dependencies between two consecutive instructions occur when the execution (or no-execution) of the second instruction depends on the result of the first one. Instructions \code{branch()}, \code{halt()}, and \code{sync(slave)} generate control dependencies in the programs. As described in Section~\ref{sec:instruction-set}, these instructions must wait for specific conditions to decide when and/or where continue execution. Therefore, the controller must stall the execution of a instruction following one of the previously listed. 

Resource dependencies occurs when two instructions access the same hardware resource. We can identify these kind of conflicts from the architecture described in Figure~\ref{fig:NCP-architecture}. The \code{create()}, \code{receive()}, and the guards for the \code{branch()} instructions require access to the MSG-DATA block. Because there is a single memory bus, they cannot run in parallel, and the second instruction must wait until the first one has released the bus. In the case of the \code{branch()} instruction, the guards access the memory bus only when operating over variables in the data buffers. %Therefore, if the accesed guard performs any other operation, the instruction can run in parallel with a create or send instruction.

We can note a similar conflict between the \code{send()} and \code{sync()} instructions. In master mode, the \code{sync()} instruction is basically a \code{send()} instruction with a special flag to generate a synchronization frame. As both instructions use the same microcode block, they cannot run concurrently. %The microcode for the \code{sync()} instruction in slave mode is directly implemented in the controller, and has no conflicts with the send instruction.

Another potential conflict with shared resources arises from \code{create()-send()} sequences. The \code{create()} block writes data into the send data buffer, and the \code{send()} instruction reads this buffer to create the Ethernet frame. However, the data flow is unidirectional, and we overcome this limitation by using a dual-port FIFO queue. The \code{send()} block can start reading the data while the \code{create()} block is still writing it. Preventing the \code{send()} block from reading an empty buffer is straightforward, as the it must first generate the frame headers before reading the data from the buffer.

%we must assure that there will be always data available for reading in the FIFO. This is not a big problem, because the send instruction must first create the headers of the Ethernet frame before reading the buffer for the data. Besides, mencionar acerca del ancho de los buses de lectura y escritura.

Table~\ref{tab:dependence-table} summarizes the dependencies for the NCP. The meaning of the characters are $w$ for wait until finished, $c$ for continue with next instruction, and $b$ for wait until the memory bus is available. To read the table, consider two sequential instructions \code{x()-y()}. The instruction \code{x()} specifies the column and \code{y()} specifies the row. For example, the sequence \code{branch()-send()} results in a sequential execution as specified by $w$, while the instructions \code{send()-branch()} can be executed in parallel. 

In the implementation, all instruction blocks indicate their running state to the controller. The controller uses these states to calculate the locking conditions during the decoding phase. After triggering the execution of one instruction, the controller immediately decodes the next one. If Table~\ref{tab:dependence-table} permits concurrent execution, the controller will trigger the second microcode block. Otherwise, it will stay on a waiting state until the lock is resolved. 

\begin{table}[t] \small
  \centering
  %\normalsize
  \caption{Summary of instruction dependencies}
  \vspace{2em}
  \begin{tabular}{c|ccccccccc}
%    &  \tikz \node [rotate=90] {nop};
%    &  \tikz \node [rotate=90] {create};
    &  \begin{rotate}{90} create \end{rotate}
%    &  \tikz \node [rotate=90] {send};
    &  \begin{rotate}{90} send \end{rotate}
%    &  \tikz \node [rotate=90] {receive};
    &  \begin{rotate}{90} receive \end{rotate}
%    &  \tikz \node [rotate=90] {sync};
    &  \begin{rotate}{90} sync(m) \end{rotate}
%    &  \tikz \node [rotate=90] {halt};
    &  \begin{rotate}{90} sync(s) \end{rotate}
    &  \begin{rotate}{90} halt \end{rotate}
%    &  \tikz \node [rotate=90] {future};
    &  \begin{rotate}{90} future \end{rotate}
%    &  \tikz \node [rotate=90] {if}; \\
    &  \begin{rotate}{90} branch \end{rotate} \\
    \hline
     create  & w & c & b & c & w & w & c & w \\
     send    & c & w & c & w & w & w & c & w \\
     receive & b & c & w & c & w & w & c & w \\
     sync(m) & c & w & c & w & w & w & c & w \\
     sync(s) & c & c & c & c & w & w & c & w \\
     halt    & c & c & c & c & w & w & c & w \\
     future  & c & c & c & c & w & w & w & w \\
     branch  & b & c & b & c & w & w & c & w \\
  \end{tabular}
\label{tab:dependence-table}
\end{table}

The controller block identify hazards between consecutive instructions, triggering concurrent execution whenever it is possible. The current architecture is prone to structural and control hazards.

Instructions \code{branch()}, \code{halt()}, and \code{sync(slave)} are a source of control hazards in the program, as they must wait for specific conditions to decide when and/or where continue execution.

From the diagram in Figure~\ref{fig:NCP-architecture} we identify two different sources of structural hazards:
\begin{itemize}
\item Instructions \code{create()}, \code{receive()}, and the guards for \code{branch()} require access to the MSG-DATA block. Because there is a single memory bus, they cannot run in parallel. In the case of the \code{branch()} instruction, the guards access the memory bus only when operating over variables in the data buffers
\item Instructions \code{send()} and \code{sync()} in master mode use the same block to generate Ethernet frames, and then they cannot run concurrently
\end{itemize}

The \code{send buffer} also represents a potential structural hazard between the \code{create()} and \code{send()} instructions. We overcome this problem using a dual-port FIFO, which allows the \code{send()} block to start reading the buffer while the \code{create()} block is filling it.

%we must assure that there will be always data available for reading in the FIFO. This is not a big problem, because the send instruction must first create the headers of the Ethernet frame before reading the buffer for the data. Besides, mencionar acerca del ancho de los buses de lectura y escritura.

Table~\ref{tab:dependence-table} summarizes the dependencies for the NCP. The meaning of the characters are $w$ for wait until finished, $c$ for continue with next instruction, and $b$ for wait until the memory bus is available. To read the table, consider two sequential instructions \code{x()-y()}. The instruction \code{x()} specifies the column and \code{y()} specifies the row. For example, the sequence \code{branch()-send()} results in a sequential execution as specified by $w$, while the instructions \code{send()-branch()} can be executed in parallel. 

In the implementation, all instruction blocks indicate their running state (idle/busy) to the controller. The controller uses these states to calculate the locking conditions during the decoding phase. After triggering the execution of one instruction, the controller immediately decodes the next one. If Table~\ref{tab:dependence-table} permits concurrent execution, the controller will trigger the second microcode block. Otherwise, it will stay on a waiting state until the lock is resolved, stalling the fetch of subsequent instructions.

\begin{table}[t] \small
  \centering
  %\normalsize
  \caption{Summary of instruction dependencies}
  \vspace{2em}
  \begin{tabular}{c|ccccccccc}
%    &  \tikz \node [rotate=90] {nop};
%    &  \tikz \node [rotate=90] {create};
    &  \begin{rotate}{90} create \end{rotate}
%    &  \tikz \node [rotate=90] {send};
    &  \begin{rotate}{90} send \end{rotate}
%    &  \tikz \node [rotate=90] {receive};
    &  \begin{rotate}{90} receive \end{rotate}
%    &  \tikz \node [rotate=90] {sync};
    &  \begin{rotate}{90} sync(m) \end{rotate}
%    &  \tikz \node [rotate=90] {halt};
    &  \begin{rotate}{90} sync(s) \end{rotate}
    &  \begin{rotate}{90} halt \end{rotate}
%    &  \tikz \node [rotate=90] {future};
    &  \begin{rotate}{90} future \end{rotate}
%    &  \tikz \node [rotate=90] {if}; \\
    &  \begin{rotate}{90} branch \end{rotate} \\
    \hline
     create  & w & c & b & c & w & w & c & w \\
     send    & c & w & c & w & w & w & c & w \\
     receive & b & c & w & c & w & w & c & w \\
     sync(m) & c & w & c & w & w & w & c & w \\
     sync(s) & c & c & c & c & w & w & c & w \\
     halt    & c & c & c & c & w & w & c & w \\
     future  & c & c & c & c & w & w & w & w \\
     branch  & b & c & b & c & w & w & c & w \\
  \end{tabular}
\label{tab:dependence-table}
\end{table}
