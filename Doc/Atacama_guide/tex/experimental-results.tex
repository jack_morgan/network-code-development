\newpage
\part{Experimental Characterization}
%\section{Experimental Characterization} \label{chapter:experimental-results}

%This chapter presents a detailed description and experimental characterization of the prototypes that implement the \ac{NIC}-Only and \ac{NIC}/Switch configurations introduced in Chapter~\ref{chapter:hardware-modules}. The developed components for these two configurations are part of the Atacama framework, which is being released as an open-source project. This dissertation, excludes the experimental evaluation Switch-Only configuration from the analysis, since it is not possible to characterize the end-to-end latency, and it is also limited to micro-segmented topologies. For further details on the characterization and discussion of early prototypes on the Switch-Only configuration see~\cite{Carvajal:TC-2012}. The modules are not part of Atacama, but are still available as an open-source project.

%The reported results are based on the most recent version of the prototypes at the time of completion of this thesis, and provide hard experimental evidence of the achieved timing properties of the evaluated architectures. However, due to the open-source nature of the framework, the prototypes are subject to further revisions and optimizations, and thus the absolute numbers reported here should only be used as a reference.


\section{Implemented Prototypes}

This section describes some implementation details for the prototypes that serve as an evaluation platform for the \ac{NC-ASIP} and custom switch forwarding logic. Without loss of generality, we target the NetFPGA development board~\cite{NetFPGA_page} as the unified underlying technology due to its widespread utilization in prototyping and testing of high-performance networking applications. The following paragraphs overview the properties of the NetFPGA, and describe some implementation details of the custom \ac{NIC}s and switches.

\subsection{NetFPGA Platform}

The NetFPGA is an open and low-cost platform targeted to the implementation of reusable hardware code for high-performance networking applications. The platform package consists of three elements: (1) an FPGA-based development board, (2) software tools to integrate the board with a host workstation, and (3) fully functional examples and application projects available in an open-source basis.

The version 2.1 of the hardware board includes a Xilinx Virtex2 Pro 50 \ac{FPGA} chip, a quad-port 1[Gbit/s] \ac{PHY}, and additional hardware resources to implement networking devices. The board includes a \ac{PCI} connector that allows integration to a standard computer for reconfiguration and debugging.

The basic software tools include a Linux device driver and a set of utilities that enable access to the board from programs running in the host computer. Using these utilities, the user can easily read/write the NetFPGA's internal memory, and even reconfigure the FPGA functionality by downloading bitfiles directly from the workstation without needing any additional cable or even physical access to the board.

Finally, the most remarkable aspect of the NetFPGA comes from the third element: all the codes for example projects and applications, including the \ac{HDL} code for the FPGA logic and all the software utilities, are offered in an open-source way through the official website. This reduces considerably the cost of implementing new components by either directly using already tested modules or adapting them for any specific needs.

State-of-the-art hardware, low cost, user friendly interfaces, and an open-source philosophy, set the NetFPGA platform as a referent on the design of customized high-performance networking devices. Nowadays, a constantly increasing number of users, including students, teachers and researchers from around the world, take advantage of the platform and a growing number of available open-source application projects to prototype, implement, test, use and share new networking hardware designs in a reduced time.

\subsection{NC-ASIPs Evaluation Platform}

We prepared an \ac{HDL} description of the \ac{NC-ASIP} described in Section~\ref{sec:NCP-arch}. The current prototype supports schedules of up to 256 instructions, 256 variable definitions which are mapped to a 16 [KB] memory for application data (4096 32-bit words), and four logical channels for reception of application data. The \ac{NC-ASIP} is hard-coded to process and classify frames according to the EthType values listed in Table~\ref{table:ethtypes}. We arbitrarily chose these values for evaluation purposes among the unassigned values from the IEEE EtherType public listing~\cite{Ethtype-public-listing}. These values are easily modifiable in the hardware description.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}\centering
\caption{EthType values for real-time operation}
\ra{1.3}
\label{table:ethtypes}
\begin{tabular}{@{}cc@{}}\toprule
Frame Type  & Value \\\midrule
NC-SYNC     & 0x5CFF        \\
NC-DATA     & 0x5CE0         \\
NC-ACK      & 0x5CAA           \\
%BE          & 0x0800         \\
\bottomrule
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Using a generic \ac{HDL} description, we prepared an \ac{IP} core that fits the Virtex2 chip in the NetFPGA board. The core encapsulates all the functionality of the \ac{NC-ASIP}, and includes additional logic to interface the PCI bus, enabling easy configuration and debugging from the host workstation.

Figure~\ref{fig:NCP-eval-platform} shows a diagram of the evaluation platform containing four independent instances of the IP core in a single board. The PCI port provides access to the configuration block of all the instances. This block contains the memory space (see Figure~\ref{fig:NCP-architecture}), a control register that configures the NC-ASIPs, and debugging counters that keep track of run time status and statistics (number of received and transmitted frames and bytes, synchronization and run time errors, etc.). We include a set of software tools that enable easy access to the configuration space from a host workstation running Linux. The user can test different scenarios by filling the PROG blocks with pre-programmed schedules, emulate real-time tasks by reading/writing the data buffers, and set/check the status of each unit through the control register and debug counters.

For simplicity, and due to the limited clocking resources in the Virtex2 chip, we limit the platform to operate exclusively on 1[Gbit/s] links, which sets the \TXclk and \RXclk driving the Ethernet core to operate at 125[Mhz] (see Section~\ref{sec:NCP-arch}). The post Place and Route timing analysis reported a maximum frequency of 77[Mhz] for the \NCclk when implementing four instances of the \ac{NC-ASIP}. Because the \ac{NCC} has wider data buses than the Ethernet core, it is still possible to operate at the link speed using a slower clock in the \ac{NCC}. For the current prototype, we set the \NCclk to 62.5[Mhz]. The \ac{NC-ASIP} includes a configuration register to set the frequency of the \NCpulse for the timers in timing control instructions as a fraction of the \NCclk.

%%%%%%%%%%%%%%%%
\begin{figure}[t] %\small
 \centering
 \includegraphics[scale=0.7]{figures/netfpga-nic-eval}
  \caption{Evaluation Platform for Real-time Stations}
  \label{fig:NCP-eval-platform}
\end{figure}
%%%%%%%%%%%%%%%%

\subsection{Enhanced Switch Platform}

The custom switch prototype is based on the open reference switch design available for the NetFPGA. This fully functional design implements a four-ports switch, using a modular approach which makes easy to add new blocks for extra functionality. Consequently, we designed the custom forwarding modules (see Section~\ref{sec:enhanced-switch}) to fit the signal convention of the reference design. We then simply added the new modules by tapping the data bus between the MAC and the input buffers to integrate the on-the-fly classifier, without any additional modification to the original switching mechanism. For the output logic, we tapped the data bus for ongoing frames to include the selector that assigns the port to either real-time path or switching fabric. The input classifier on each port identifies and processes real-time frames based on the EthType values shown in Table~\ref{table:ethtypes}.

In this case, all the clocks run at 125[MHz] (see Section~\ref{sec:enhanced-switch}).

\section{Timing Characterization of the Prototypes}

This section presents a detailed characterization of the timing behavior for the implemented prototypes. The experimental setup considers a multi-hop Ethernet configuration using the devices described in the previous Section. 


\subsection{Experimental Setup}
Figure~\ref{fig:experimental-setup} shows the general setup used for the experimental evaluation of the prototypes. We attached four NetFPGA boards to the PCI ports of a single workstation. One board holds the NC-ASIPs evaluation platform, and the others the enhanced switch. The software tools provide easy access to configure and debug different operation scenarios.

%%%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \includegraphics[scale=1]{figures/experimental-setup}
  \caption{Experimental Setup}
  \label{fig:experimental-setup}
\end{figure}
%%%%%%%%%%%%%%%%

The prototypes hold additional logic to interface the Xilinx's Chipscope tool, which serves as a logic analyzer for the FPGA's internal signals. This tool allows us to measure the execution time for processing tasks inside the devices with high precision in a frame-per-frame basis. The setup also includes an IXIA 400T traffic generator, which allows us to collect long-term statistics over controlled streams, including latency measurement with a resolution of \sins{20}.

For all the experiments, we connected the ports in the different devices using \simt{7.62} long Cat6 cables.

\section{Execution Time of Schedules}

Figure~\ref{fig:execution-time} shows the measured execution time for the \code{create}, \code{send}, \code{receive}, and \code{branch} instructions as a function of $v_l$, corresponding to the length of the application data they process (in 32-bits words). The reported values consider the number of \NCclk cycles since the controller triggers the corresponding state machine, until it returns to the idle state. For the \code{branch} instruction, the reported value corresponds to the comparison of two variables of the same length, which is the guard with the longest execution time. As we see, the execution time grows linearly as a function of the variable length. Because the minimum size of generated frames is 64~bytes, the \code{send} instruction has a constant execution time for variables shorter than 11 words. The measured times for each length remain constant through 50 samples, indicating that the execution time for the instructions block inside the NCC is predictable. This emphasizes the clear advantage of using hardware implementations for the scheduling units with respect to traditional software stacks, which are unable to provide the determinism and speed for the efficient utilization of the Ethernet infrastructure~\cite{Fischmeister2007, grillinger:2006-88} 
%%%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \includegraphics[scale=0.7]{figures/matlab/execution_time}
  \caption{Execution Time for NC Instructions}
  \label{fig:execution-time}
\end{figure}
%%%%%%%%%%%%%%%%

Figure~\ref{fig:NCP-timing} shows the execution flow of instructions inside the \ac{NCC} for an example schedule. The schedule in Figure~\ref{fig:schedule-for-timing} emits a frame with a five-words variable defined in the second position of the \code{MSG-CFG} block every three cycles of the \NCpulse. In the example, the frequency of the \NCpulse is 1/100 of the frequency of the \NCclk. After sending the frame, the schedule compares two variables to decide whether or not to retrieve the variable data stored in the receiver buffer from a previous transmission from another node. The timing diagrams in Figure~\ref{fig:timing-diagram} show that the controller triggers concurrent execution of multiple blocks according to the rules described in Table~\ref{tab:dependence-table}. The controller triggers the concurrent execution of the \code{future}, \code{create}, \code{send}, and \code{branch} instructions, with a delay of six \NCclk cycles between sequential instructions, which accounts for the fetch and decoding tasks. However, the execution of the \code{receive} instruction depends on the result of the preceding \code{branch} (and in this case they also have hardware dependencies as both access the variable space). In the example, the \code{branch} returns \code{TRUE}, and thus the controller triggers concurrent execution of the \code{receive} and \code{halt} instructions. The schedule then waits for the expiration of the countdown timer started on the \code{future} instruction, repeating the process every three \NCpulse.

In the Ethernet core, the \ac{MAC} starts transmitting the frame as soon as the \code{RT-Rx FIFO} asserts its non-empty signal. As we can see, the latency associated to the creation of a frame in the NCC is lower than the latency for transmitting it to the medium (\code{tx\_dv} signal), which indicates that the NC-ASIP can process application data for line rate operation.

\begin{figure*}
  \centering
  \subfloat[Example Schedule]{\label{fig:schedule-for-timing}
  \hspace{1cm}
  \begin{minipage}{4.7cm}
  %%\vspace{-4cm}
  \centering
	\lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/s_example.ncode} 
	\end{minipage}} \\
%%  \hspace{2cm}
  \subfloat[Timing Diagram for Execution of Example Schedule]{\label{fig:timing-diagram}
  \includegraphics[scale=0.8]{figures/timing_diagram}}
  \caption{Execution Time for an Example Schedule in the End Station}
  \label{fig:NCP-timing}
\end{figure*}


\subsection{End-to-end Latency}
We define the \ac{EEL} for a real-time frame as the time required to move a byte from the \code{MSG-DATA} in the source to the \code{receive-FIFO} in the destination. In a Ethernet network using the enhanced devices, the \ac{EEL} between two stations with $N_S$ switches in the path is:
%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
\textrm{EEL} = \textrm{Tx}_\textrm{ASIP} + \textrm{Rx}_\textrm{ASIP} + N_S \textrm{Sw} + (N_S + 1) \textrm{L}
\label{eq:EEL}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%
where
\begin{itemize}
\item Tx$_\textrm{ASIP}$: defined for a \code{create-send} sequence, without further instructions in the middle. Time span since triggering the \code{create} block until the first byte of application data leaves the MAC in the Ethernet core. Note that this definition assumes that the schedule transmit a variable immediately after creating it. In the case of requiring further processing between the creation and sending of the variable, the time must be modeled on each case according the particular instructions between the \code{create} and \code{send}.

\item Sw: switch forwarding time since the first byte of application data arrives to the MAC in the input port until it leaves the MAC in the output port.\

\item Rx$_\textrm{ASIP}$: time span since the first byte of application data arrives to the MAC in the destination until it reaches the \code{receive-FIFO} for the corresponding channel.

\item L: effects of physical links. Time span since the first byte of data leaves the MAC in the origin port until it reaches the MAC in the other end.
\end{itemize}

We characterized these parameters on the implemented prototypes. Table~\ref{table:latency} shows the maximum value for each term in~(\ref{eq:EEL}) as a function of $v_l$, measured on each device using the Chipscope tool with a sampling clock of \siMhz{125}. The reported value corresponds to the highest value (worst-case) observed in a set of 100 samples for each case. For all cases, the difference between the highest and lowest value in the sample was never higher than two cycles of the sampling clock. We attribute these variations to the drift between the different clock domains in the devices and the sampling clock.

Both NC-ASIPs and switches start transmission as soon as they detect available data in the FIFOs, and thus the latency is independent of $v_l$. The total latency in the switch includes a delay of 32 cycles related to the input classifier, and 76 cycles before the \code{RT-Tx-FIFO} gets access to the MAC in the output port. As discussed in Section~\ref{sec:rt-switch-forwarding}, this waiting time accounts for the IFG (12 cycles) and the transmission time of a minimum sized frame (64 cycles). The rest accounts for internal processing. In the case of the links, the latency  will depend on the specific configuration, and must be characterized for each case. For this experiment, each link considered two Broadcom BCM5464SR PHYs linked with \simt{7.62} CAT6 cables. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}\centering
\caption{Maximum Observed Latency}
\ra{1.3}
\label{table:latency}
\begin{tabular}{@{}lcc@{}}\toprule
                        & Max. latency [\siMhz{125} clock pulses]	\\\midrule
    Tx$_\textrm{ASIP}$ 	& 60  \\
    Sw			            & 148  \\
    Rx$_\textrm{ASIP}$	& 94, for $v_l \leq$11  \\
    										& 94 + 4($v_l$-11), for $v_l>$11 \\
    L				            &  48 \\
\bottomrule
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Replacing the observed values in~(\ref{eq:EEL}), we model the worst-case \ac{EEL} in the experimental setup as:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\textrm{WCPL}(N_S) = 203 + 197 N_S  [pulses]
\begin{equation}
\textrm{EEL}(N_S) = 1.624 + 1.576 N_S  [\mu s]
\label{eq:WCPL1}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for $v_l \leq 11$, and

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\textrm{WCPL}(N_S) = 159 + 197 N_S + 4 v_l  [pulses]
\begin{equation}
\textrm{EEL}(N_S, v_l) = 1.272 + 1.576 N_S + 0.032 v_l  [\mu s]
\label{eq:WCPL2}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for $vl >11$.

To verify the model, we directly measured the \ac{EEL} between two instances of the NC-ASIP implemented on a single NetFPGA using a reference global clock, which exchange real-time frames across three enhanced switches. We additionally connected three ports of a traffic generator broadcasting best-effort frames at 800[Mbit/s] each. Table~\ref{table:WCPL} compares the highest observed latency from a set of 100 samples for each case, to the worst-case value obtained from~(\ref{eq:WCPL1}) and~(\ref{eq:WCPL2}). As we see, even under the highly saturated scenario for best-effort traffic, the models provide a tight and effective upper-bound for the latency of real-time frames.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}\centering
\caption{Worst-case End-to-end Latency [$\mu$s]}
\ra{1.3}
\label{table:WCPL}
\begin{tabular}{@{}lccc@{}}\toprule
    $v_l$ [words]		 			& model [$\mu$s] & observed	[$\mu$s] & error	\\\midrule
    5 					& 6.352				&   6.32				&	 0.51 \%  \\
    20 	&       		6.64				&    6.608		&	 0.48 \% \\
    35	   	&    		7.12			&    7.088    	&	 0.45 \% \\
    70	&	8.24								&	8.224					&  0.20 \%  \\
\bottomrule
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Robustness Against Co-existing Best-effort Traffic}
\label{subsec:robustness}

In this experiment we used an IXIA traffic generator to collect long-term statistics over controlled streams. One port emits two periodical reference streams of timestamped frames. One stream generates frames of configurable length tagged as \code{NC-SYNC}, with a period of 100[$\mu$s]. The other stream is similar, but generates regular best-effort frames. These streams propagate through three switches to a second port in the generator which collects the instantaneous propagation latency for each type. Two additional ports broadcast raw best-effort frames of 1000[B] at a rate of 470[Mbit/s] each. This configuration generates an aggregated bandwidth close to over-utilization, and allows us to stress the effects of jitter in the latency of the different classes before starting to drop frames.

Fig.~\ref{fig:robustness} shows the observed latency versus the transmission rate on the reference streams. Latency measurements are only valid when there are no dropped frames. The markers show the average latency over a sample set of $10^{7}$ frames for each type. The ends of the bars represent the minimum and maximum value on each set. On the one hand, we see that the latency and jitter of the reference best-effort stream increase with the transmission rate. When the rate of timestamped streams raises over 50[Mbit/s], the total traffic exceeds the capacity of the links, and the reference best-effort stream starts reporting dropped frames. On the other hand, the real-time path provides a bounded latency and no losses for real-time frames, independent of the rate of best-effort traffic. The \emph{cut-through} forwarding in the real-time path also reduces the propagation latency in relation to the traditional \emph{store-and-forward} used in most \ac{COTS} switches.
%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \includegraphics[scale=0.75]{figures/matlab/robustness}
  \caption{Robustness of Real-time Latency}
  \label{fig:robustness}
\end{figure}
%%%%%%%%%%%%%%