load data_figures_paper3
close all;

f=figure;
for i=1:7
    latency_COTS_robustness_no_mean(i,:) = latency_COTS_robustness(i,:)-mean(latency_COTS_robustness(i,:));
end

for i=1:7
    latency_COTS_robustness_no_mean(i,:) = latency_COTS_robustness(i,:) - min(latency_COTS_robustness(i,:));%   -mean(latency_COTS_robustness(i,:));
end

latency_RT_robustness_no_mean = latency_RT_robustness(1,:) - mean(latency_RT_robustness(1,:));

boxplot([latency_COTS_robustness_no_mean]', 'labels', boxplot_labels);
xlabel('Transmission rate on BE stations [Mbps]');
ylabel('Lateness [s]');
grid on;
MyFigStyle_IEEE_journal(f, 1, 'boxplot_COTS_robustness.eps', 'NorthEast');