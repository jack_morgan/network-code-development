close all
branch = 9:2:607;
create = 7:1:306;
send = 15:1:303;
receive= 7:1:306;

send = [ones(1,11)*14 send];

words = 1:300;

n_samples = 50;
sampling_step = 5;
figure
plot(words(1:sampling_step:n_samples), create(1:sampling_step:n_samples), '-o');
hold on;
plot(words(1:sampling_step:n_samples), send(1:sampling_step:n_samples),'-s');
plot(words(1:sampling_step:n_samples), branch(1:sampling_step:n_samples), '-d');
plot(words(1:sampling_step:n_samples), receive(1:sampling_step:n_samples), '-*');

legend('create', 'send', 'branch', 'receive')
xlabel('32-bit words');
ylabel('NCC_{clk} cycles');
grid on
MyFigStyle(1,1,'execution_time.eps','NorthWest',1);