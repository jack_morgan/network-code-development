% function [result msg] =MyFigStyle(H, print_option, nombre);
% 
% plot_parameters=struct('Color',[0 0 0],...
%                        'LineWidth',2,...
%                        'MarkerSize',10,...
%                        'MarkerEdgeColor','k',...
%                        'MarkerFaceColor', 'none');                           
%                            
% label_parameters= struct('FontName', 'Helvetica',...
%                          'FontSize', 16,...
%                          'FontWeight', 'bold');
%                      
% title_parameters= struct('FontSize', 13,...
%                          'FontWeight', 'bold');                     
% 
% % title_parameters= struct('FontName', 'Helvetica',...
% %                          'FontSize', 10,...
% %                          'FontWeight', 'bold'); %bold
%                      
% axes_parameters= struct( 'FontName', 'Helvetica',...
%                          'FontSize', 16,...
%                          'FontWeight', 'normal');
%                      
% legend_parameters= struct('FontName', 'Helvetica',...
%                          'FontSize', 16,...
%                          'Location', 'SouthEast');
%                          
% fig_parameters=struct( 'PaperUnits' , 'centimeters',...
%                        'PaperSize' , [15 10]);
%                        %'PaperPosition',[0, 0, 12, 9]);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% n_figures=length(H);
% j=1;
% legend_handle=0;
% l=1;
% %adquisicion de datos de la figura
% for i=1:n_figures
%     figure(H(i));
%     subplots=get(H(i), 'Children');
%     n=length(subplots);
%     for k=2:2
%         axes_handle(l)=subplots(k);
%         curves_handle(:,l)=get(axes_handle(l),'Children');
%         aux=legend(axes_handle(l));
%         if ~isempty(aux)
%             legend_handle(j)=aux;
%             j=j+1;
%         end
%         xlabel_handle(l)=get(axes_handle(l),'XLabel');
%         ylabel_handle(l)=get(axes_handle(l),'YLabel');
%         title_handle(l)= get(axes_handle(l), 'Title')
%         l=l+1;
%     end
% end
% 
% %seteo de propiedades
% set(curves_handle, plot_parameters);
% set(axes_handle,axes_parameters);
% set(xlabel_handle,label_parameters);
% set(ylabel_handle,label_parameters);
% set(title_handle, title_parameters)
% 
% if legend_handle~=0;
%     set(legend_handle, legend_parameters);
% end
% 
% set(H, fig_parameters);
% 
% if print_option==1
%    print('-depsc',nombre)
%    [result,msg] = eps2pdf(nombre, 'C:\Program Files\gs\gs8.54\bin\gswin32.exe',0);
% else result=0; msg=0;
% end

%function [result msg] =MyFigStyle(H, print_option, nombre);
function [result msg] =MyFigStyle(H, varargin); %(figure, print_option, nombre, legend_loc, linewidth)

if length(varargin)==0
    print_option=0;
    nombre=strcat('figure',int2str(H),'.eps');
    legend_location='Best';
    linewidth=1;
end

if length(varargin)==1
    print_option=cell2mat(varargin(1));
    nombre=strcat('figure',int2str(H),'.eps');
    legend_location='Best';
    linewidth=1;
end

if length(varargin)==2
    print_option=cell2mat(varargin(1));
    nombre= cell2mat(varargin(2));
    legend_location='Best';
    linewidth=1;
end

if length(varargin)==3
    print_option=cell2mat(varargin(1));
    nombre=cell2mat(varargin(2));
    legend_location=cell2mat(varargin(3));
    linewidth=1;
end

if length(varargin)==4
    print_option=cell2mat(varargin(1));
    nombre=cell2mat(varargin(2));
    legend_location=cell2mat(varargin(3));
    linewidth=cell2mat(varargin(4));
end

plot_parameters=struct('Color',[0 0 0],...
                       'LineWidth',linewidth,...
                       'MarkerSize',8,...
                       'MarkerEdgeColor','k',...
                       'MarkerFaceColor', 'none');                           
                           
label_parameters= struct('FontName', 'Helvetica',...
                         'FontSize', 13,...
                         'FontWeight', 'normal');

% title_parameters= struct('FontName', 'Helvetica',...
%                          'FontSize', 10,...
%                          'FontWeight', 'bold'); %bold
                     
axes_parameters= struct( 'FontName', 'Helvetica',...
                         'FontSize', 13,...
                         'FontWeight', 'normal');
                     
legend_parameters= struct('FontName', 'Helvetica',...
                         'FontSize', 13,...
                         'Location', legend_location);
                         
%fig_parameters=struct( 'PaperUnits' , 'centimeters',...
%                        'PaperSize' , [10 10]);
                        %'PaperPosition',[0, 0, 12, 9]);
                       
fig_parameters=struct(  'Units', 'centimeters',...
                        'Position', [1 5 15 9],...
                        'PaperPositionMode', 'auto');
                       %'PaperPosition',[0, 0, 10, 7]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=length(nombre);
if p>=4
    if nombre(end-3:end) ~= '.eps'
        nombre=strcat(nombre,'.eps');
    end
else nombre=strcat(nombre,'.eps');
end
n_figures=length(H);
j=1;
legend_handle=0;

%adquisicion de datos de la figura
for i=1:n_figures
    figure(H(i));
    axes_handle(i)=gca;
    curves_handle(:,i)=get(axes_handle(i),'Children');
    aux=legend(axes_handle(i));
    if ~isempty(aux)
        legend_handle(j)=aux;
        j=j+1;
    end
    xlabel_handle(i)=get(axes_handle(i),'XLabel');
    ylabel_handle(i)=get(axes_handle(i),'YLabel');
end

%seteo de propiedades
set(curves_handle, plot_parameters);
set(axes_handle,axes_parameters);
set(xlabel_handle,label_parameters);
set(ylabel_handle,label_parameters);

if legend_handle~=0;
    set(legend_handle, legend_parameters);
end

set(H, fig_parameters);

if print_option==1
   print('-depsc',nombre)
   [result,msg] = eps2pdf(nombre, 'C:\Program Files\gs\gs9.05\bin\gswin64.exe',0)
else result=0; msg=0;
end