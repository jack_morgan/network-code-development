\input{tex/configuration}

\usepackage{semantic}


\newcommand{\HRule}[1]{\rule{\linewidth}{#1}}

\begin{document}

%\include{tex/titlepage}
%------------------------------ Title ------------------------------
\thispagestyle {empty}
\vspace*{\stretch{1}}
\begin{center}
	\noindent \HRule{1.5mm} \\[5mm]
	\noindent \Large \textbf{Network Code Language Specification} \\
	\noindent \normalsize \textbf{Instruction Set \& Bytecode}\\[3mm]
	\noindent \HRule{1.5mm}
	\vspace*{2cm}
	{\small Version: 3.0.1} \\
	\vspace*{\stretch{1}}
\end{center}

%------------------------------ Authors ------------------------------
\newpage
\hspace{2cm}
\vspace{\stretch{1}}
\begin{center}
	Corresponding authors:\\[2mm]
	Gonzalo Carvajal \\
	Department of Electrical and Computer Engineering \\
	University of Waterloo \\
	\url{gcarvaja@uwaterloo.ca} \\[5mm]
	%
	Sebastian Fischmeister \\
	Department of Electrical and Computer Engineering \\
	University of Waterloo \\
	\url{sfischme@uwaterloo.ca} \\[5mm]
	%
	Luis Araneda \\
	Department of Electrical Engineering \\
	Universidad de Concepcion, Chile \\
	\url{luaraneda@udec.cl} \\[5mm]
\end{center}
%
\vspace{\stretch{1}}
Additional collaborators:
\begin{itemize}
	\item Robert Trausmuth (\url{trausmuth@fhwn.ac.at})
	\item Madhukar Anand (\url{anandm@cis.upenn.edu}),
	\item Gregor K\"onig (\url{gregor@gregorkoenig.info}),
	\item Insup Lee (\url{lee@cis.upenn.edu}),
	\item Ben (Ching-Pei) Lin (\url{c7lin@engmail.uwaterloo.ca}),
	\item Oleg Sokolsky (\url{sokolsky@cis.upenn.edu}).
\end{itemize}
%
\vspace{\stretch{1}}
Associated web page(s) are:
\begin{itemize}
	\item \url{https://esg.uwaterloo.ca}
\end{itemize}

\vspace{\stretch{1}}


%------------------------------ TOC ------------------------------
\cleardoublepage
\pdfbookmark{\contentsname}{toc}
\tableofcontents

%------------------------------ Specification ------------------------------
\cleardoublepage

This document details the codeset description for the NCM core. It is organized in sections, each representing one instruction category.

\section{Control Flow Instructions}

\subsection{Future(delay, address)}

The future instruction adds an entry to the future list \emph{flist}.

\begin{itemize}
	\item The parameter \code{delay} specifies the delay until the timeout for the trigger.
	\item The parameter \code{address} specifies the jump address in the program.
\end{itemize}

\begin{tabular}{l|l}
	aaaabbbbbbbb0000cccccccccccccccc & a \dots the operation code (4 bits) [0101] \\
	& b \dots the jump address (8 bits) \\
	& c \dots the delay value (16 bits) \\
\end{tabular}

\subsection{Halt()}

The halt instruction suspends NCM until it is resumed by an event from the future queue. It also
starts evaluating the future queue.

\begin{tabular}{l|l}
	aaaa000000000000000000000000000 & a \dots the operation code (4 bits) [0100] \\
\end{tabular}

\subsection{If(guard, jmp)}

The if instruction implements a conditional jump. If the guard condition returns true, then the jump
is made and the program counter is set to the specified address. Otherwise the interpreter continues
with executing the next instruction.

\begin{itemize}
	\item The parameter guard encodes the guard function and possible values passed to the function.
	\item The parameter addr encodes the jump address that will be used as the new program counter
	value, if the guard returns True.
\end{itemize}

\begin{tabular}{l|l}
	aaaabbbbbbbbcccccccccccccccccccc & a \dots the operation code (4 bits) [0111] \\
	& b \dots jump address (8 bits) \\
	& c \dots guard mask(20 bit) \\
\end{tabular}


\subsubsection{Guard Mask Values}

The guard mask is divided into two groups: Mask values lower than 8 are status queries, mask values
above 8 are variable queries.

\begin{itemize}
	\item\textbf{AlwaysTrue}: Always returns the value \emph{True}. \\
	\begin{tabular}{l|l}
		gggg0000000000000000 & g \dots guard identification (4 bits) [0111]
	\end{tabular}
	%----------
	\item\textbf{AlwaysFalse}: Always returns the value \emph{False}. \\
	\begin{tabular}{l|l}
		gggg0000000000000000 & g \dots guard identification (4 bits) [0000]
	\end{tabular}
	%----------
	\item\textbf{ChannelXYReceiveBufferEmpty}: Returns \emph{True} if the empty flag of the channel receive buffer \emph{crb} is set for specified channel. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbb00000000 & g \dots guard identification (4 bits) [0001] \\
		                     & b \dots channel identification (8 bits)
	\end{tabular}
	%----------
	\item\textbf{SendBufferEmpty}: Returns \emph{True} if the empty flag of the send buffer \emph{sb} is set. \\
	\begin{tabular}{l|l}
		gggg0000000000000001 & g \dots guard identification (4 bits) [0001]
	\end{tabular}
	%----------
	\item\textbf{TestCount}: Tests the counter value against a defined value. If the two values are	equivalent, then the guard will return \emph{True}; and \emph{False} otherwise. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbbbbbbbbbb & g \dots guard identification (4 bits) [0010] \\
		                     & b \dots test value (16 bits)
	\end{tabular}
	%----------
	\item\textbf{StatusTest}: Checks for certain error conditions. The error conditions use priority encoding from left (Bit 15) to right (Bit 0). \\
	\begin{tabular}{l|l}
		ggggbbbbbbbbbbbbbbbb & g \dots guard identification (4 bits) [0011] \\
		                     & b \dots test value (16 bits)
	\end{tabular}\\ \\
	\begin{tabular}{l|c|l}
		\textbf{Bit} & \textbf{Mask} & \textbf{Description} \\
		\hline
		Bit 15 & 0x8000 & Sync Timeout   \\
		Bit 14 & 0x4000 &  \\
		Bit 13 & 0x2000 & \\
		Bit 12 & 0x1000 & \\
		Bit 11 & 0x0800 & \\
		Bit 10 & 0x0400 & Receive Fault \\
		Bit 9 & 0x0200 & \\
		Bit 8 & 0x0100 &  \\
		Bit 7 & 0x0080 & User Bit 7 \\
		Bit 6 & 0x0040 & User Bit 6 \\
		Bit 5 & 0x0020 & User Bit 5 \\
		Bit 4 & 0x0010 & User Bit 4 \\
		Bit 3 & 0x0008 & User Bit 3 \\
		Bit 2 & 0x0004 & User Bit 2 \\
		Bit 1 & 0x0002 & User Bit 1 \\
		Bit 0 & 0x0001 & User Bit 0
	\end{tabular}
	%----------
	\item\textbf{EqualVarVar}: Compares two variables and returns \emph{True} if the two values are the same. The comparison first checks the length of the variables. If the length is equal a wordwise subtract $(varc - varb)$ is done until the result is not equal to zero (greater/less) or the length is reached, in which case the two are considered the same. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbbcccccccc & g \dots guard identification (4 bits) [1000] \\
		                     & b \dots variable identification (8 bits) \\
		                     & c \dots variable identification (8 bits)
	\end{tabular}
	%----------
	\item\textbf{GreaterVarVar}: Compares two variables and returns \emph{True} if the first variable is greater than the second one. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbbcccccccc & g \dots guard identification (4 bits) [1001] \\
		                     & b \dots variable identification (8 bits) \\
		                     & c \dots variable identification (8 bits)
	\end{tabular}
	%----------
	\item\textbf{LessVarVar}: Compares two variables and returns \emph{True} if the first variable is less than the second one. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbbcccccccc & g \dots guard identification (4 bits) [1010] \\
		                     & b \dots variable identification (8 bits) \\
		                     & c \dots variable identification (8 bits)
	\end{tabular}
	%----------
	\item\textbf{TestVar}: Returns \emph{True} if the given variable fulfills given criteria. The criteria are evaluated using a priority scheme from left to right. \\
	\begin{tabular}{l|c|l}
		\textbf{Bit} & \textbf{Mask} & \textbf{Description} \\
		\hline
		Bit 3 & 0x08 & variable is not zero \\
		Bit 2 & 0x04 & variable is all zero\\
		Bit 1 & 0x02 & variable has even parity\\
		Bit 0 & 0x01 & variable has odd parity
	\end{tabular}
	\\ \\
	\begin{tabular}{l|l}
		ggggbbbbbbbb0000nzeo & g \dots guard identification (4 bits) [1011] \\
		                     & b \dots variable identification (8 bits) \\
		                     & n \dots variable not zero \\
		                     & z \dots variable zero \\
		                     & e \dots variable even parity \\
		                     & o \dots variable odd parity
	\end{tabular}
	%----------
	\item\textbf{VarQueueEmpty}: Checks for values in the message queue. It is \emph{True} if the variable supports queue structure and is empty. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbb00000000 & g \dots guard identification (4 bits) [0101] \\
		                     & b \dots variable identification (8 bits)
	\end{tabular}
	%----------
	\item\textbf{VarQueueFull}: Checks for values in the message queue. It is \emph{True} if the variable supports queue structure and is full. \\
	\begin{tabular}{l|l}
		ggggbbbbbbbb00000000 & g \dots guard identification (4 bits) [0110] \\
		                     & b \dots variable identification (8 bits)
	\end{tabular}
\end{itemize}


\section{Data Instructions}

\subsection{Create(varid)}

The create instruction copies the variable contents into the send buffer \emph{snd}. The create
instruction is an asynchronous instruction.

\begin{itemize}
	\item The parameter \code{varid} identifies the variable in the variable space.
\end{itemize}

\begin{tabular}{l|l}
	
	aaaa000000000000cccccccc00000000 & a \dots the operation code (4 bits) [0001] \\
	& c \dots the variable identifier (8 bits) \\
\end{tabular}

\subsection{Send(EthType, channel, TTL)}

The send instruction adds the channel identifier to the message in the send buffer and passes it to
the Ethernet phy chip. The send instruction sets the send buffer empty bit; it is an asynchronous
instruction.

\begin{itemize}
	\item The parameter \code{EthType} indicates the type of the generated frame (real-time of best-effort).
	\item The parameter \code{channel} identifiers the channel to be used for the message.
	\item The parameter \code{TTL} indicates the maximum number of switches in the path
\end{itemize}

\begin{tabular}{l|l}
	
	aaaa000ebbbbbbbb00000000tttttttt & a \dots the operation code (4 bits) [0010] \\ 
	&	e \dots the frame type (0 for RT, 1 for BE)\\
	& b \dots the channel identifier (8 bits) \\
	& t \dots the TTL parameter (8 bits)
\end{tabular}

\subsection{Receive(channel, varid)}

The receive instruction sets the first byte in the channel receive buffer \emph{crb} to zero and
then copies the contents into the specified variable. The receive instruction clears the receive
buffer bit for channel \code{channel}; is an asynchronous instruction.

If the receive buffer is empty and a receive() instruction is executed, a time-out signal will be asserted with the duration of 1 instruction cycle.
This signal can be used by the application layer to detect the absence of data in the receive buffer.

\begin{itemize}
	\item The parameter \code{channel} identifiers the channel receive buffer \emph{crb} from which the
	data will be taken.
	\item The parameter \code{varid} identifies the variable into which the data should be copied.
\end{itemize}

\begin{tabular}{l|l}
	aaaa0000bbbbbbbbcccccccc00000000 & a \dots the operation code (4 bits) [0011] \\
	& b \dots the channel identifier (8 bits) \\
	& c \dots the variable identifier (8 bits) \\
	%& d \dots the offset in the receive buffer (8 bits) \\
	%& x \dots dump buffer \\
\end{tabular}


\section{Misc Instructions}

\subsection{Nop()}

The nop instruction performs no operation. \\

\begin{tabular}{l|l}
	%12345678901234567890123456789012
	aaaa0000000000000000000000000000 & a \dots the operation code (4 bits) [0000] \\
\end{tabular}

\subsection{Sync(mode, value)}

The sync instruction operates in two modes: (1) in master mode, it transmits a
specific synchronization message, (2) in the slave mode, it stall execution of the program until the reception of the synchronization message. In slave mode the system
decrements the timeout value (time units should be specified on the implementation). If the timeout reaches zero, then a \emph{TIMEOUT} flag is
set, which can be queried with a branch command. The instruction also empties the future queue and resets a previously set \emph{TIMEOUT} state. A
timeout value of zero means no timeout.

\begin{itemize}
	\item The parameter \code{mode}  specifies the mode in which the synchronization should be executed.
	\item The parameter \code{value} has two meanings: in master mode indicates the TTL fo the synchronization message, and in slave mode indicates the timeout.
	%\item The parameter \code{TTL} indicates the maximum number of switches in the path
\end{itemize}

\begin{tabular}{l|l}
	aaaam00000000000ddddddddtttttttt 	& a \dots the operation code (4 bits) [1111] \\
	& m \dots mode (0 slave, 1 master) \\
	& [d,t] \dots timeout (16 bits, slave only) \\
	& t \dots TTL (8 bits, master only)\\
\end{tabular}

\subsection{Count(operation)}

The count instruction gives access to a 16 bit counter.
It supports increments, decrements and reset.
The content of the counter can be queried for a certain value using a branch command.
The counter is not protected against over/underflow.

The count() instruction has a second purpose when called with the ``not'' parameter.
It can be used as a synchronization signal for the application layer, asserting a logical signal for 1 instruction cycle.
It is a one-way signal from the processor's logic to the user's logic and it doesn't stall the execution flow.

\begin{itemize}
	\item The parameter \code{operation} specifies the what should happen with the counter. It can be incremented, decremented or cleared.
\end{itemize}

\begin{tabular}{l|l}
	aaaa00oo000000000000000000000000 	& a \dots the operation code (4 bits) [1000] \\
	& o \dots the operation to perform (2 bits) \\
	& \hspace{1cm} 00 \dots (not) do nothing / app. layer sync. \\
	& \hspace{1cm} 01 \dots (clear) clear counter (set to 0) \\
	& \hspace{1cm} 10 \dots (inc) increment counter \\
	& \hspace{1cm} 11 \dots (dec) decrement counter
\end{tabular}

%\subsection{Signal(num)}
%
%The signal instruction sends an interrupt to the upper layer, thus providing means of synchronization
%between application and real time communication. 
%
%\begin{itemize}
%\item The parameter \code{num} provides a possibility to distiguish different signals generated by the same NC program.
%\end{itemize}
%
%\begin{tabular}{l|l}
%  aaaa000000000000dddddddddddddddd & a \dots the operation code (4 bits) [1001] \\
%  & d \dots 16 bit value \\
%\end{tabular}


\newpage
\section{Example}
\label{sec:example}

%\begin{lstlisting}[language=ncode,label={lst:sample},stepnumber=2,caption={Sample program.}]

\begin{verbatim}
L0: counter(inc); 					/* increment counter  */
if(TestCount, 10, L1 );	/* if the counter is equal to 10, jump to label L1 */
future(10, L0); 				/* wait 10 time units and jump to label L0 */
halt();
L1: counter(dec); 					/* increment counter  */
if(TestCount, 0, L0 );	/* if the counter is equal to 0, jump to label L0 */
future(10, L0); 				/* wait 10 time units and jump to label L1 */
halt();	
if(StatusTest, SyncTimeout, L1);
if(StatusTest, ReceiveFault, L1);
if(StatusTest, UserBit7, L1);
if(StatusTest, UserBit6, L1);
if(StatusTest, UserBit5, L1);
if(StatusTest, UserBit4, L1);
if(StatusTest, UserBit3, L1);
if(StatusTest, UserBit2, L1);
if(StatusTest, UserBit1, L1);
if(StatusTest, UserBit0, L1);
\end{verbatim}
%\end{lstlisting}

The compiled byte code for the Network Code program looks as follows:

\begin{verbatim}

82000000;
7042000A;
5000000A;
40000000;
83000000;
70020000;
5040000A;
40000000;
70430080;
70430040;
70430020;
70430010;
70430008;
70430004;
70430002;
70430001;
\end{verbatim}

\end{document}
