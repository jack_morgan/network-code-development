# README #

This repository contains the general documentation, hardware designs, and software tools necessary to synthesize and configure the real-time capable Ethernet devices introduced in the Atacama framework.


### Folder structure ###

The source files are organized in the following folders:

- Doc folder: 
	Contains a conceptual overview about the underlying principles of Atacama to provide real-time communication on top of Ethernet, and a low-level specification of the Network Code language necessary to describe TDMA arbitration.
	
- HDL_implementation:
	Contains the complete HDL files and project headers to synthesize real-time capable switches (NetFPGA-1G folder) and NICs (NC_NIC folder). README files within each sub-folder provide further details of the individual projects and instructions on how to synthesize and run the designs.

- NC_software:
	Contains software tools necessary to program the prototypes and access internal registers for configuration and runtime debugging. README files within each sub-folder provide further  instructions on how to compile and execute the different tools.